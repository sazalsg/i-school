 <div class="col-sm-12" style="padding:5px;">
       <div class="dashboard">
       <ul class="list-inline first" >

           <li class="list-inline-item li">
           <a href="/Student_dashboard" class="link"><img src="img/home.png" style="height:40px;"/><br/><span  class="text-center align">Home</span></a>
           </li>
           
           <li class="list-inline-item li">
           <a href="/student_dashboard_teacher" class="link"><img src="img/employee.png" style="height:40px;"/><br/><span  class="text-center align">Teacher</span></a>
           </li>

           <li class="list-inline-item li">
           <a href="/student_dashboard_class_routine" class="link"><img src="img/timetable.png" style="height:40px;"/><br/><span  class="text-center align">Class Routine</span></a>
           </li>&nbsp;&nbsp;

           <li class="list-inline-item li">
           <a href="/student_dashboard_subject" class="link"><img src="img/course.png" style="height:40px;"/><br/><span  class="text-center align">Subject</span></a>
           </li>

           <li class="list-inline-item li">
           <a href="" class="link"><img src="img/examsc.png" style="height:40px;"/><br/><span  class="text-center align">Exam Schedule</span></a>
           </li>

           <li class="list-inline-item li">
           <a href="/student_dashboard_library" class="link"><img src="img/library.png" style="height:40px;"/><br/><span  class="text-center align">Library</span></a>
           </li>

           <li class="list-inline-item li">
           <a href="" class="link"><img src="img/events.png" style="height:40px;"/><br/><span  class="text-center align">Events</span></a>
           </li>

           <li class="list-inline-item li">
           <a href="" class="link"><img src="img/assignment.png" style="height:40px;"/><br/><span  class="text-center align">Assignment</span></a>
           </li>

           <li class="list-inline-item li">
           <a href="/student_dashboard_payment" class="link"><img src="img/payment.png" style="height:40px;"/><br/><span  class="text-center align">Payment</span></a>
           </li>

           <li class="list-inline-item li">
           <a href="/student_dashboard_syllabus" class="link"><img src="img/syllabus.png" style="height:40px;"/><br/><span  class="text-center align">Academic Syllabus</span></a>
           </li>

           <li class="list-inline-item li">
           <a href="/student_dashboard_exam" class="link"><img src="img/exam.png" style="height:40px;"/><br/><span  class="text-center align">Exam</span></a>
           </li>

           <li class="list-inline-item li">
           <a href="" class="link"><img src="img/lesson.png" style="height:40px;"/><br/><span  class="text-center align">Lesson Plan</span></a>
           </li>

           <li class="list-inline-item li">
           <a href="" class="link"><img src="img/achievement.png" style="height:40px;"/><br/><span  class="text-center align">Achievements</span></a>
           </li>

           <li class="list-inline-item li">
           <a href="" class="link"><img src="img/application.png" style="height:40px;"/><br/><span  class="text-center align">Application</span></a>
           </li>

           <li class="list-inline-item li">
           <a href="" class="link"><img src="img/material.png" style="height:40px;"/><br/><span  class="text-center align">Material Request</span></a>
           </li>

           <li class="list-inline-item li">
           <a href="/student_dashboard_hostel" class="link"><img src="img/hostel.png" style="height:40px;"/><br/><span  class="text-center align">Hostel</span></a>
           </li>&nbsp;&nbsp;&nbsp;

           <li class="list-inline-item li">
           <a href="/student_dashboard_transport" class="link"><img src="img/transport.png" style="height:40px;"/><br/><span  class="text-center align">Transport</span></a>
           </li>&nbsp;&nbsp;

           <li class="list-inline-item li">
           <a href="" class="link"><img src="img/report.png" style="height:40px;"/><br/><span  class="text-center align">Notice</span></a>
           </li>

           <li class="list-inline-item li">
           <a href="" class="link"><img src="img/settings.png" style="height:40px;"/><br/><span  class="text-center align">Account</span></a>
           </li>

       </ul>

     </div>
</div>
