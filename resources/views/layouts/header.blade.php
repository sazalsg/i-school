<!DOCTYPE html>
<html lang="en">
  <head>
    <title>@yield('Title')</title>
    <link rel="icon" type="image/gif" href="{{URL::asset('img/HS LOGO.png')}}" />
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <meta property="og:image" content="{{URL::asset('img/HS LOGO.png')}}">
      <meta property="og:image" content="http://0.0.0.0:8888/img/HS LOGO.png" />

    <meta property="og:image" content="{{URL::asset('http://www.i-deasolution.com/img/HS LOGO.png')}}">

    @include('layouts.css_link')
</head>

  
  <body>
    <!--Header-part-->
    <div id="header">
      <h1><a href="/">School Management Admin</a></h1>
    </div>
    <!--close-Header-part-->
