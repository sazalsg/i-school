<!--sidebar-menu-->
<div id="sidebar">
  <a href="#" class="visible-phone"><i class="fa fa-tachometer" aria-hidden="true"></i>Dashboard</a>
  
  <ul>
    <li class="active"><a href="/"><i class="fa fa-tachometer" aria-hidden="true"></i><span> Dashboard</span></a></li>
   


     <li class="submenu"><a href="#"><i class="fa fa-list-alt" aria-hidden="true"></i><span>&nbsp;RBAC</span><span class="label label-important">5</span></a>
      <ul>
        <li><a href="/create_admin"><i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;Create Admin</a></li>
        <li><a href="/create_permission"><i class="fa fa-unlock" aria-hidden="true"></i>&nbsp;Create Permission</a></li>
        <li><a href="/create_role"><i class="fa fa-user-secret" aria-hidden="true"></i>&nbsp;Create Role</a></li>
        <li><a href="/permission_role"><i class="fa fa-check" aria-hidden="true"></i>&nbsp; Permission Role</a></li>
        <li><a href="/user_access"><i class="fa fa-sign-in" aria-hidden="true"></i></i>&nbsp; User Access</a></li>
      </ul>
    </li>







 @permission('STUDENTS')

    <li class="submenu"><a href="#"><i class="fa fa-users" aria-hidden="true"></i><span> Students</span><span class="label label-important">3</span></a>
  <ul>
        
    <li><a href="/aplicant_student"><i class="fa fa-user-plus" aria-hidden="true"></i> Aplicant Students</a></li>
    <li><a href="/applicant_student_report"><i class="fa fa-user-plus" aria-hidden="true"></i> Aplicant Students Report</a></li>
    <li><a href="/addmission_test"><i class="fa fa-graduation-cap" aria-hidden="true"></i>Addmission Test</a></li>
    <li><a href="/admit_student"><i class="fa fa-graduation-cap" aria-hidden="true"></i> Admit Students</a></li>
    <li><a href="/admit_bulk_student"><i class="fa fa-address-card-o" aria-hidden="true"></i>Admit Bulk Students</a></li>
    <li><a href="/student_promoation"><i class="fa fa-line-chart" aria-hidden="true"></i> Students Promotion</a></li>
</ul>
    </li>
    @endpermission
    
  <li class="submenu"><a href="#"><i class="fa fa-users" aria-hidden="true"></i><span> Students Information</span><span class="label label-important">3</span></a>
      <ul>
         @php $student_class=DB::table('students')->groupby('class')->get();  @endphp
          @foreach($student_class as $student_class_data)
          
         
          
        <li><a href="/student_information/{{$student_class_data->class}}"><i class="fa fa-info" aria-hidden="true"></i> &nbsp;{{$student_class_data->class}}</a></li>
          @endforeach
          
      </ul>
   </li>


    <li class="submenu">
      <a href="#">
        <i class="fa fa-smile-o" aria-hidden="true"></i>
        <span> Apprisal</span>
        <span class="label label-important">3</span>
      </a>
      <ul>
        <li>
          <a href="/student_apprisal">
            <i class="fa fa-user-o" aria-hidden="true"></i> Apprisal
          </a>
        </li>
        <li>
          <a href="/student_apprisal_component">
            <i class="fa fa-object-ungroup" aria-hidden="true"></i>Apprisal Templete
          </a>
        </li>
        <li>
          <a href="/student_apprisal_report">
            <i class="fa fa-list" aria-hidden="true"></i> Apprisal Report
          </a>
        </li>
        <li>
          <a href="/apprisal_template_report">
            <i class="fa fa-list" aria-hidden="true"></i> Apprisal Template Report
          </a>
        </li>

        
      </ul>
    </li>
    <li class="submenu">
      <a href="#">
        <i class="fa fa-users" aria-hidden="true"></i>
        <span> Teacher And Staff</span>
        <span class="label label-important">3</span>
      </a>
      <ul>
        <li>
          <a href="/teacher_info">
            <i class="fa fa-user-plus" aria-hidden="true"></i> Teacher
          </a>
        </li>
        <li>
          <a href="/teacher_info_report">
            <i class="fa fa-list-alt" aria-hidden="true"></i> Teacher's Report
          </a>
        </li>
        <li >
          <a href="/staff_info">
            <i class="fa fa-male" aria-hidden="true"></i>
            <span>Saff Information</span>
            <span class="label label-important"></span>
          </a>
        </li>
        <li>
          <a href="/staff_report">
            <i class="fa fa-list-alt" aria-hidden="true"></i> Staff Report
          </a>
        </li>
      </ul>
    </li>
    <li class="submenu">
      <a href="#">
        <i class="fa fa-id-badge" aria-hidden="true"></i>
        <span> Parent</span>
        <span class="label label-important">2</span>
      </a>
      <ul>
        <li>
          <a href="/parents_info">
            <i class="fa fa-address-book" aria-hidden="true"></i> Parents
          </a>
        </li>
        <li>
          <a href="/parentreport">
            <i class="fa fa-address-card-o" aria-hidden="true"></i> Parents Report
          </a>
        </li>
      </ul>
    </li>
    <li class="submenu">
      <a href="#">
        <i class="fa fa-id-card-o" aria-hidden="true"></i>
        <span> Class</span>
        <span class="label label-important">3</span>
      </a>
      <ul>
        <li>
          <a href="/manage_class">
            <i class="fa fa-book" aria-hidden="true"></i> Manage Classes
          </a>
        </li>
        <li>
          <a href="/manage_section">
            <i class="fa fa-bandcamp" aria-hidden="true"></i> Manage Sections
          </a>
        </li>
        <li>
          <a href="/academic_syllabus">
            <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Academic Syllabus
          </a>
        </li>
        <li>
          <a href="/manage_department">
            <i class="fa fa-table" aria-hidden="true"></i> Manage Department
          </a>
        </li>
      </ul>
    </li>
    <!--<li><a href="#"><i class="icon icon-tint"></i> <span>Buttons &amp; icons</span></a></li>
<li><a href="#"><i class="icon icon-pencil"></i> <span>Eelements</span></a></li>-->
    <li class="submenu">
      <a href="#">
        <i class="fa fa-bookmark" aria-hidden="true"></i>
        <span> Subject</span>
        <span class="label label-important">5</span>
      </a>
      <ul>
        @php
        $class_name=DB::table('manage_class')->get();@endphp
        @foreach($class_name as $class_name_list)
        <li>
          <a href="/manage_subject/{{$class_name_list->class_name}}">
            <i class="fa fa-calendar-o" aria-hidden="true"></i> {{$class_name_list->class_name}}
          </a>
        </li>
        @endforeach

       <li>
          <a href="/component">
            <i class="fa fa-plus-square" aria-hidden="true"></i> Component
          </a>
        </li>

      </ul>
    </li>
    <li class="submenu">
      <a href="#">
        <i class="fa fa-calendar" aria-hidden="true"></i>
        <span> Class Routine</span>
        <span class="label label-important">5</span>
      </a>
      <ul>
        @php
        $class_name=DB::table('manage_class')->get();@endphp
        @foreach($class_name as $class_name_list)
        <li>
          <a href="/manage_class_routine/{{$class_name_list->class_name}}">
            <i class="fa fa-calendar-o" aria-hidden="true"></i> {{$class_name_list->class_name}}
          </a>
        </li>
        @endforeach
      </ul>
    </li>
    <li class="submenu">
      <a href="#">
        <i class="fa fa-area-chart" aria-hidden="true"></i>
        <span> Daily Attendence</span>
        <span class="label label-important">5</span>
      </a>
      <ul>
        <li>
          <a href="/daily_attendance">
            <i class="fa fa-hand-paper-o" aria-hidden="true"></i> Daily Attendence
          </a>
        </li>
        <li>
          <a href="/daily_attendance_report">
            <i class="fa fa-file-text-o" aria-hidden="true"></i> Attendence Report
          </a>
        </li>
      </ul>
    </li>
    <li class="submenu">
      <a href="#">
        <i class="fa fa-graduation-cap" aria-hidden="true"></i>
        <span> Exam</span>
        <span class="label label-important">4</span>
      </a>
      <ul>
        <li>
          <a href="/exam_list">
            <i class="fa fa-list" aria-hidden="true"></i> Exam List
          </a>
        </li>
        <li>
          <a href="/exam_grade">
            <i class="fa fa-university" aria-hidden="true"></i> Exam Grade
          </a>
        </li>

        <li>
          <a href="/manage_marks">
            <i class="fa fa-building-o" aria-hidden="true"></i> Manage Marks
          </a>
        </li>

        <li>
          <a href="/check_marks">
            <i class="fa fa-building-o" aria-hidden="true"></i> Check Marks
          </a>
        </li>


        <li>
          <a href="/marks_publish">
            <i class="fa fa-building-o" aria-hidden="true"></i> Publish Marks
          </a>
        </li>


        <!-- <li><a href="#"><i class="fa fa-circle" aria-hidden="true"></i> Send Marks By Sms</a></li> -->
        <li>
          <a href="/tabulation_sheet">
            <i class="fa fa-file-o" aria-hidden="true"></i> Tabulation Sheet
          </a>
        </li>
        <li>
          <a href="/question_paper">
            <i class="fa fa-file-text" aria-hidden="true"></i> Question Paper
          </a>
        </li>
      </ul>
    </li>
    <li class="submenu">
      <a href="#">
        <i class="fa fa-money" aria-hidden="true"></i>
        <span> Payroll</span>
        <span class="label label-important">4</span>
      </a>
      <ul>
        <li>
          <a href="/salary_slip">
            <i class="fa fa-list" aria-hidden="true"></i>Salary Slip
          </a>
        </li>
         <li>
          <a href="/salary_slip_report">
            <i class="fa fa-list" aria-hidden="true"></i>Salary Slip Report
          </a>
        </li>
        <li>
          <a href="/salary_structure">
            <i class="fa fa-university" aria-hidden="true"></i>Salary Structure
          </a>
        </li>
        <li>
          <a href="/salary_component">
            <i class="fa fa-university" aria-hidden="true"></i>Salary Components
          </a>
        </li>
      </ul>
    </li>


    <li class="submenu">
      <a href="#">
        <i class="fa fa-briefcase" aria-hidden="true"></i>
        <span> Accounting</span>
        <span class="label label-important">4</span>
      </a>
      <ul>
        <li>
          <a href="/accountant">
            <i class="fa fa-briefcase" aria-hidden="true"></i>
            <span> Accountant</span>
          </a>
        </li>
        <li>
          <a href="/chart_of_account">
            <i class="fa fa-pie-chart" aria-hidden="true"></i> &nbsp;Chart Of Account
          </a>
        </li>

        <li>
          <a href="/create_template">
            <i class="fa fa-pie-chart" aria-hidden="true"></i> &nbsp;Payment Templete
          </a>
        </li>
        <li>
          <a href="/student_payment_entry">
            <i class="fa fa-window-maximize" aria-hidden="true"></i> &nbsp; Invoice
          </a>
        </li>

        <li>
          <a href="/invoice">
            <i class="fa fa-money" aria-hidden="true"></i> &nbsp;Invoice Report
          </a>
        </li>

        
        <li>
          <a href="/invoice_component">
            <i class="fa fa-money" aria-hidden="true"></i> &nbsp;Invoice Component
          </a>
        </li>
        
        
         
        <li>
          <a href="/payment_history">
            <i class="fa fa-money" aria-hidden="true"></i> &nbsp;Payment History
          </a>
        </li>

        <li>
          <a href="/expense">
            <i class="fa fa-etsy" aria-hidden="true"></i> Journal Entry
          </a>
        </li>
       
      </ul>
    </li>
    <li class="submenu">
      <a href="#">
        <i class="fa fa-book" aria-hidden="true"></i>
        <span> Libray</span>
        <span class="label label-important">3</span>
      </a>
      <ul>
        <!-- <li><a href="/manage_book"><i class="fa fa-book" aria-hidden="true"></i> Manage Book</a></li> -->
        <li>
          <a href="/templet_article">
            <i class="fa fa-circle" aria-hidden="true"></i> Templete Article
          </a>
        </li>
        <li>
          <a href="/stock_article">
            <i class="fa fa-circle" aria-hidden="true"></i> Stock Article
          </a>
        </li>
        <li>
          <a href="/article">
            <i class="fa fa-circle" aria-hidden="true"></i> Manage Article
          </a>
        </li>
        <li>
          <a href="/membership">
            <i class="fa fa-circle" aria-hidden="true"></i> Membership
          </a>
        </li>
        <li>
          <a href="/article_issue">
            <i class="fa fa-circle" aria-hidden="true"></i> Article Issue
          </a>
        </li>
        <li>
          <a href="/article_recive">
            <i class="fa fa-circle" aria-hidden="true"></i> Article Recived
          </a>
        </li>
      </ul>
    </li>
    <li class="submenu">
      <a href="#">
        <i class="fa fa-id-card-o" aria-hidden="true"></i>
        <span> Transport</span>
        <span class="label label-important">3</span>
      </a>
      <ul>

          
        

         <li>
          <a href="/route">
           <i class="fa fa-road" aria-hidden="true"></i> Route
          </a>
        </li>
        
        <li>
          <a href="/manage_transport">
            <i class="fa fa-bus" aria-hidden="true"></i> Manage Transport
          </a>
        </li>
        <li>
          <a href="/assign_transport">
            <i class="fa fa-circle" aria-hidden="true"></i> Assign Transport
          </a>
        </li>
      </ul>
    </li>
    <li class="submenu">
      <a href="#">
        <i class="fa fa-id-card-o" aria-hidden="true"></i>
        <span> Dormitory</span>
        <span class="label label-important">3</span>
      </a>
      <ul>
        <li>
          <a href="/dormitory">
            <i class="fa fa-life-ring" aria-hidden="true"></i> Manage Dormitory
          </a>
        </li>
        <li>
          <a href="/assign_dormitory">
            <i class="fa fa-check-square-o" aria-hidden="true"></i> Assign Dormitory
          </a>
        </li>
      </ul>
    </li>
    <li>
      <a href="/notice_board">
        <i class="fa fa-window-maximize" aria-hidden="true"></i>
        <span> Noticeboard</span>
      </a>
    </li>
    <li>
      <a href="#">
        <i class="fa fa-comments" aria-hidden="true"></i>
        <span> Messages</span>
      </a>
    </li>
   
    <li class="submenu">
      <a href="#">
        <i class="icon icon-info-sign"></i>
        <span> Settings</span>
        <span class="label label-important">3</span>
      </a>
      <ul>
        <li>
          <a href="/general_settings">
            <i class="fa fa-circle" aria-hidden="true"></i> General Settings
          </a>
        </li>
          <li>
          <a href="/setup">
            <i class="fa fa-circle" aria-hidden="true"></i> All Setup
          </a>
        </li>
        <li>
          <a href="/sms_settings">
            <i class="fa fa-circle" aria-hidden="true"></i> Sms Settings
          </a>
        </li>
        <li>
          <a href="/language_settings">
            <i class="fa fa-circle" aria-hidden="true"></i> Language Settings
          </a>
        </li>
      </ul>
    </li>
  </ul>
</div>
<!--sidebar-menu-->
