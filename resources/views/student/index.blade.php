@extends('student.master_template')
@section('dashboard_title','Dashboard')
@section('breadcrumbs','Home')
@section('student_dasboard_content')
		<div class="quick-actions_homepage">
			<div class="widget-content" style="margin-top:-50px;" >
				<div class="row-fluid">

		      <div class="widget-box">
		        <div class="widget-title bg_lg"><span class="icon"><i class="icon-signal"></i></span>
		        </div>
		        <div class="widget-content" >￼
							<div class="span3">
								<a href=" ">Quick link</a>
								<div class="list-group">
								  <a href="/book_list" class="list-group-item">Book List</a>
								  <a href="#" class="list-group-item">Course Advisor</a>
									<a href="#" class="list-group-item">Academic Programs</a>
									<a href="#" class="list-group-item">Rules Of Class Attendance</a>
									<a href="#" class="list-group-item">Academic Rules</a>
								</div>
							</div>&nbsp;
		          <div class="row-fluid">
		            <div class="span6">
									<iframe src="https://calendar.google.com/calendar/embed?src=codebool71%40gmail.com&ctz=Asia/Dhaka" style="border:0" width="480" height="500" frameborder="0" scrolling="no"></iframe>
		            </div>
		            <div class="span3">
									<ul class="site-stats">
										<li class="bg_lh" style="height:103px;"><i class="fa fa-address-book" aria-hidden="true"></i><strong>{{$issue_book}}</strong> <small>My Issue Book</small></li>
										<li class="bg_lh" style="height:103px;"><i class="fa fa-book" aria-hidden="true"></i> <strong>{{$subject_data}}</strong> <small>My Total Subject</small></li>
										<li class="bg_lh" style="height:103px;"><i class="fa fa-male" aria-hidden="true"></i> <strong>{{$class_teacher->class_teacher}}</strong> <small> My Class Teacher</small></li>
										<li class="bg_lh" style="height:103px;"><i class="fa fa-user" aria-hidden="true"></i><strong>{{$attendance}}</strong> <small>My Total Attendance</small></li>
									</ul>
		            </div>
		          </div>
		        </div>
		      </div>
		    </div>
			</div>
			<div class="row-fluid">
				<div class="span12" style="width:1320px;">
					<div class="widget-box">
						<div class="widget-title"> <span class="icon"><i class="icon-ok"></i></span>
							<h5>My Class Routine</h5>
						</div>
						<div class="widget-content">
					 	 <div class="todo">
							 <div id="home" class="tab-pane fade in active">
								 <div class="panel panel-default" data-collapsed="0">
									 <div class="panel-body">

										 @foreach($section as $section_value)

							 <div class="div_print_{{$section_value->section}}">

						 <div>
							 <span>{{Session::get('school.system_name')}}</span><br>
							 <span>Class Routine For : {{$class_name}}</span><br>
							 <span></span><br>
							 <span style="margin-top: 3%;" class="text-center tag">Section Name : {{$section_value->section}}</span>
						 </div>



										 <table class="table table-bordered" border="0" cellspacing="0" cellpadding="0">
													 <tbody>
																	<?php
								 for ($i=1; $i <= 7; $i++) {

								 $sunday=DB::table('class_routine')->join('class_routine_start_child','class_routine.class_routine_id','=','class_routine_start_child.parent')
										 ->join('class_routine_end_child','class_routine.class_routine_id','=','class_routine_end_child.parent')->where('class_name',$class_name)->where('section',$section_value->section)->where('class_day',$i)->orderby('class_starting_time','asc')->get();
										 ?>
														 <tr class="gradeA">

															 <td width="100"><?php
															 if ($i==1):
																 echo "Saturday";
															 elseif ($i==2):
																 echo "Sunday";
															 elseif ($i==3):
																 echo "Monday";
															 elseif ($i==4):
																 echo "Tuesday";
															 elseif ($i==5):
																 echo "Wednesday";
															 elseif ($i==6):
																 echo "Thursday";
															 elseif ($i==7):
																 echo "Friday";
															 endif;
															 ?></td>

																@foreach($sunday as $sunday_value)

															 <td>
																	 <div >
																							 <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
																								 <span style="color:crimson">{{$sunday_value->subject}}</span>( <?php echo date("h.i A", $sunday_value->class_starting_time); ?> - <?php echo date("h.i A", $sunday_value->class_ending_time); ?>)
																								 <span>
																									 <br><span style="color: green">{{$sunday_value->teacher}}</span>
																								 </span>
																							 </button>

																				 </div>

															 </td>
															 @endforeach

														 </tr>
														 <?php
						 }
						 ?>
													 </tbody>
										 </table>
						 </div>

							<a id='print' onclick="pop_print_<?php echo $section_value->section ?>()" media='print'  title='Print' type="button"><i class="fa fa-print" aria-hidden="true"></i></a>

						 <script type="text/javascript">

						 function pop_print_<?php echo $section_value->section ?>(){
								 w=window.open(null, 'Print_Page', 'scrollbars=yes');
								 w.document.write(jQuery('.div_print_<?php echo $section_value->section ?>').html());
								 w.document.write('<style>@page{size:landscape;}</style><html><head><title></title>');
								 w.document.write("<link href='/css/bootstrap.min.css'>");
								 w.document.close();
								 w.print();
						 }
						 </script>
										 @endforeach
									 </div>
								 </div>
							 </div>

					 	 </div>
					  </div>

					</div>
					<div class="widget-box">
						<div class="widget-title"> <span class="icon"><i class="icon-ok"></i></span>
							<h5>{{Session::get('class')}} Class Notice Board</h5>
						</div>
						<div class="widget-content">
							<div class="todo">
								<ul>
									@foreach($class_notice as $class_notice_data)
									<li>
										<div class="user-thumb"> <img width="40" height="40" alt="User" src="img/notice.png"> </div>
										<div class="article-post"> <marquee><h3 style="color:#2980b9">{{$class_notice_data->notice_title}}</h3></marquee></div>
										<div class="article-post"> <h5>{{$class_notice_data->notice_subject}}</h5></div>
										<div class="article-post"> <h5>{{$class_notice_data->Notice}}</h5></div>
									</li>
									@endforeach
								</ul>
							</div>
						</div>
					</div>
					<div class="widget-box">
						<div class="widget-title"> <span class="icon"><i class="icon-ok"></i></span>
							<h5>My Notice List</h5>
						</div>
						<div class="widget-content">
							<div class="todo">
								<ul>
									<li>
										@foreach($student_notice as $student_notice_data)
										<div class="user-thumb"> <img width="40" height="40" alt="User" src="img/notice.png"> </div>
										<div class="article-post"><h3 style="color:#2980b9">{{$student_notice_data->notice_title}}</h3></div>
										<div class="article-post"> <h5>{{$student_notice_data->notice_subject}}</h5></div>
										<div class="article-post"> <h5>{{$student_notice_data->Notice}}</h5></div>
										</div>
									</li>
									@endforeach
								</ul>
							</div>
						</div>
					</div>
				</div>

			<!-- <div class="row-fluid">
				 <div class="span6">
					 <div class="widget-box">
						 <div class="widget-title"> <span class="icon"><i class="icon-ok"></i></span>
							 <h5>Class Routine</h5>
						 </div>
						 <div class="widget-content">
							 <div class="todo">
								 <ul>
									 <li>
										 <div class="user-thumb"> <img width="40" height="40" alt="User" src="img/class.png"> </div>
										 <div class="article-post"> <span class="user-info">Class:</span>
											 <p><a href="#"></a>Started Time </p>
										 </div>
									 </li>
								 </ul>
							 </div>
						 </div>
					 </div>
					 </div>
        <div class="span6">
					 <div class="widget-box widget-chat">
						 <div class="widget-title bg_lb"> <span class="icon"> <i class="icon-comment"></i> </span>
							 <h5>Chat Option</h5>
						 </div>
						 <div class="widget-content nopadding collapse in" id="collapseG4">
							 <div class="chat-users panel-right2">
								 <div class="panel-title">
									 <h5>Online Users</h5>
								 </div>
								 <div class="panel-content nopadding">
									 <ul class="contact-list">
										 <li id="user-Alex" class="online"><a href=""><img alt="" src="img/demo/av1.jpg" /> <span>Alex</span></a></li>
										 <li id="user-Linda"><a href=""><img alt="" src="img/demo/av2.jpg" /> <span>Linda</span></a></li>
										 <li id="user-John" class="online new"><a href=""><img alt="" src="img/demo/av3.jpg" /> <span>John</span></a><span class="msg-count badge badge-info">3</span></li>
										 <li id="user-Mark" class="online"><a href=""><img alt="" src="img/demo/av4.jpg" /> <span>Mark</span></a></li>
										 <li id="user-Maxi" class="online"><a href=""><img alt="" src="img/demo/av5.jpg" /> <span>Maxi</span></a></li>
									 </ul>
								 </div>
							 </div>
							 <div class="chat-content panel-left2">
								 <div class="chat-messages" id="chat-messages">
									 <div id="chat-messages-inner"></div>
								 </div>
								 <div class="chat-message well">
									 <button class="btn btn-success">Send</button>
									 <span class="input-box">
									 <input type="text" name="msg-box" id="msg-box" />
									 </span> </div>
							 </div>
						 </div>
					 </div>
				 </div>
			 </div> -->















































</div>
@stop
