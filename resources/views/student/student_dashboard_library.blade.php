@extends('student.master_template')
@section('dashboard_title','Student Dhasboard Library')
@section('breadcrumbs','Student Dhasboard Library')
@section('student_dasboard_content')
<div class="container">

<?php

if($membership_info)
{

   $member_status=$membership_info->status;
   if($member_status=='Active')	
	{
		 ?>

   <table class="table table-responsive " style="background: #fff;">
	 <tbody>
	  <tr style="background:#0D1582">
		<td class="text-center" colspan="2">
		 <span style="font-size:19px; color:#FFFFFF;"><b>Subject Information</b></span>
	    </td>
	  </tr>
	  </tbody>
	  </table>
  <table class="table table-bordered">
  <thead  class="thead-inverse"> 
    <tr>
      <th>Sl.No</th>
      <th>Article Id</th>
      <th>Article Name</th>
      <th>Issue Date</th>
      <th>Return Date</th>
      <th>Status</th>
    </tr>
  </thead>
  <tbody>
  
  
    <tr>
      <th scope="row">{{1}}</th>
       <td>{{$student_data->article_id}}</td>
      <td>{{$student_data->	article_name}}</td>
      <td>{{$student_data->	issue_date}}</td>
      <td>{{$student_data->	return_date}}</td>
      <td>{{$student_data->status}}</td>
     
    </tr>

  </tbody>
</table>
		<?php
	}
	else
	{
		echo "Inactive";
	}

}
else
{
	echo "U are not Member of Library";
}


?>
</div>
@stop
