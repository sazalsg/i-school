@extends('admin.index')
@section('Title','daily_attendance')
@section('breadcrumbs','daily_attendance')
@section('breadcrumbs_link','/daily_attendance')
@section('breadcrumbs_title','daily_attendance')
@section('content')
@if (Session::has('success'))
    <div class="alert alert-success alert-dismissible fade in">
                <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong><i class="fa fa-commenting-o" aria-hidden="true"></i> &nbsp; Success!</strong> {{ Session::get('success') }}
    </div>
   
@endif


@if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissible fade in">
        <ul  style='list-style:none'>
            @foreach ($errors->all() as $error)
                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> &nbsp;{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


{{Form::open(['url'=>'/daily_attendance','class'=>'form-horizontal','method'=>'post','name'=>'basic_validate','id'=>'basic_validate','novalidate'=>'novalidate'])}}


<div class="container">
        <h2><i class="fa fa-hand-paper-o" aria-hidden="true"></i></i> Student Daily Attandance </h2>
  <!-- Tab Heading  -->
        <p title="Transport Details">{{Session::get('school.system_name')}}( {{Session::get('school.school_eiin')}} ) Add Student Daily Attandance</p>
         <div class='row'>
         
         <div class="panel panel-default" >
          <div class="panel-body text-left">
             <ul class='dropdown_test'>
             
            <li><a href='/home'><i class="fa fa-tachometer" aria-hidden="true"></i> &nbsp;Homes</a></li>
              <li><a href='/daily_attendance_report'><i class="fa fa-hand-paper-o" aria-hidden="true"></i> Daily Attendance Report</a></li>
            <li><a href='/teacher_info'><i class="fa fa-street-view" aria-hidden="true"></i> Add Teacher</a></li>
            <li><a href='/staff_report'><i class="fa fa-address-book-o" aria-hidden="true"></i> Staff's Report </a></li>
             </ul>
          </div>
        </div>
      <div class="controls text-right">
                <div data-toggle="buttons-checkbox" class="btn-group">
                  <button  class="btn btn-default"  title='Export PDF' type="button"><a target="_blank" href="/daily_attendance_pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></button>

                  <button class="btn btn-default"  title='Export Excel' type="button"><a  href="/daily_attendance_excle"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a></button>
                  
                  <button class="btn btn-default" title='Preview' ttype="button"><a target="_blank" href="/daily_attendance_pdf"><i class="fa fa-street-view" aria-hidden="true"></i></a></button>
                  
                  <button class="btn btn-default" title='Print' type="button"><i class="fa fa-print" aria-hidden="true"></i></button>

                </div>
        </div>
    </div>


  <!-- Transport Details -->
              <div class="widget-box">
                <div class="widget-title">
                  <span class="icon">
                    <i class="icon-info-sign">
                    </i>
                  </span>
                  <h5>Add Daily Attendance
                  </h5>
                </div>
                <div class="widget-content padding">

            
                    <div class="control-group">
                      <table class="table address">
                        <thead>
                          <tr>
                          
                            <th>Class</th>
                            <th>Section</th>
                            <th>Department</th>
                            <th>subject</th>
                          </tr>
                        </thead>
                    
                    <tbody>
                     <tr>
                        @foreach($class_data as $class_list_data)
                          @php $class_list_all_data[$class_list_data->class_name]=$class_list_data->class_name @endphp
                        @endforeach
                        <td>{{Form::select('class_name',$class_list_all_data,null,['style'=>'width: 157px','id'=>'class_info'])}}</td>
                        <td>{{Form::select('section',['Section Name'=>'Section Name'],null,['id'=>'section_info','style'=>'width: 157px'])}}</td>
                        <td>{{Form::select('Department',['Department Name'=>'Department Name'],null,['id'=>'Department_info','style'=>'width: 157px'])}}</td>
                        <td>{{Form::select('subject',['subject Name'=>'subject Name'],null,['id'=>'subject_info','style'=>'width: 157px'])}}</td>
                        <td> {{Form::hidden('date',date('d-m-Y'),['id'=>'required','placeholder'=>'Stock Id','title'=>'date'])}}</td>
                     </tr>
                    </tbody>
                    </table>
                </div>
                 
            </div>
</div>



<div id="table_show_trigger_forsubject"  hidden="hidden" class="col-xs-12">
      <table style="width: 25%;" class="table">
          

          <tr>
              <td><b>Class Name</b></td>
              <td class="class_name_in_view"></td>
              
          </tr>

          <tr>
              <td><b>Section Name</b></td>
              <td class="section_name_in_view"></td>
              
          </tr>

          <tr>
              <td><b>Department Name</b></td>
              <td class="department_name_in_view"></td>
              
          </tr>

          <tr>
              <td><b>Subject Name</b></td>
              <td class="subject_name_in_view"></td>
              
          </tr>
           <tr>
              <td><b>Date</b></td>
              <td ><?php echo date('d-m-Y')?></td>
              
          </tr>
          
      </table>
</div>

 

<div class="container">
 
  <div class="row" style="text-align: center;">
    <div class="col-sm-4"></div>
    <div class="col-sm-4" >
      <div class="tile-stats tile-gray">
        <div class="icon"><i class="entypo-chart-area"></i></div>
        <h3 style="color: #34495e;">Attendance For Cl</h3>
        <h4 style="color: #34495e;">Section</h4>
        <h4 style="color:#34495e;"></h4>
      </div>
    </div>
    <div class="col-sm-4"></div>
  </div>

  <br></br>


  <center>
    <a class="btn btn-default"><i class="fa fa-check-square-o" aria-hidden="true"></i> Mark All Present</a>
    <a class="btn btn-default"><i class="fa fa-times" aria-hidden="true"></i> Mark All Absent</a>
  </center>
  <br></br>



  <div id="attendance_update">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>Roll</th>
          <th><?php echo date('d-m-Y') ?></th>
        
        </tr>
      </thead>
      <tbody id="student_data">
       
      </tbody>
    </table>
  </div>


  <center>
    {{Form::submit('Save Attendance',['class'=>'btn btn-success'])}}
   
  </center>
 
</div>
{{Form::close()}}




<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script type="text/javascript">

      $(document).ready(function(){
        
       


        

         $('#class_info').unbind().change(function(){
            var class_name=$('#class_info').val();
                $.ajax({
                url: '/class_w_department_filter',
                type: "post",
                data: {'class_name':class_name,'_token': $('input[name=_token]').val()},
                success: function(data){
                  $('#Department_info').html(data);
                }
              });


                $.ajax({
                  url: '/class_w_section_filter',
                  type: "post",
                  data: {'class_name':class_name,'_token': $('input[name=_token]').val()},
                  success: function(data){
                    $('#section_info').html(data);
                  }
                });

                 $.ajax({
                  url: '/class_w_subject_filter',
                  type: "post",
                  data: {'class_name':class_name,'_token': $('input[name=_token]').val()},
                  success: function(data){
                    $('#subject_info').html(data);
                  }
                });
            
         });

          $("#subject_info").unbind().change(function(){
                var subject=$("#subject_info").val();
                var section=$("#section_info").val();
                var Department_info=$("#Department_info").val();
                var class_name=$("#class_info").val();

                $("#table_show_trigger_forsubject").show(500);

              
                $('.class_name_in_view').text(class_name);
                $('.section_name_in_view').text(section);
                $('.department_name_in_view').text(Department_info);
                $('.subject_name_in_view').text(subject);


               $.ajax({
                url:'/attendance_student',
                type:"post",
                data:{'class_name':class_name,'section':section,'Department_info':Department_info,'_token': $('input[name=_token]').val()},
                success:function(r)
                {
                    $("#student_data").html("");
                    for(i=0;i<r.length;i++)
                    {
                      $("#student_data").append("<tr>\
                        <td>"+r[i].roll+"</td>\
                        <td><input type=\"checkbox\" value="+r[i].roll+" name=\"status[]\"></td>\
                      </tr>");
                    }   
                }
               });
          
            });

         
           

         
 });



      </script>

@stop