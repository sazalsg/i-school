@extends('admin.index')
@section('Title','Chart Of Accountant Edit')
@section('breadcrumbs','Chart of Accountant Edit')
@section('breadcrumbs_link','/chart_of_account_edit')
@section('breadcrumbs_title','Manage Chart Of Accountant edit')

@section('content')
@if (Session::has('success'))
    <div class="alert alert-success alert-dismissible fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong> {{ Session::get('success') }}
    </div>
   
@endif

@if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissible fade in">
        <ul  style='list-style:none'>
            @foreach ($errors->all() as $error)
                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> &nbsp;{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif





	
<h2><i class="fa fa-list-alt" aria-hidden="true"></i> &nbspChart Of Account</h2> <!-- Tab Heading  -->
 		<p title="Transport Details">{{Session::get('school.system_name')}}  Chart Of Accounts</p> <!-- Transport Details -->


    <div class='row'>
         
         <div class="panel panel-default" >
          <div class="panel-body text-left">
             <ul class='dropdown_test'>
                <li><a href='/create_admin'><i class="fa fa-list-alt" aria-hidden="true"></i> &nbsp;Chart Of Account</a></li>
                <li><a href='/user_access'><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Journal Entry</a></li>
                <li><a href='/permission_role'><i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;Get Payment</a></li>

                <li><a href='/chart_of_account'>&nbsp;<i class="fa fa-backward" aria-hidden="true"></i></a></li>
             </ul>
          </div>
        </div>



    </div>




                <div class="widget-box">
                  <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                    <h5>Data table</h5>
                  </div>

          <div class="widget-content nopadding">
             {{Form::open(['url'=>"/chart_of_account/$accounts->id",'files'=>true,'class'=>'form-horizontal','method'=>'PUT','name'=>'basic_validate','id'=>'basic_validate','navalidate'=>'navalidate'])}} 
             
              <div class="control-group">
                {{Form::label('account_name','Account Name:',['class'=>'control-label','title'=>'Account Name'])}}
                <div class="controls">
                  {{Form::text('account_name',$accounts->account_name,['id'=>'required','title'=>'accountant_name','placeholder'=>'Accountant Name'])}}
                </div>
              </div>
              
              <div class="control-group">
                {{Form::label('root','Root',['class'=>'control-label','title'=>'root'])}}
                <div class="controls">
                  {{Form::select('parent_account',['Asset' => 'Asset', 'Expense' => 'Expense'])}}
                </div>
              </div>
              
              

              <div class="control-group">
                {{Form::label('max_tran_amnt','Maximum Tran. Amt.',['class'=>'control-label','title'=>'Maximum Tran. Amt.'])}}
                <div class="controls">
                  {{Form::text('transaction_amount',$accounts->transaction_amount,['id'=>'required','title'=>'transaction_amount','placeholder'=>'Maximum Tran. Amt.'])}}
                </div>
              </div>


              <div class="control-group">
                {{Form::label('transaction_type','Transaction Type',['class'=>'control-label','title'=>'transaction_type'])}}
                <div class="controls">
                  {{Form::select('transaction_type',['Check' => 'Check', 'Cash' => 'Cash'])}}
                </div>
              </div>

              

          <div class="form-actions">
                <input type="submit" value="UPDATE" class="btn btn-success">
              </div>
            {{Form::close()}}
          </div>
            </div>
@stop