@extends('admin.index')
@section('Title','Invoice')
@section('breadcrumbs','Invocie')
@section('breadcrumbs_link','/invoice')
@section('breadcrumbs_title','Invoice')
@section('content')

@if (Session::has('success'))
    <div class="alert alert-success alert-dismissible fade in">
            <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong> {{ Session::get('success') }}
    </div>
   
@endif


<div class="container">
<h2><i class="fa fa-money" aria-hidden="true"></i> &nbsp; Invoice Report</h2>
<!-- Tab Heading  -->
<p title="Transport Details">{{Session::get('school.system_name')}} Invoice's Report</p>
<!-- Transport Details -->



<div class="tab-content" >
  <!-- Transport List Report  -->
  <div id="home" class="tab-pane fade in active">
    <div class="widget-box">
      <div class="widget-title">
        <span class="icon">
          <i class="icon-th">
          </i>
        </span>
        <h5>Student Invoice Data Table
        </h5>
      </div>
      <div class="widget-content nopadding">
        <table class="table table-bordered data-table">
          
          <thead>
            <tr>
              <th>Invoice ID</th>
              <th>Class</th>
              <th>Student Roll</th>
              <th>On Net Total</th>
              <th>Waber</th>
              <th>Waber Purpose</th>
              <th>Payment</th>
              <th>Status</th>
              <th>Creator</th>
              <th>Created At</th>
              <th>Last Updated</th>
              <th>Action</th>
            </tr>
          </thead>

          <tbody>

          @foreach($invoice_data as $invoice_data_list)
            <tr class="gradeX">
              <td>{{$invoice_data_list->invoice_id}}</td>
              <td>{{$invoice_data_list->class_name}}</td>
              <td>{{$invoice_data_list->student_roll}}</td>
              <td>{{$invoice_data_list->on_net_total}}</td>
              <td>{{$invoice_data_list->waber}}</td>
              <td>{{$invoice_data_list->waber_purpose}}</td>
              <td>{{$invoice_data_list->Payment}}</td>
              <td>

    {{Form::open(['url'=>"/invoice/$invoice_data_list->invoice_id" ,'method'=>'GET'])}}
                  @if($invoice_data_list->cash_status=='Unpaid')
                      <input type="Submit" class="btn btn-danger" name="" value="{{$invoice_data_list->cash_status}}">
                  @else

                    <input type="Submit" class="btn btn-success" name="" value="{{$invoice_data_list->cash_status}}">
                  @endif
    {{Form::close()}}
                
              </td>
              <td>{{$invoice_data_list->invoice_creator}}</td>
              <td>{{$invoice_data_list->created_at}}</td>
              <td>{{$invoice_data_list->updated_at}}</td>
              <td id="my_align" class="display_status">

{{Form::open(['url'=>"/invoice_print/$invoice_data_list->invoice_id",'method'=>'GET'])}}
    {{ Form::submit('print',['class'=>'btn btn-primary'])}}
{{Form::close()}}



{{Form::open(['url'=>"invoice/$invoice_data_list->invoice_id",'method'=>'DELETE'])}}
    {{Form::submit('Delete',['class'=>'btn btn-danger applicant_student_delete','target'=>'_blank'])}}
{{Form::close()}}


              </td>
            </tr>
          @endforeach


          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>

@stop