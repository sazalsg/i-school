@extends('admin.index')
@section('Title','Invoice')
@section('breadcrumbs','Invocie')
@section('breadcrumbs_link','/invoice')
@section('breadcrumbs_title','Invoice')
@section('content')



<div class="container">
<h2><i class="fa fa-money" aria-hidden="true"></i> &nbsp; Payment History</h2>
<!-- Tab Heading  -->
<p title="Transport Details">I School Managment  Student Payment's Histoy Report</p>
<!-- Transport Details -->

<div class='container'>
	<select><option>Class</option></select> <select><option>Section</option></select> <select><option>Department</option></select>
	<select><option>Form Date</option></select> <select><option>To Date</option></select>
</div>

<div class="tab-content" >
  <!-- Transport List Report  -->
  <div id="home" class="tab-pane fade in active">
    <div class="widget-box">
      <div class="widget-title">
        <span class="icon">
          <i class="icon-th">
          </i>
        </span>
        <h5>Student Invoice Data Table
        </h5>
      </div>
      <div class="widget-content nopadding">
        <table class="table table-bordered data-table">
          <thead>
            <tr>
              <th>Name
              </th>
              <th>Email
              </th>
              <th>Password
              </th>
              <th>Phone
              </th>
              <th>Address
              </th>
              <th>Gender
              </th>
              <th>Profession
              </th>
              <th>Monthly Salery
              </th>
              <th>Anual Salery
              </th>
              <th>Photo
              </th>
              <th>Action
              </th>
            </tr>
          </thead>
          <tbody>
            <tr class="gradeX">
              <td>hasian akter
              </td>
              <td>hasina@gmail.com
              </td>
              <td>99997
              </td>
              <td>0181566561
              </td>
              <td>old police quater
              </td>
              <td>female
              </td>
              <td>Housewife
              </td>
              <td>000000
              </td>
              <td>000000
              </td>
              <td>no pic
              </td>
              <td class="center">
                <div class="dropdown">
                  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                    <span class="caret">
                    </span>
                  </button>
                  <ul class="dropdown-menu">
                    <li>
                      <a href="#">Edit
                      </a>
                    </li>
                    <li>
                      <a href="#">Delete
                      </a>
                    </li>
                    <li>
                      <a href="#">Admit Card
                      </a>
                    </li>
                  </ul>
                </div>
              </td>
            </tr>
            <tr class="gradeX">
              <td>rehana akter
              </td>
              <td>rehana@gmail.com
              </td>
              <td>99997
              </td>
              <td>0181566561
              </td>
              <td>old police quater
              </td>
              <td>female
              </td>
              <td>Housewife
              </td>
              <td>000000
              </td>
              <td>000000
              </td>
              <td>no pic
              </td>
              <td class="center">
                <div class="dropdown">
                  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                    <span class="caret">
                    </span>
                  </button>
                  <ul class="dropdown-menu">
                    <li>
                      <a href="#">Edit
                      </a>
                    </li>
                    <li>
                      <a href="#">Delete
                      </a>
                    </li>
                    <li>
                      <a href="#">Admit Card
                      </a>
                    </li>
                  </ul>
                </div>
              </td>
            </tr>
            <tr class="gradeX">
              <td>Sahajaidda akter
              </td>
              <td>sahajaidda@gmail.com
              </td>
              <td>99997
              </td>
              <td>0181566561
              </td>
              <td>old police quater
              </td>
              <td>female
              </td>
              <td>Housewife
              </td>
              <td>000000
              </td>
              <td>000000
              </td>
              <td>no pic
              </td>
              <td class="center">
                <div class="dropdown">
                  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                    <span class="caret">
                    </span>
                  </button>
                  <ul class="dropdown-menu">
                    <li>
                      <a href="#">Edit
                      </a>
                    </li>
                    <li>
                      <a href="#">Delete
                      </a>
                    </li>
                    <li>
                      <a href="#">Admit Card
                      </a>
                    </li>
                  </ul>
                </div>
              </td>
            </tr>
            <tr class="gradeX">
              <td>Shakilla akter
              </td>
              <td>Shakilla@gmail.com
              </td>
              <td>99997
              </td>
              <td>0181566561
              </td>
              <td>old police quater
              </td>
              <td>female
              </td>
              <td>Housewife
              </td>
              <td>000000
              </td>
              <td>000000
              </td>
              <td>no pic
              </td>
              <td class="center">
                <div class="dropdown">
                  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                    <span class="caret">
                    </span>
                  </button>
                  <ul class="dropdown-menu">
                    <li>
                      <a href="#">Edit
                      </a>
                    </li>
                    <li>
                      <a href="#">Delete
                      </a>
                    </li>
                    <li>
                      <a href="#">Admit Card
                      </a>
                    </li>
                  </ul>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>

@stop