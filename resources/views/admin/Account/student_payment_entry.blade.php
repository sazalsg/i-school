@extends('admin.index')
@section('Title','Student Payment Entry')
@section('breadcrumbs','Student Payment Entry')
@section('breadcrumbs_link','/student_payment_entry')
@section('breadcrumbs_title','Student Payment Entry')

@section('content')

@if (Session::has('success'))
    <div class="alert alert-success alert-dismissible fade in">
                <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong> {{ Session::get('success') }}
    </div>
   
@endif

@if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissible fade in">
        <ul  style='list-style:none'>
            @foreach ($errors->all() as $error)
                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> &nbsp;{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif



	<div class="container">
  		<h2><i class="fa fa-money" aria-hidden="true"></i> Create New Invoice</h2> <!-- Tab Heading  -->
 		<p title="Transport Details">{{Session::get('school.system_name')}}  Payment Details</p> <!-- Transport Details -->

			 

		  

<div class='row'>
     <div class="panel panel-default" >
      <div class="panel-body text-left">
         <ul class='dropdown_test'>
            <li><a href='/chart_of_account'><i class="fa fa-list-alt" aria-hidden="true"></i> &nbsp;Chart Of Account</a></li>
            <li><a href='/create_template'><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Payment Templete</a></li>
            
            <li><a href='/student_payment_entry'><i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;Student Invoice</a></li>
            
         </ul>
      </div>
    </div>



  <div style="margin-left: 2%" class="controls text-right">
            <div data-toggle="buttons-checkbox" class="btn-group">

              <button  class="btn btn-default" title='Export PDF' type="button"><a target="_blank" href="/"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></button>

              <button class="btn btn-default" title='Export Excel' type="button"><a  href="/"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a></button>

              <button class="btn btn-default" title='Preview' ttype="button"><a target="_blank" href="/"><i class="fa fa-street-view" aria-hidden="true"></i></a></button>
              
              <button id='print' class="btn btn-default" title='Print' type="button"><i class="fa fa-print" aria-hidden="true"></i></button>
            </div>
    </div>
</div>






		<div class="tab-content"> <!-- Transport List Report  -->
		    	
  {{Form::open(['url'=>'/student_payment_entry','files'=>true,'class'=>'form-horizontal','method'=>'post','name'=>'basic_validate','id'=>'basic_validate','novalidate'=>'novalidate'])}}
		    <div id="home" class="tab-pane fade in active">
		      	

		    	 <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>New Entry</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="#" name="basic_validate" id="basic_validate" novalidate="novalidate">

             	<p class='text-center'>Invoice information</p>


             

              <div class="control-group">
              <label class="control-label">Invoice Type</label>
              <div class="controls">
                <label>
                  <input type="radio" value="Single Invoice" id="invoice_type" class="invoice_type" name="invoice_type"/>
                  Single Invoice</label> 


                <label>
                  <input type="radio" value="Mass Invoice" id="s" class="invoice_type" name="invoice_type"/>
                  Mass Invoice</label> 

                
                
              </div>
            </div>



              
              <div class="control-group">
                <label class="control-label">Class</label>
                <div class="controls">
                @foreach($class_name as $class_name_value)
                  @php
                    $class_name_array[$class_name_value->class_name]=$class_name_value->class_name
                  @endphp
                @endforeach()
                  {{Form::select('class_name',$class_name_array,null,['class'=>'class_name'])}}
                </div>
              </div>
              
              <div class="control-group student_div">
                <label class="control-label">Student</label>
                <div class="controls">
                  
                  {{Form::select('student_name',[''=>'Set Roll or Student Name'],null,['id'=>'student_name','class'=>'student_name'])}}
                </div>
              </div>


              <div class="control-group roll_div">
                <label class="control-label">Roll</label>
                <div class="controls">
                  
                  {{Form::text('student_roll',null,['id'=>'student_roll','class'=>'student_roll'])}}
                </div>
              </div>
              
              

              <div class="control-group title_div">
                <label class="control-label">Titel</label>
                <div class="controls">
                  {{Form::text('title','',['id'=>'required','class'=>'title'])}}
                </div>
              </div>


              <div class="control-group">
                <label class="control-label">Date</label>
                <div class="controls">
                  {{Form::date('name', \Carbon\Carbon::now(),['disabled'=>'disabled'])}}
                </div>
              </div>

              

               <div class="control-group">
            {{Form::label('from_date','From Date(mm/dd)',['class'=>'control-label','title'=>'birth_date'])}}
                <div class="controls">
                <div  data-date="12-02-2012" class="input-append date datepicker">
                   {{Form::text('from_date',null,['data-date-format'=>'mm-dd-yyyy','style'=>'width:85%'])}}

                   <span class="add-on"><i class="icon-th"></i></span>
                  <!-- <input type="date">  -->
                  </div>
                </div>
            </div>


             <div class="control-group">
            {{Form::label('to_date','To Date(mm/dd)',['class'=>'control-label','title'=>'birth_date'])}}
                <div class="controls">
                <div  data-date="12-02-2012" class="input-append date datepicker">
                   {{Form::text('to_date',null,['data-date-format'=>'mm-dd-yyyy','style'=>'width:85%','class'=>'to_date'])}}

                   <span class="add-on"><i class="icon-th"></i></span>
                  <!-- <input type="date">  -->
                  </div>
                </div>

            </div>


            <div class="control-group title_div">
                <label class="control-label">Total Month</label>
                <div class="controls">
                  {{Form::text('total_month','',['id'=>'required','class'=>'total_month'])}}
                </div>
            </div>



              




              <p class='text-center'>Payment information</p><hr>


               
               <div id="templete_wise_component">
                  
                  

               </div>
                

              


              	<div class="control-group">
                	<label class="control-label" >Actual Total</label>
                	<div class="controls">
                  		{{Form::text('total',null,['class'=>'total_p_student'])}}
                	</div>
              	</div>


                <div class="control-group">
                  <label class="control-label" >On Net Total</label>
                  <div class="controls">
                      {{Form::text('on_net_total',null,['class'=>'on_net_total'])}}
                  </div>
                </div>




                <div class="control-group">
                  <label class="control-label">Waver</label>
                  <div class="controls">
                      {{Form::text('waber',null,['class'=>'waber_info'])}}
                      
                  </div>
                </div>


                <div id="waber" hidden="hidden" class="control-group">
                  <label class="control-label">Waver Purpose</label>
                  <div class="controls">
                      {{Form::text('waber_purpose',null,['class'=>'waber_purpose'])}}
                      
                  </div>
                </div>




              	<div class="control-group">
                	<label class="control-label">Pyment</label>
                	<div class="controls">
                      {{Form::text('Payment',null,['class'=>'payment_info'])}}
                  		
                	</div>
              	</div>

                <div class="control-group">
                  <label class="control-label"></label>
                  <div class="controls">
                      <span id="due_information"></span>
                  </div>
                </div>



                <div class="control-group">
                  <label class="control-label">Account</label>
                  <div class="controls"> 
                      {{Form::text('account_name',null,['class'=>'account_name_p_student','disabled'=>'disabled'])}}
                      {{Form::hidden('account_name',null,['class'=>'account_name_p_student_hidden'])}}
                  </div>
                </div>

              	<div class="control-group">
                	<label class="control-label">Status</label>
                	<div class="controls">
                  {{Form::select('or',['Paid','Unpaid'],null,['class'=>'payment_status'])}}
                  {{Form::hidden('cash_status',null,['class'=>'pay_2_status'])}}
                  	
                	</div>
              	</div>

              	<div class="control-group">
                	<label class="control-label">Method</label>
                	<div class="controls">
                  		<select><option>Cash</option><option>Check</option></select>
                	</div>
              	</div>


              

          <div class="form-actions">
                <input type="submit" value="Submit" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>


		    </div>
		    

{{Form::close()}}




		</div>
	</div>

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script type="text/javascript">

      $(document).ready(function(){
          
          $('.class_name').unbind().change(function(){
              var class_name=$(this).val();
              
              $.ajax({
                url: '/student_payment_entry_for_student_data',
                type: "post",
                data: {'class_name':class_name,'_token': $('input[name=_token]').val()},
                success: function(data){
                  console.log(data);
                  $('#student_name').html(data);
                  $('.student_roll').val();
                }
              });

              $("#templete_wise_component").html("");
              templete_desgin(class_name)
          });

          $('#student_name').unbind().change(function(){
              var student_roll=$(this).val();
              
                $.ajax({
                  url: '/notice_board_student_data_get',
                  type: "post",
                  data: {'student_roll':student_roll,'_token': $('input[name=_token]').val()},
                  success: function(data){
                    console.log(data);
                    $(".student_roll").val(data.roll);
                    $(".title").val(data.student_name+'/'+data.roll+'-'+data.class+'-Payment Invoice');
                    // $(".student_wise_email").val(data.email);
                    // $(".student_wise_class").val(data.class);
                    // $(".student_wise_section").val(data.section);

                    //$('#student_section').html(data);
                  }
                });
          });

          $('.student_roll').unbind().keyup(function(){
              var student_roll=$(this).val();
              
              $.ajax({
                  url: '/notice_board_student_data_get',
                  type: "post",
                  data: {'student_roll':student_roll,'_token': $('input[name=_token]').val()},
                  success: function(data){
                    if(data!="")
                    {
                      $(".class_name").val(data.class);
                      $(".student_name").html("<option>"+data.student_name+"</option>");
                      $(".title").val(data.student_name+'/'+data.roll+'-'+data.class+'-Payment Invoice');
                    }
                    // $(".student_wise_class").val(data.class);
                    // $(".student_wise_section").val(data.section);

                    //$('#student_section').html(data);
                    $("#templete_wise_component").html("");
                    templete_desgin(data.class);
                  }
                });
              
              
          });

          $('#invoice_type,#s').unbind().click(function(){
            
              var invoice_type=$(this).val();
              
              if(invoice_type=="Mass Invoice")
              {
                  $(".student_div").attr('hidden',true);
                  $(".roll_div").attr('hidden',true);
                  
                  $(".student_div").attr('disabled',true);
                  $(".roll_div").attr('disabled',true);
                  
              }
              else
              {
                  $(".student_div").attr('hidden',false);
                  $(".roll_div").attr('hidden',false);
                  

                  $(".student_div").attr('disabled',false);
                  $(".roll_div").attr('disabled',false);
                  
              }
          });

          $('.payment_info').unbind().keyup(function(){
              var total_p_student=$('.on_net_total').val();
              var waber=$('.waber_info').val();
              var payment_value=$(this).val();

              var different_value=(parseInt(total_p_student)-parseInt((waber)?waber:0))-parseInt(payment_value);
              
              if(different_value!=0)
              {
                  
                  $('#due_information').html("<p style='color:red'>Have This Invoice Due "+(isNaN(different_value)?0:different_value)+"</p>")

                  $('.payment_status').html("<option>Unpaid</option>");
                  $('.payment_status').attr('disabled','disabled');
                  $('.pay_2_status').val('Unpaid');
              }
              else
              {
                $('#due_information').html("<p style='font-color:red'></p>");
                 $('.payment_status').html("<option>Paid</option>");
                 $('.payment_status').attr('disabled','');
                 $('.pay_2_status').val('Paid');
              }
          });

          $('.waber_info').unbind().keyup(function(){
              if ($(this).val()!=0) {
                $('#waber').show();
              }
              else
              {
                  $('#waber').hide();
              }
          });



      });

      function templete_desgin(class_name=None)
      {
        $.ajax({
                url: '/student_templete_desgin',
                type: "post",
                data: {'class_name':class_name,'_token': $('input[name=_token]').val()},
                success: function(data){
                  if(data !="")
                  {
                    var obj = jQuery.parseJSON(data.templete_json);
                   

                    jQuery.each( obj.all_value, function( key, val ) {

                         $("#templete_wise_component").append("\
                    <div class=\"control-group\">\
                      <label class=\"control-label component_name\">"+key+"</label>\
                      <div class=\"controls\">\
                      <input type='hidden' value="+key+" class='component_name'>\
                      <input type='text' value="+val+" class='component_value'>\
                      </div>\
                    </div>");
                         $('.total_p_student').val(obj.total);
                         $('.account_name_p_student').val(obj.account_name);
                         $('.account_name_p_student_hidden').val(obj.account_name);
                         $('#from_date').val(data.from_date);
                         $('#to_date').val(data.to_date);
                         $('.total_month').val(data.total_month);

                         var actual_total=$('.total_p_student').val();
                         var total_month=data.total_month;
                         var on_net_t=parseInt(actual_total)*parseInt(total_month);
                         $('.on_net_total').val(on_net_t);

                     
                    });
                  }
                  else{
                    $("#templete_wise_component").html("<p style='color:red;text-align:center'>No Templete Not Found</p>");
                  }
                  
                }
              });
      }

      </script>

@stop
<!-- 

 var student_roll=$(".stuent_roll_wise_data").val();

         $.ajax({
            url: '/notice_board_student_data_get',
            type: "post",
            data: {'student_roll':student_roll,'_token': $('input[name=_token]').val()},
            success: function(data){

              $(".sw_student_name").val(data.student_name);
              $(".student_wise_email").val(data.email);
              $(".student_wise_class").val(data.class);
              $(".student_wise_section").val(data.section);

              //$('#student_section').html(data);
            }
          });
 -->


<!-- 

         $('.Admin_card').unbind().click(function(){
         var id = $(this).attr('aplicant_id');
         
         $.ajax({
            url: '/applicant_student_admit_card',
            type: "post",
            data: {'id':id,'_token': $('input[name=_token]').val()},
            success: function(data){
              $('#markshet').html(data);
            }
          });

        });




         $('.applicant_student_delete').unbind().click(function(){
             if( !confirm('Are you sure you want to continue?')) {
                    return false;
                }else
                    {
                     var id = $(this).attr('value');
                     $(this).closest('tr').remove();
                     $.ajax({
                        url: '/aplicant_student/'+id+'',
                        type: "DELETE",
                        data: {'id':id,'_token': $('input[name=_token]').val()},
                        success: function(data){


                        }
                      });
                 }
         

        }); -->