  @extends('admin.index')
@section('Title','Admit Student')
@section('breadcrumbs','Admit Student')
@section('breadcrumbs_link','/admit_student')
@section('breadcrumbs_title','Admit Student')

@section('content')
  
  @if (Session::has('success'))
    <div class="alert alert-success alert-dismissible fade in">
                <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong> {{ Session::get('success') }}
    </div>
   
@endif


  @if (Session::has('wrong'))
    <div class="alert alert-danger alert-dismissible fade in">
                <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Sorry!</strong> {{ Session::get('wrong') }}
    </div>
   
@endif


@if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissible fade in">
        <ul  style='list-style:none'>
            @foreach ($errors->all() as $error)
                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> &nbsp;{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   





  <div class="container">
      <h2><i class="fa fa-graduation-cap" aria-hidden="true"></i> Admit Student</h2> <!-- Tab Heading  -->
    <p title="Transport Details">{{Session::get('school.system_name')}}  Student Details</p> <!-- Transport Details -->

          <div class='row'>
         
         <div class="panel panel-default" >
          <div class="panel-body text-left">
             <ul class='dropdown_test'>
                <li><a href='/create_admin'><i class="fa fa-list-alt" aria-hidden="true"></i> &nbsp;Admission Test</a></li>
                <li><a href='/user_access'><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Student Promation</a></li>
                <li><a href='/permission_role'><i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;Student Promotion</a></li>
             </ul>
          </div>
        </div>



      <!-- <div class="controls text-right">
                <div data-toggle="buttons-checkbox" class="btn-group">
                  <button  class="btn btn-default" title='Export PDF' type="button"><a target="_blank" href="/applicant_student_pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></button>

                  <button class="btn btn-default" title='Export Excel' type="button"><a  href="/applicant_student_excel"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a></button>
                  
                  <button class="btn btn-default" title='Preview' ttype="button"><a target="_blank" href="/applicant_student_pdf"><i class="fa fa-street-view" aria-hidden="true"></i></a></button>
                  
                  <button id='print' class="btn btn-default" title='Print' type="button"><i class="fa fa-print" aria-hidden="true"></i></button>

                </div>
        </div> -->
    </div>

    <div class="alert alert-info">
      <strong>Warning!</strong> <br>If You Are Does Not Set Email And Password Then System Set Automatic Username and Password (Student Roll number Is By Default Username and Password)
    </div>

    
     <p><h3>{{$autoname}}</h3><p>

       <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>New Student</h5>
          </div>






          <div class="widget-content nopadding">
    {{Form::open(['url'=>'/admit_student','files'=>true,'class'=>'form-horizontal','method'=>'post','name'=>'basic_validate','id'=>'basic_validate','novalidate'=>'novalidate'])}}
           
              <div class="control-group">
    {{Form::label('student_name','Student Name',['class'=>'control-label','title'=>'student_name'])}}
                
                <div class="controls">
    {{Form::text('student_name','',['id'=>'required','placeholder'=>'Student Name','title'=>'student_name'])}}
                </div>
              </div>
              
              <div class="control-group">

              {{Form::label('parent_name','Gurdian Name',['class'=>'control-label','title'=>'parent_name'])}}
               
                <div class="controls">
                  @php $parents_data_array[]=""; @endphp
                  @foreach($parents_data as $parents_info)
                    @php $parents_data_array[$parents_info->parent_id]=$parents_info->name @endphp
                  @endforeach
                 {{Form::select('parent_name',$parents_data_array)}}
                </div>
              </div>


              <div class="control-group">
               {{Form::label('relation','Relation',['class'=>'control-label','title'=>'relation'])}}
                <div class="controls">
    {{Form::text('relation','',['id'=>'required','placeholder'=>'Relation','title'=>'relation'])}}
                </div>
              </div>


              <div class="control-group">
              {{Form::label('class','Class',['class'=>'control-label','title'=>'class'])}}
               
                <div class="controls">
                @foreach($class_data as $class_info)
                    @php $class_data_array[$class_info->class_name]=$class_info->class_name @endphp
                  @endforeach

                  {{Form::select('class',$class_data_array,null,['class'=>'manage_class','id'=>'manage_class'])}}
                </div>
              </div>

              <div class="control-group">
              {{Form::label('Section','Section',['class'=>'control-label','title'=>'class'])}}
               
                <div class="controls">
                @php $section["Please Frist Fill Class"]="Please Frist Fill Class" @endphp
                

                  {{Form::select('section',$section,null,['id'=>'student_section'])}}
                </div>
              </div>


              <div class="control-group">
              {{Form::label('Department','Department',['class'=>'control-label','title'=>'class'])}}
               
                <div class="controls">
                @php $department["Please Frist Fill Class"]="Please Frist Fill Class" @endphp

                  {{Form::select('department',$department,null,['id'=>'Manage_department'])}}
                </div>
              </div>


              
              <div class="control-group">
            {{Form::label('session','Session',['class'=>'control-label','title'=>'session'])}}
                <div class="controls">
                @php $session_list_array[$batch->default_session]=$batch->default_session @endphp
                 @foreach($session as $session_list)
                    @php $session_list_array[$session_list->type_name]=$session_list->type_name @endphp
                  @endforeach

                {{Form::select('session',$session_list_array)}}
                <span style="color: red">By Default Set Default Session</span>
                </div>
            </div>




              <div class="control-group">
              {{Form::label('Batch','Batch',['class'=>'control-label','title'=>'batcg'])}}
                <div class="controls">
                 
                  {{Form::select('batch',["$batch->default_batch"=>"$batch->default_batch"])}}
                </div>
            </div>


              <div class="control-group">
            {{Form::label('Shift','Shift',['class'=>'control-label','title'=>'batcg'])}}
                <div class="controls">
                 
                 @foreach($shift as $shift_list)
                   @php $shift_list_array[$shift_list->type_name]=$shift_list->type_name @endphp
                @endforeach

                  {{Form::select('shift',$shift_list_array)}}
                </div>
            </div>




              <div class="control-group">
              {{Form::label('Type','Student Type',['class'=>'control-label','title'=>'class'])}}
               
                <div class="controls">
                  {{Form::select('type',['Regular'=>'Regular','Irregular'=>'Irregular'])}}
                </div>
              </div>




              <!-- <div class="control-group">
               {{Form::label('roll','Roll',['class'=>'control-label','title'=>'roll'])}}
               <div class="controls">
    {{Form::text('roll','',['id'=>'required','placeholder'=>'Student Roll','title'=>'roll'])}}
                  
                </div>
              </div> -->


              <div class="control-group">
               {{Form::label('reg','Registration Number',['class'=>'control-label','title'=>'reg_number'])}}
               
                <div class="controls">
    {{Form::text('reg_number','',['id'=>'required','placeholder'=>'Student Reg','title'=>'reg_number'])}}
                  
                </div>
              </div>





              <div class="control-group">
            {{Form::label('birth_date','Birth Date(mm/dd)',['class'=>'control-label','title'=>'birth_date'])}}
                <div class="controls">
                <div  data-date="12-02-2012" class="input-append date datepicker">
                   {{Form::text('birth_date',null,['data-date-format'=>'mm-dd-yyyy','style'=>'width:85%'])}}

                   <span class="add-on"><i class="icon-th"></i></span>
                  <!-- <input type="date">  -->
                  </div>
                </div>
            </div>





              <div class="control-group">
              {{Form::label('gender','Gender',['class'=>'control-label','title'=>'gender'])}}
                <div class="controls">
                 {{Form::select('gender',['Male'=>'Male','Female'=>'Female'])}}
                </div>
              </div>

              <div class="control-group">
               {{Form::label('address','Address',['class'=>'control-label','title'=>'address'])}}
                <div class="controls">
                  <table class="table address">
                      <thead>
                        <tr>
                          <th>Post Office</th>
                          <th>Home District</th>
                          <th>Division</th>
                          <th>Village Name</th>
                        </tr>
                      </thead>
                      <tbody>
                       <tr>
                       <td>
     {{Form::text('post_office','',['id'=>'required','class'=>'table_text','placeholder'=>'Post Office','title'=>'post_office'])}}
                          
                       </td>
                          <td>
  {{Form::text('home_district','',['id'=>'required','class'=>'table_text','placeholder'=>'Home District','title'=>'home_district'])}}
                          
                          </td>
                           <td>
    {{Form::text('division','',['id'=>'required','class'=>'table_text','placeholder'=>'Division','title'=>'division'])}}
                           
                           </td>
                            <td>
    {{Form::text('village_name','',['id'=>'required','class'=>'table_text','placeholder'=>'Village Name','title'=>'village_name'])}}
                            
                            </td>
                        </tr>
                      </tbody>
                    </table>



                  </div>
              </div>

              <div class="control-group">
              {{Form::label('phone','Phone',['class'=>'control-label','title'=>'phone'])}}
                
                <div class="controls">
      {{Form::text('phone','',['id'=>'required','placeholder'=>'Phone','title'=>'phone'])}}
                  
                </div>
              </div>


              <div class="control-group">
              {{Form::label('mobile','Mobile',['class'=>'control-label','title'=>'mobile'])}}
               
                <div class="controls">
                 {{Form::text('mobile','',['id'=>'required','placeholder'=>'Mobile','title'=>'mobile'])}}
                  
                </div>
              </div>


              <div class="control-group">
              {{Form::label('email','Email',['class'=>'control-label','title'=>'email'])}}
               
                <div class="controls">
                 {{Form::email('email','',['id'=>'required','placeholder'=>'Email','title'=>'email'])}}
                 
                </div>
              </div>


              <div class="control-group">
              {{Form::label('password','Password',['class'=>'control-label','title'=>'password'])}}
               
                <div class="controls">
               {{Form::password('password', ['class' =>'form-control','onkeyup'=>'password_len_check()','id'=>'password','title'=>'password'])}}
                 <br>
                  <span id="help_block"></span>
            
                </div>
               
              </div>

              <div class="control-group">
              {{Form::label('confirm_password','Confirm Password',['class'=>'control-label','title'=>'confirm_password'])}}
              
                <div class="controls">
                {{Form::password('confirm_password', ['class' =>'form-control','onkeyup'=>'Password_match()','id'=>'password_confirm','name'=>'password_confirmation','title'=>'confirm_password'])}}<br>
                <span id="password_match"></span>
               
                </div>
              </div>



              <script type="text/javascript">

                   function readURL(input) {
                   
                    
                      if (input.files && input.files[0])
                      {
                          var reader = new FileReader();

                          reader.onload = function (e) {
                              $('#blah')
                                  .attr('src', e.target.result)
                                  .width(200)
                                  .height(200);
                          };

                          reader.readAsDataURL(input.files[0]);
                      }
                      

                    }
              </script>

              <div class="control-group">
                {{Form::label('photo','Photo',['class'=>'control-label','title'=>'photo'])}}
                
                <div class="controls">
                {{Form::file('student_image',['onchange'=>'readURL(this)'])}}
                  
                  </div>
              </div>

              <div class="control-group">
              {{Form::label('image','',['class'=>'control-label','title'=>'image'])}}
               
                <div class="controls"> 
                {{Form::image('img/blankavatar.png','Profile_image',['alt'=>'Your Image','class'=>'img-responsive img-circle blank_applicant_student_image','id'=>'blah','style'=>'width:19%'])}}
                  
                  </div>
              </div>

              



          <div class="form-actions">
           {{Form::submit('Submit',['value'=>'Submit','class'=>'btn btn-success'])}}
                
              </div>
              {{Form::close()}}
          
          </div>
        </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

 <script type="text/javascript">
 function password_len_check()
 {
    var password_length_for_password=$("#password").val().length;

     if(password_length_for_password < 8)
     {
     $("#help_block").html("<font color='red'>password weak</font>");
     }
     else
     {
     $("#help_block").html("<font color='green'>password strong</font>");
    }

}

   $(document).ready(function(){
                            $("#password_show_button").mouseup(function(){
                                
                                $("#password").attr("type", "password");
                            });
                            $("#password_show_button").mousedown(function(){
                                $("#password").attr("type", "text");

                            });
                        });


 function Password_match()
 {
   var password_get=$("#password").val();
   var confiram_password_get=$("#password_confirm").val();
   if(password_get==confiram_password_get)
   {
   $("#password_match").html("<font color='green'>password Match</font>");
   $("#submit_button").attr('disabled',false);
   }
   else
   {
   $("#password_match").html("<font color='red'>password Not Match</font>");
    $("#submit_button").attr('disabled',true);
   }
 }

   $(document).ready(function()
   {
    
    $(".manage_class").unbind().change(function()
     {
          
         var class_name=$("#manage_class").val();
         
         // var class_name=$("#class_name").val();
         // var department=$("#department_name").val()
         // var session=$("#session").val();
          
         $.ajax({
            url: '/class_w_section_filter',
            type: "post",
            data: {'class_name':class_name,'_token': $('input[name=_token]').val()},
            success: function(data){
              
              $('#student_section').html(data);
            }
          });

         $.ajax({
            url: '/class_w_department_filter',
            type: "post",
            data: {'class_name':class_name,'_token': $('input[name=_token]').val()},
            success: function(data){
              
              $('#Manage_department').html(data);
            }
          });

      });
  });

  

 </script>





@stop
