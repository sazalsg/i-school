@extends('admin.index')
@section('Title','Applicant Student')
@section('breadcrumbs','Applicant Student > Applicant Student Edit')
@section('breadcrumbs_link','/aplicant_student')
@section('breadcrumbs_title','Applicant Student Edit')

@section('content')
     

@if (Session::has('success'))
    <div class="alert alert-success alert-dismissible fade in">
                <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong> {{ Session::get('success') }}
    </div>
   
@endif


@if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissible fade in">
        <ul  style='list-style:none'>
            @foreach ($errors->all() as $error)
                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> &nbsp;{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   




  <div class="container">
      <h2><i class="fa fa-user-plus" aria-hidden="true"></i> Aplicant Student Edit	</h2> <!-- Tab Heading  -->
      <p title="Transport Details">{{ Session::get('school.system_name') }}  Student Details</p> <!-- Transport Details -->
    
      
       <div class='row'>
         
         <div class="panel panel-default" >
          <div class="panel-body text-left">
             <ul class='dropdown_test'>
                <li><a href='/create_admin'><i class="fa fa-list-alt" aria-hidden="true"></i> &nbsp;Admission Test</a></li>
                <li><a href='/user_access'><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Student Promation</a></li>
                <li><a href='/permission_role'><i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;Student Promotion</a></li>

                <li><a href='/applicant_student_report'>&nbsp;<i class="fa fa-backward" aria-hidden="true"></i></a></li>
             </ul>
          </div>
        </div>



      <div class="controls text-right">
                <div data-toggle="buttons-checkbox" class="btn-group">
                  <button  class="btn btn-default" title='Export PDF' type="button"><a target="_blank" href="/applicant_student_pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></button>

                  <button class="btn btn-default" title='Export Excel' type="button"><a  href="/applicant_student_excel"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a></button>
                  
                  <button class="btn btn-default" title='Preview' ttype="button"><a target="_blank" href="/applicant_student_pdf"><i class="fa fa-street-view" aria-hidden="true"></i></a></button>
                  
                  <button id='print' class="btn btn-default" title='Print' type="button"><i class="fa fa-print" aria-hidden="true"></i></button>

                </div>
        </div>
    </div>
    <!-- From Heading Part End -->

      
    <div class="widget-box">
        <div class="widget-title">
          <span class="icon"> <i class="icon-info-sign"></i></span>
          <h5>New Student</h5>
        </div>

        <div class="widget-content nopadding">
        {{Form::open(['url'=>"aplicant_student/$applicant_student->applicant_id",'class'=>'form-horizontal','method'=>'put','files'=>true,'name'=>'basic_validate','id'=>'basic_validate','novalidate'=>'novalidate'])}}
            
            <div class="control-group" hidden="hidden">
            {{Form::label('applicant_id','Applicant Id',['class'=>'control-label','title'=>'applicant_id'])}}
                <div class="controls">
                  
                {{Form::text('applicant_id',$applicant_student->applicant_id,['id'=>'required','placeholder'=>'Applicant Id','title'=>'applicant_id'])}}
                </div>
            </div>


            

            <div class="control-group">
            {{Form::label('student_name','Student Name',['class'=>'control-label','title'=>'student_name'])}}
                <div class="controls">
                  
                {{Form::text('student_name',$applicant_student->student_name,['id'=>'required','placeholder'=>'Student Name','title'=>'student_name'])}}
                </div>
            </div>
              
            <div class="control-group">
            {{Form::label('parent_name','Parent Name',['class'=>'control-label','title'=>'parent_name'])}}
                <div class="controls">
             
            @php $parent_info[$applicant_student->parent_name]=$applicant_student->parent_name @endphp
                  @foreach($parents_data as $parents_data_list)
                    @php $parent_info[$parents_data_list->name]=$parents_data_list->name @endphp
                  @endforeach

                  {{Form::select('parent_name',$parent_info)}}
                </div>
            </div>

            <div class="control-group">
            {{Form::label('relation','Relation',['class'=>'control-label','title'=>'relation'])}}
                <div class="controls">
                  {{Form::text('relation',$applicant_student->relation,['id'=>'required','placeholder'=>'Relation','title'=>'relation'])}}
                </div>
            </div>


            <div class="control-group">
            {{Form::label('session','Session',['class'=>'control-label','title'=>'session'])}}
                <div class="controls">
                @php $session_list_array[$applicant_student->session]=$batch->default_session @endphp
                 @foreach($session as $session_list)
                    @php $session_list_array[$session_list->type_name]=$session_list->type_name @endphp
                  @endforeach

                {{Form::select('session',$session_list_array)}}
                <span style="color: red">By Default Set Default Session</span>
                </div>
            </div>




            <div class="control-group">
            {{Form::label('class','Class',['class'=>'control-label','title'=>'class'])}}
                <div class="controls">
                	@php $class_array[$applicant_student->class]=$applicant_student->class @endphp
                  
                  @foreach($manage_class as $manage_class_data)
                    @php $class_array[$manage_class_data->class_name]=$manage_class_data->class_name @endphp
                  @endforeach
                  	{{Form::select('class',$class_array)}}
                </div>
            </div>

            <div class="control-group">
            {{Form::label('admission_test','Admission Test',['class'=>'control-label','title'=>'admission_test'])}}
                <div class="controls">
                	@php $admission_test[$applicant_student->admission_test]=$applicant_student->admission_test @endphp

                  @foreach($exam_lsit as $exam_lsit_data)
                   @php $admission_test[$exam_lsit_data->exam_name]=$exam_lsit_data->exam_name @endphp
                  @endforeach

                  	{{Form::select('admission_test',$admission_test)}}
                </div>
            </div>

              
            <div class="control-group">
          {{Form::label('department','Department',['class'=>'control-label','title'=>'department'])}}
                <div class="controls">
                	@php @ $department[$applicant_student->department]=$applicant_student->department @endphp
                  @foreach($department_data as $department_data_list)
      @php $department[$department_data_list->department_name]=$department_data_list->department_name @endphp
                  @endforeach
                  {{Form::select('department',$department)}}
                </div>
            </div>


            <div class="control-group">
            {{Form::label('Batch','Batch',['class'=>'control-label','title'=>'batcg'])}}
                <div class="controls">
                 
                  {{Form::select('batch',["$applicant_student->batch"=>$applicant_student->batch,"$batch->default_batch"=>$batch->default_batch])}}
                </div>
            </div>


            <div class="control-group">
            {{Form::label('Shift','Shift',['class'=>'control-label','title'=>'batcg'])}}
                <div class="controls">
              @php $shift_list_array[$applicant_student->shift]=$applicant_student->shift @endphp
                 @foreach($shift as $shift_list)
                   @php $shift_list_array[$shift_list->type_name]=$shift_list->type_name @endphp
                @endforeach

                  {{Form::select('shift',$shift_list_array)}}
                </div>
            </div>


           


            <div class="control-group">
            {{Form::label('birth_date','Birth Date(mm/dd)',['class'=>'control-label','title'=>'birth_date'])}}
                <div class="controls">
                <div  data-date="12-02-2012" class="input-append date datepicker">
                   {{Form::text('birth_date',$applicant_student->birth_date,['data-date-format'=>'mm-dd-yyyy','style'=>'width:85%'])}}

                   <span class="add-on"><i class="icon-th"></i></span>
                  <!-- <input type="date">  -->
                  </div>
                </div>
            </div>


            <div class="control-group">
            {{Form::label('gender','Gender',['class'=>'control-label','title'=>'gender'])}}
                <div class="controls">
                @php  $gender[$applicant_student->gender]=$applicant_student->gender;
                      $gender['Male']='Male';
                      $gender['Female']='Female';
                @endphp
                  {{Form::select('gender',$gender)}}
                </div>
            </div>


            <div class="control-group">
            {{Form::label('address','Address',['class'=>'control-label','title'=>'address'])}}
                <div class="controls">
                    <table class="table address">
                        <thead>
                          <tr>
                            <th>Post Office</th>
                            <th>Home District</th>
                            <th>Division</th>
                            <th>Village Name</th>
                          </tr>
                        </thead>
                        
                        <tbody>
                          <tr>
                              <td>{{Form::text('post_office',$aplicant_student_child->post_office,['id'=>'required','class'=>'table_text','placeholder'=>'Post Office','title'=>'post_office'])}}</td>
                              <td>{{Form::text('home_district',$aplicant_student_child->home_district,['id'=>'required','placeholder'=>'Home District','title'=>'home_district','class'=>'table_text'])}}</td>
                              <td>{{Form::text('division',$aplicant_student_child->division,['id'=>'required','placeholder'=>'Division','title'=>'division','class'=>'table_text'])}}</td>
                              <td>{{Form::text('village_name',$aplicant_student_child->village_name,['id'=>'required','placeholder'=>'Village Name','title'=>'village_name','class'=>'table_text'])}}</td>
                          </tr>
                        </tbody>
                    </table>
                </div>
            </div>


            <div class="control-group">
            {{Form::label('postal_code','Postal Code',['class'=>'control-label','title'=>'postal_code'])}}
              <div class="controls">
                {{Form::text('postal_code',$applicant_student->postal_code,['id'=>'required','placeholder'=>'Postal Code','title'=>'postal_code'])}}
              </div>
            </div>

            
            <div class="control-group">
            {{Form::label('phone','Phone',['class'=>'control-label','title'=>'phone'])}}
              <div class="controls">
                  {{Form::text('phone',$applicant_student->phone,['id'=>'required','placeholder'=>'Phone','title'=>'phone'])}}
              </div>
            </div>


            <div class="control-group">
            {{Form::label('email','Email',['class'=>'control-label','title'=>'email'])}}
              <div class="controls">
              {{Form::email('email',$applicant_student->email,['id'=>'required','placeholder'=>'Email','title'=>'email'])}}
              </div>
            </div>

            <script type="text/javascript">
              function readURL(input) {
                if (input.files && input.files[0])
                    {
                      var reader = new FileReader();
                      reader.onload = function (e)
                      {
                              $('#blah')
                                  .attr('src', e.target.result)
                                  .width(200)
                                  .height(200);
                      };
                      reader.readAsDataURL(input.files[0]);
                    }
              }
            </script>


            <div class="control-group">
            {{Form::label('photo','Photo',['class'=>'control-label','title'=>'photo'])}}
                <div class="controls">
                {{Form::file('image',['onchange'=>'readURL(this)'])}}
                </div>
            </div>

            <div class="control-group">
            {{Form::label('','',['class'=>'control-label','title'=>''])}}
                <div class="controls"> <!-- img/blankavatar.png -->
                    @php
                        if(file_exists("img/backend/aplicant_student/$applicant_student->applicant_id.jpg"))
                    {
                        $image_path="img/backend/aplicant_student/$applicant_student->applicant_id.jpg";
                    }
                    else
                    {
                        $image_path="img/blankavatar.png";
                    }
                    @endphp
                {{Form::image($image_path,'Profile_image',['alt'=>'No Image Found','class'=>'img-responsive img-circle blank_applicant_student_image','id'=>'blah','style'=>'width:19%'])}}
                </div>
            </div>

            <div class="form-actions">
            {{Form::submit('Submit',['value'=>'Submit','class'=>'btn btn-success'])}}
            </div>
        {{Form::close()}}
        </div>
      </div>
    </div>
@stop