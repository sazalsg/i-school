@extends('admin.index')
@section('Title','Admit Bulk Student')
@section('breadcrumbs','Admit Bulk Student')
@section('breadcrumbs_link','/admit_bulk_student')
@section('breadcrumbs_title','admit_bulk_student')

@section('content')
    


@if (Session::has('success'))
    <div class="alert alert-success alert-dismissible fade in">
                <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong> {{ Session::get('success') }}
    </div>
   
@endif


@if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissible fade in">
        <ul  style='list-style:none'>
            @foreach ($errors->all() as $error)
                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> &nbsp;{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif



        <div class="container">
                    <h2><i class="fa fa-address-card-o" aria-hidden="true"></i> Admit Bulk Student </h2> <!-- Tab Heading  -->
                     <p title="Transport Details">I School Managment Addmission Pass Student Entry</p> <!-- Transport Details -->
        </div>

    <div class="container">
        <div class="text-right">
          <div class="panel panel-default text-right" >
      <div class="panel-body">
         <ul class='dropdown_test'>
            <li><a href='/applicant_student_report'><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;Send Result By Email</a></li>
            <li><a href='#'><i class="fa fa-comment" aria-hidden="true"></i>&nbsp;Send Result By Sms</a></li>
            <li><a href='#'><i class="fa fa-table" aria-hidden="true"></i>&nbsp;Tabulation Sheet </a></li>
             <li><a href='#'><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp; Cheack More Addmission Information</a></li>
        </ul>
      </div>
    </div>
        </div>
    </div>    
<br>




 <div class="alert alert-info">
      <strong>Warning!</strong> <br>System Automatic Set Default Batch For New Student Admited

      After Complete Admit Bulk Student Operation Must be Set All Information For This Student If Dont set Mandatory Information this student deprived our felicity
    </div>

{{Form::open(['url'=>'/admit_bulk_student','files'=>true,'class'=>'form-horizontal','method'=>'post','name'=>'basic_validate','id'=>'basic_validate','novalidate'=>'novalidate'])}}
  
    <div class='container'>
    <p style="color: red">Data Sequence :  Class Name > Section Name > Department > Shift </p>

    @php $class_data_array[""]="Select Class" @endphp
    @foreach($class as $class_data)
        @php $class_data_array[$class_data->class_name]=$class_data->class_name @endphp
   @endforeach
        
    {{Form::select('class',$class_data_array,null,['class'=>'manage_class','id'=>'manage_class','style'=>'width:20%'])}}
   {{Form::select('section',[''=>'Select Class First'],null,['id'=>'student_section','style'=>'width:20%'])}}
       <!-- <select>
            <option>Select Section</option>
        </select> -->

    {{Form::select('department',[''=>'Select Class First'],null,['id'=>'Manage_department','style'=>'width:20%'])}}


    @php $shift_data_array[""]="" @endphp
    @foreach($shift_data as $shift_data_list)
      @php $shift_data_array[$shift_data_list->type_name]=$shift_data_list->type_name @endphp
    @endforeach

    {{Form::select('shift',$shift_data_array,null,['id'=>'mange_shift','style'=>'width:20%'])}}

        <!-- <select>
            <option>Select Department</option>
        </select> -->
    </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

<script type="text/javascript">

    $(document).ready(function() {
    var max_fields      = 50; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
    var max_student_id=0;
    var batch=0;
    var year=0;
   
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click


       function set_autoname()
       {

          var current_id=parseInt(max_student_id)+1;
          max_student_id=current_id;
          var current_id_convert_to_string=current_id.toString();
          var repeat_str_len=4-parseInt(current_id_convert_to_string.length);
          var repeat_string="0".repeat(repeat_str_len)+current_id_convert_to_string;

          var new_id=year+batch+repeat_string;
          
        $(".test").last().find('.b_s_roll').val(new_id);
       }


        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append("<div class='test'><input type='text' class='b_s_name' placeholder='Student Name' name='student_name[]'><input type='text' class='b_s_roll' disabled placeholder='Student Roll' name='roll[]'><input type='hidden' class='b_s_roll' placeholder='Student Roll' name='roll[]'><input type='text' class='b_s_reg' placeholder='Student Reg' name='reg_number[]'>&nbsp;<a href='#' style='margin-bottom: 2px;margin-top: 3px;'  class='remove_field btn btn-danger'><i class='fa fa-trash' aria-hidden='true'></i></a></div>"); //add input box
        }
        else
        {
            alert('Only 10 Fields Are Allowed')
        }



        if(max_student_id==0)
       {
         $.ajax({
            url: '/admit_bulk_student_autoname_genrate',
            type: "post",
            data: {'_token': $('input[name=_token]').val()},
            success: function(data){
                
                 max_student_id=data.student_id;
                 year=data.current_year;
                 batch=data.default_batch.default_batch;
                set_autoname();
            }
          });
       }
       else
       {
        set_autoname();
       }




    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
</script>

<div class="input_fields_wrap text-center" style="padding:2%">
    
    <div>
    {{Form::text('student_name[]','',['id'=>'required','class'=>'b_s_name','placeholder'=>'Student Name','title'=>'student_name','style'=>'width:14%'])}}
     
    {{Form::text('roll[]',$generate_autoname,['id'=>'required','class'=>'b_s_roll','placeholder'=>'Student Roll','title'=>'student_roll','disabled'])}}

    {{Form::hidden('roll[]',$generate_autoname,['id'=>'required','class'=>'b_s_roll','placeholder'=>'Student Roll','title'=>'student_roll'])}}
        
    {{Form::text('reg_number[]','',['id'=>'required','class'=>'b_s_reg','placeholder'=>'Student Reg','title'=>'student_reg'])}}  
    
    &nbsp;<a href='#' style="margin-bottom: 2px;margin-top: 3px;" class='remove_field btn btn-danger'><i class='fa fa-trash' aria-hidden='true'></i></a>
     </div>
</div>
<div class='container text-center'>

{{Form::submit('Add More Fields',['class'=>'add_field_button btn btn-info','data-original-title'=>'Tabulation Sheet'])}}
   {{Form::submit('Save Students',['class'=>'btn btn-success ','data-original-title'=>'Save Students'])}}
    <!-- <input type="submit" class="add_field_button btn btn-info">Add More Fields
 -->
   </div>
<div class='container text-center'>

<!-- <button type='submit' class="btn btn-success"><i class="fa fa-check" aria-hidden="true"></i> &nbsp;Save Student</button> -->
</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

 <script type="text/javascript">

     $(document).ready(function()
    {
    
        $(".manage_class").unbind().change(function()
        {   
          
         var class_name=$("#manage_class").val();
         
         // var class_name=$("#class_name").val();
         // var department=$("#department_name").val()
         // var session=$("#session").val();
          
         $.ajax({
            url: '/class_w_section_filter',
            type: "post",
            data: {'class_name':class_name,'_token': $('input[name=_token]').val()},
            success: function(data){
              
              $('#student_section').html(data);
            }
          });

         $.ajax({
            url: '/class_w_department_filter',
            type: "post",
            data: {'class_name':class_name,'_token': $('input[name=_token]').val()},
            success: function(data){
              
              $('#Manage_department').html(data);
            }
          });

        });
  });

  

 </script>
{{Form::close()}}
@stop