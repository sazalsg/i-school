@extends('admin.index')
@section('Title','Student Promation')
@section('breadcrumbs','Student Promation')
@section('breadcrumbs_link','/student_promoation')
@section('breadcrumbs_title','Student Promoation')

@section('content')


@if (Session::has('success'))
    <div class="alert alert-success alert-dismissible fade in">
                <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong> {{ Session::get('success') }}
    </div>
   
@endif


@if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissible fade in">
        <ul  style='list-style:none'>
            @foreach ($errors->all() as $error)
                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> &nbsp;{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
@if (Session::has('error'))
    <div class="alert alert-danger alert-dismissible fade in">
                <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Error!</strong> {{ Session::get('error') }}
    </div>
   
@endif


        <div class="container">
					<h2><i class="fa fa-line-chart" aria-hidden="true"></i> Student Promoation  </h2> <!-- Tab Heading  -->
 					<p title="Transport Details">I School Managment Student Promoation</p> <!-- Transport Details -->
		</div>

	<div class="container">
		<h3>Note</h3>
		<div class='text-info'>
				Promoting student from the present class to the next class will create an enrollment of that student to the next session. Make sure to select correct class options from the select menu before promoting.If you don't want to promote a student to the next class, please select that option. That will not promote the student to the next class but it will create an enrollment to the next session but in the same class.<br><br>
		</div>
	</div>

	<div class="container">
		<div class="text-right">
			     <div class="panel panel-default text-right" >
      <div class="panel-body">
         <ul class='dropdown_test'>
            <li><a href='/applicant_student_report'><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add New Student</a></li>
            <li><a href='#'><i class="fa fa-comment" aria-hidden="true"></i>&nbsp;Send Result By Sms</a></li>
            <li><a href='#'><i class="fa fa-table" aria-hidden="true"></i>&nbsp;Student Admission </a></li>
             <li><a href='#'><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;Student Information</a></li>
        </ul>
      </div>
    </div>
		</div>
	</div>


	<div class='container'>
		<div class="widget-box promoation" >
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Student Promoation</h5>
          </div>
          <div class="widget-content nopadding">
    {{Form::open(['class'=>'form-horizontal','method'=>'PUT','name'=>'basic_validate','id'=>'basic_validate','novalidate'=>'novalidate'])}}

            
              <div class="control-group">
    {{Form::label('current_session','Current Session',['class'=>'control-label','title'=>'current_session'])}}
               
                <div class="controls">
                    @foreach($session_filter as $session_filter_data)
                       @php $session_array[$session_filter_data->session]=$session_filter_data->session @endphp
                    @endforeach
                    
                 {{Form::select('current_session',$session_array,null,['id'=>'current_session'])}}
                 	
                </div>
              </div>
              
<!--
              
-->
              
              
          <!--     
              
               <div class="control-group">
              {{Form::label('current_department','Current Department',['class'=>'control-label','title'=>'current_department'])}}
                
                <div class="controls">
                     @foreach($current_department as $current_department_data)
                       @php $current_department_array[$current_department_data->department]=$current_department_data->department @endphp
                    @endforeach
                 	   {{Form::select('current_department',$current_department_array,null,['class'=>'manage_promoation','id'=>'current_department'])}}
         		</div>
              </div>
               -->
              
              
             
              
              
              
              
              
              
              
              <div class="control-group">
              {{Form::label('current_class','Current Class',['class'=>'control-label','title'=>'promote_from_class'])}}
             
                <div class="controls">
                    @foreach($class_filter as $class_filter_data)
                       @php $class_array[$class_filter_data->class]=$class_filter_data->class @endphp
                    @endforeach
                 {{Form::select('current_class',$class_array,null,['class'=>'manage_promoation','id'=>'current_class'])}}
                </div>
              </div>
              
              
              <div class="control-group">
              {{Form::label('promote_class','Promote To Class',['class'=>'control-label','title'=>'promote_to_class'])}}
                <!-- <label class="control-label">Promotion To Class</label> -->
                <div class="controls">
                     @foreach($promote_class as $promote_class_data)
                       @php $promote_class_array[$promote_class_data->class_name]=$promote_class_data->class_name @endphp
                    @endforeach
                 	   {{Form::select('Class',$promote_class_array,null,['class'=>'manage_promoation','id'=>'promote_class'])}}
         		</div>
              </div>
              
              
             


              



					<div class="form-actions text-center">
                        

                <!-- <input type="submit" value="Manage Promotion" class="btn btn-primary"> -->

              </div>
        
          </div>
        </div>
	</div>
{{Form::close()}}



    {{Form::open(['url'=>'/student_promoation/student-Promation','class'=>'form-horizontal','method'=>'PUT','name'=>'basic_validate','id'=>'basic_validate','novalidate'=>'novalidate'])}}
	<div class='text-center'><h3>Stuent Of Class One</h3><hr></div>
	<div class='container'>
		



		<div id="home" class="tab-pane fade in active">
           
		      		<div class="widget-box">
          				<div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            				<h5>Class One Student Data Table</h5>
          				</div>

          		<div class="widget-content nopadding">
		              <span id="manage_promotion"></span>
          			</div>
        		</div>
             {{Form::submit('Manage Promotion',['class'=>'btn tip-top','data-original-title'=>'Manage Promotion'])}} 
		    </div>
	</div>
{{Form::close()}}





<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

 <script type="text/javascript">

   $(document).ready(function()
   {
    
    $(".manage_promoation").unbind().change(function()
     {
        
        var current_department=$("#current_department").val();
         var current_class=$("#current_class").val();
         var promote_class=$("#promote_class").val();
         var current_session=$("#current_session").val();
//         var promote_session=$("#promote_session").val();
         
        
          
         $.ajax({
            url: '/student_promoation',
            type: "post",
            data: {'current_department':current_department,'current_class':current_class,'promote_class':promote_class,'current_session':current_session,'_token': $('input[name=_token]').val()},
            success: function(data){
              $('#manage_promotion').html(data);
             
            }
          });

      

      });
  });

  

 </script>

@stop