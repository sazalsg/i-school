@extends('admin.index')
@section('Title','Academic Syllabus')
@section('breadcrumbs','Academic Syllabus')
@section('breadcrumbs_link','/academic_syllabus')
@section('breadcrumbs_title','Academic Syllabus')

@section('content')

@if (Session::has('success'))
    <div class="alert alert-success alert-dismissible fade in">
                <a href=" " class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong> {{ Session::get('success') }}
    </div>
   
@endif

@if (Session::has('Error'))
    <div class="alert alert-danger alert-dismissible fade in">
                <a href=" " class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Error!</strong> {{ Session::get('Error') }}
    </div>
   
@endif


@if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissible fade in">
        <ul  style='list-style:none'>
            @foreach ($errors->all() as $error)
                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> &nbsp;{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif




<div hidden="hidden" class="modal fade" id="myModal" style="width: 50%; height: 80%" role="dialog">
         <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-user-circle-o" aria-hidden="true"></i>&nbsp;File View</h4>
                </div>

               <div id='file_div'>sss</div>


            </div>
      </div>
</div>





<div class="container">
    <h2><i class="fa fa-window-restore" aria-hidden="true"></i>Academic Syllabus</h2> <!-- Tab Heading  -->
    <p title="Transport Details">{{Session::get('school.system_name')}}( {{Session::get('school.school_eiin')}} Details</p><br/><!-- Transport Details -->

     <div class='row'>
       
       <div class="panel panel-default" >
          <div class="panel-body text-left">
             <ul class='dropdown_test'>
              <li><a href='/manage_class'><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Manage Class</a></li>
                <li><a href='/manage_section'><i class="fa fa-plus-square-o" aria-hidden="true"></i> &nbsp;Manage Section</a></li>
                <li><a href='/manage_department'><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Manage Department</a></li>
                
             </ul>
          </div>
        </div>



      <div class="controls text-right">
                <div data-toggle="buttons-checkbox" class="btn-group">
                  <button  class="btn btn-default" title='Export PDF' type="button"><a target="_blank" href="/academic_syllabus_pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></button>
                  <button class="btn btn-default" title='Export Excel' type="button"><a  href="/academic_syllabus_excle"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a></button>
                  <button class="btn btn-default" title='Preview' ttype="button"><a target="_blank" href="/academic_syllabus_pdf"><i class="fa fa-street-view" aria-hidden="true"></i></a></button>
                 
                  <button id='print' class="btn btn-default" title='Print' type="button"><i class="fa fa-print" aria-hidden="true"></i></button>
                </div>
        </div>
      </div>




      <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home"><i class="fa fa-bars" aria-hidden="true"></i> Academic Syllabus</a></li>
        <li><a data-toggle="tab" href="#menu1"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Academic Syllabus</a></li>
      </ul>



  <div class="tab-content"> <!-- Transport List Report  -->

        <div id="home" class="tab-pane fade in active">
            <div class="widget-box">
                <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                  <h5>Data table</h5>
                </div>

               
              <div class="widget-content nopadding">
               <table class="table table-bordered data-table font_my">

                   <thead>
                    <tr>
                      <th>Title </th>
                      <th>Description</th>
                      <th>Subject</th>
                      <th>File Name</th>
                      <th>File Preview</th>
                       <th>Action</th>
                    </tr>

                   </thead>
                   <tbody>

                  @foreach($academic_syllabus as $academic_syllabus_list)
                     <tr class="gradeX">
                        <td>{{$academic_syllabus_list->title}}</td>
                        <td>{{$academic_syllabus_list->description}}</td>
                        <td>{{$academic_syllabus_list->subject}}</td>
                        <td>{{$academic_syllabus_list->class_name.".pdf"}}</td>
                       

                        <td > <a style="font-size: 25px;color: red;margin-left: 40%;" file_extension="" file_title="{{$academic_syllabus_list->class_name}}" data-toggle='modal' data-target='#myModal' class="file_jquery" id="file_jquery" href="#">
          <i  class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
                       
                        <td class="center">

                          <div class="display_status">
                                    {{Form::open(['url'=>"academic_syllabus/$academic_syllabus_list->id/edit" ,'method'=>'GET'])}}
                                    {{Form::submit('Edit',['class'=>'btn btn-primary'])}} 
                                    {{Form::close()}}
                                    

                                    {{Form::button('DELETE',['class'=>'btn btn-danger applicant_student_delete','value'=>$academic_syllabus_list->id,])}}

                                    {{Form::open(['url'=>"academic_syllabus_download/$academic_syllabus_list->class_name" ,'method'=>'GET'])}}
                                    {{Form::submit('Download',['class'=>'btn btn-danger'])}}
                                    {{Form::close()}}

                      </td>
                     </tr>
                  @endforeach
                    

                    

                   </tbody>
                 </table>
               </div>

               
              
          </div>
      </div>
     

      <div id="menu1" class="tab-pane fade">
        <div>
      
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
              <h5>Academic Syllabus</h5>
            </div>
        
            <div class="widget-content nopadding">
        {{Form::open(['url'=>'/academic_syllabus','class'=>'form-horizontal','files'=>True,'method'=>'post','name'=>'basic_validate','id'=>'basic_validate','novalidate'=>'novalidate'])}}
           
            
              <div class="control-group">
                
              {{Form::label('Title','',['class'=>'control-label'])}}
                <div class="controls">
                  {{Form::text('title','',['title'=>'title'])}}
                </div>
              </div>

              <div class="control-group">
                
                {{Form::label('description','',['class'=>'control-label'])}}
                <div class="controls">
                {{Form::text('description','',['id'=>'description','title'=>'description'])}}
                </div>
              </div>

              <div class="control-group">
              {{Form::label('class_name','Class Name',['class'=>'control-label','title'=>'class_name'])}}
                <div class="controls">
                  
                  
                @foreach($manage_class as $manage_class_list)
                    @php $class[$manage_class_list->class_name]=$manage_class_list->class_name @endphp
                  @endforeach

                  {{Form::select('class_name',$class,null,['id'=>'class_name'])}}
                </div>
            </div>

              <div class="control-group">
                
              {{Form::label('Subject','',['class'=>'control-label'])}}
                <div class="controls">
                 {{Form::select('subject',['Subject Name'=>'Subject Name'],null,['id'=>'subject'])}}
                </div>
              </div>

            <div class="control-group">
            {{Form::label('academic_syllebus_file','Academic Syllebus File',['class'=>'control-label','title'=>'queston_file'])}}
                <div class="controls">
                {{Form::file('files',['onchange'=>'readURL(this)'])}} <font color="red">PDF File Only Upload Here</font>
                </div>
            </div>


              <div class="form-actions">
              {{Form::submit('upload syllabus',['class'=>'btn btn-success'])}}
              </div>

            {{Form::close()}}
      </div>

      </div>
    </div>
      </div>
  </div>
</div>




<script src="{{URL::asset('js/jquery-3.2.1.min.js')}}"></script>
      <script type="text/javascript">

      $(document).ready(function(){
        


         $('.file_jquery').click(function(){
          
            var file_name = $(this).attr('file_title');
            var file_extesnsion = 'pdf';
            $("#file_div").html("<iframe src='img/backend/academic_syllabus/"+file_name+""+"."+""+file_extesnsion+"' frameborder='0' style='width: 100%; height:450px'></iframe>");
         

         });

         $('#close').click(function(){
          $('#myFrame').hide();
          $('#close').hide();

        });
});
</script>

<script src="{{URL::asset('js/jquery-3.2.1.min.js')}}"></script>
      <script type="text/javascript">

      $(document).ready(function(){
        $('.applicant_student_delete').unbind().click(function(){
         var id = $(this).attr('value');
        if (confirm('Are you sure you want to delete this?')) { 
         $(this).closest('tr').remove();
        
         $.ajax({
            url: '/academic_syllabus/'+id+'',
            type: "DELETE",
            data: {'id':id,'_token': $('input[name=_token]').val()},
            success: function(data){
              
              
            }
          });
       }

        });
      });


      </script>

    <script type="text/javascript">
 $(document).ready(function()
 {
    $("#print").click(function()
     {
      
          var w = window.open('/academic_syllabus_pdf');

          w.onload = function()
          {
              w.print();
          };
      
    });
});

 </script>
<script type="text/javascript">

   $(document).ready(function()
   {

    $("#class_name").unbind().change(function()
     {
          
         var class_name=$("#class_name").val();
         
          $.ajax({
            url:'/class_wise_subject',
            type:"post",
            data:{'class_name':class_name,'_token': $('input[name=_token]').val()},
            success:function(data)
            {

              $("#subject").html(data);
             
            }


          });
    });
       
       
  
});
</script>



@stop


