@extends('admin.index')
@section('Title','School Management Admin')
@section('breadcrumbs','')
@section('breadcrumbs_link','')
@section('breadcrumbs_title','')

@section('content')
   <div class="quick-actions_homepage">
      <ul class="quick-actions">
        <style type="text/css">
        .wd{
          width: 237px;
        }
        </style>
        <li class="wd bg_ls"> <a href="/aplicant_student"><img src="img/student.png" style="height:66px;"/>Applicant Student</a></li>
        <li class="wd bg_lb"> <a href="/admit_student"><img src="img/admit.png" style="height:66px;"/>Admit Student</a></li>
        <li class="wd bg_ly"> <a href="/student_apprisal"><img src="img/appraisal.png" style="height:66px;"/>Appraisal</a></li>
        <li class="wd bg_lg"> <a href="/teacher_info"><img src="img/teacher.png" style="height:66px;"/>Teacher</a> </li>
        <li class="wd bg_ls"> <a href="/staff_info"><img src="img/staff.png" style="height:66px;"/> Staff</a> </li>
        <li class="wd bg_lb"> <a href="/parents_info"><img src="img/parent.png" style="height:66px;"/>Parents</a> </li>
        <li class="wd bg_ly"> <a href="/daily_attendance"><img src="img/attendance.png" style="height:66px;"/>Daily Attendance</a> </li>
        <li class="wd bg_lg"> <a href="/exam_list"><img src="img/exam.png" style="height:66px;"/> Exam</a> </li>
       <li class="wd bg_ls"> <a href="/salary_slip"><img src="img/payroll.png" style="height:66px;"/> Payroll</a> </li>
       <li class="wd bg_lb"> <a href="/invoice"><img src="img/accountant.png" style="height:66px;"/> Accountant</a> </li>
       <li class="wd bg_ly"> <a href="/article_issue"><img src="img/library.png" style="height:66px;"/> Library</a> </li>
       <li class="wd bg_lg"> <a href="/assign_transport"><img src="img/transport.png" style="height:66px;"/> Transpost</a> </li>
        <li class="wd bg_ls"> <a href="/assign_dormitory"><img src="img/dormitory.png" style="height:66px;"/> Dormitory</a> </li>
        <li class="wd bg_lb"> <a href="/notice_board"><img src="img/notice.png" style="height:66px;"/> Notice</a> </li>
       <li class="wd bg_ly"> <a href="/general_settings"><img src="img/settings.png" style="height:66px;"/> Settings</a> </li>

      </ul>
    </div>
<!--End-Action boxes-->

<!--Chart-box-->
    <div class="row-fluid">
      <div class="widget-box">
        <div class="widget-title bg_lg"><span class="icon"><i class="icon-signal"></i></span>
          <h5>Real Time Chart</h5>
        </div>
        <div class="widget-content" >
          <div class="row-fluid">
            <div class="span9">
                <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                <script type="text/javascript">
                        google.charts.load('current', {'packages':['corechart']});
                        google.charts.setOnLoadCallback(drawVisualization);

                        function drawVisualization() {
                          // Some raw data (not necessarily accurate)
                          var data = google.visualization.arrayToDataTable([
                           ['Month', 'Student', 'Teacher', 'Parents', 'Attendance', 'Invoice', 'Messages'],
                           ['2004/05',  165,      938,         522,             998,           450,      614.6],
                           ['2005/06',  135,      1120,        599,             1268,          288,      682],
                           ['2006/07',  157,      1167,        587,             807,           397,      623],
                           ['2007/08',  139,      1110,        615,             968,           215,      609.4],
                           ['2008/09',  136,      691,         629,             1026,          366,      569.6]
                        ]);

                      var options = {
                        title : 'SCHOOL MANAGEMENT SYSTEM',
                        vAxis: {title: 'Overview'},
                        hAxis: {title: 'Month'},
                        seriesType: 'bars',
                        series: {5: {type: 'line'}}
                      };

                      var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
                      chart.draw(data, options);
                    }
                </script>
                 <div id="chart_div" style="width: 800px; height: 500px;"></div>
              </div>
            <div class="span3">
              <ul class="site-stats">
                <li class="bg_lh"><i class="fa fa-users"></i> <strong>{{$Student=DB::table('students')->get()->count()}}</strong> <small>Total Student's</small></li>
                <li class="bg_lh"><i class="fa fa-male"></i> <strong>{{$Teacher=DB::table('teacher')->where('status','Teacher')->get()->count()}}</strong> <small>Total Teacher's</small></li>
                <li class="bg_lh"><i class="fa fa-address-book-o"></i> <strong>{{$Class=DB::table('manage_class')->get()->count()}}</strong> <small>Total Class</small></li>
                <li class="bg_lh"><i class="fa fa-book"></i> <strong>{{$Article=DB::table('article_info')->get()->count()}}</strong> <small>Total Library Books</small></li>
                <li class="bg_lh"><i class="fa fa-male"></i> <strong>{{$Staff=DB::table('teacher')->where('status','Staff')->get()->count()}}</strong> <small>Total Staff</small></li>
                <li class="bg_lh"><i class="fa fa-bed"></i> <strong>{{$Hostel=DB::table('manage_dormitory')->get()->count()}}</strong> <small>Hostel</small></li>
                <li class="bg_lh"><i class="fa fa-bus" aria-hidden="true"></i> <strong>{{$Transport=DB::table('manage_transport')->get()->count()}}</strong> <small>Transport</small></li>
                <li class="bg_lh"><i class="fa fa-user" aria-hidden="true"></i><strong>{{$Attendance=DB::table('daily_attendance')->get()->count()}}</strong> <small>Attendance</small></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
<!--End-Chart-box-->

    <hr/>

    <div class="row-fluid">
      <div class="span6">
        <div class="widget-box">

          <div class="widget-title bg_ly" data-toggle="collapse" href="#collapseG2"><span class="icon"><i class="icon-chevron-down"></i></span>
            <h5>Latest Notice</h5>
          </div>

          <div class="widget-content nopadding collapse in" id="collapseG2">
            <ul class="recent-posts">
              @php
            $notice=DB::table('notice_board')->orderBy('notice_id','desc')->limit(4)->get();
              @endphp
            @foreach($notice as $notice_list)
            <li>
              <div class="user-thumb"> <img width="40" height="40" alt="User" src="img/notice.png"> </div>
              <div class="article-post"> <span class="user-info"> By: {{$notice_list->author}} / Date:{{$notice_list->created_at}}</span>
                <p><a href="#"> {{$notice_list->Notice}}</a> </p>
              </div>
            </li>
            @endforeach

              <li>
                <a href="/notice_board">
                <button class="btn btn-warning btn-mini">View All</button>
              </a>
              </li>
            </ul>
          </div>
        </div>


        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-ok"></i></span>
            <h5>Class list</h5>
          </div>
          <div class="widget-content">
            <div class="todo">
              <ul>
                @php
                $time=time();
                $class = DB::table('class_routine')
                        ->join('class_routine_start_child', 'class_routine.class_routine_id', '=', 'class_routine_start_child.parent')
                        ->join('class_routine_end_child', 'class_routine.class_routine_id', '=', 'class_routine_end_child.parent')
                       ->get();
                @endphp
              @foreach($class as $class_list)
                <li>
                  <div class="user-thumb"> <img width="40" height="40" alt="User" src="img/class.png"> </div>
                  <div class="article-post"> <span class="user-info">Class:{{$class_list->class_name}},Section:{{$class_list->section}},Subject:{{$class_list->	subject}} </span>
                    <p><a href="#"></a>Started Time :{{date("h:i:sa",$class_list->class_starting_time)}} End Time : {{date("h:i:sa",$class_list->class_ending_time)}}</p>
                  </div>
                </li>
            @endforeach
              </ul>
            </div>
          </div>
        </div>



		<!--end calender-->
        <div class="widget-box">
          <div class="widget-title bg_lo"  data-toggle="collapse" href="#collapseG3" > <span class="icon"> <i class="icon-chevron-down"></i> </span>
            <h5>News updates</h5>
          </div>
          <div class="widget-content nopadding updates collapse in" id="collapseG3">
            <div class="new-update clearfix"><i class="icon-ok-sign"></i>
              <div class="update-done"><a title="" href="#"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</strong></a> <span>dolor sit amet, consectetur adipiscing eli</span> </div>
              <div class="update-date"><span class="update-day">20</span>jan</div>
            </div>
            <div class="new-update clearfix"> <i class="icon-gift"></i> <span class="update-notice"> <a title="" href="#"><strong>Congratulation Maruti, Happy Birthday </strong></a> <span>many many happy returns of the day</span> </span> <span class="update-date"><span class="update-day">11</span>jan</span> </div>
            <div class="new-update clearfix"> <i class="icon-move"></i> <span class="update-alert"> <a title="" href="#"><strong>Maruti is a Responsive Admin theme</strong></a> <span>But already everything was solved. It will ...</span> </span> <span class="update-date"><span class="update-day">07</span>Jan</span> </div>
            <div class="new-update clearfix"> <i class="icon-leaf"></i> <span class="update-done"> <a title="" href="#"><strong>Envato approved Maruti Admin template</strong></a> <span>i am very happy to approved by TF</span> </span> <span class="update-date"><span class="update-day">05</span>jan</span> </div>
            <div class="new-update clearfix"> <i class="icon-question-sign"></i> <span class="update-notice"> <a title="" href="#"><strong>I am alwayse here if you have any question</strong></a> <span>we glad that you choose our template</span> </span> <span class="update-date"><span class="update-day">01</span>jan</span> </div>
          </div>
        </div>

    </div>


      <div class="span6">
        <div class="widget-box widget-chat">
          <div class="widget-title bg_lb"> <span class="icon"> <i class="icon-comment"></i> </span>
            <h5>Chat Option</h5>
          </div>
          <div class="widget-content nopadding collapse in" id="collapseG4">
            <div class="chat-users panel-right2">
              <div class="panel-title">
                <h5>Online Users</h5>
              </div>
              <div class="panel-content nopadding">
                <ul class="contact-list">
                  <li id="user-Alex" class="online"><a href=""><img alt="" src="img/demo/av1.jpg" /> <span>Alex</span></a></li>
                  <li id="user-Linda"><a href=""><img alt="" src="img/demo/av2.jpg" /> <span>Linda</span></a></li>
                  <li id="user-John" class="online new"><a href=""><img alt="" src="img/demo/av3.jpg" /> <span>John</span></a><span class="msg-count badge badge-info">3</span></li>
                  <li id="user-Mark" class="online"><a href=""><img alt="" src="img/demo/av4.jpg" /> <span>Mark</span></a></li>
                  <li id="user-Maxi" class="online"><a href=""><img alt="" src="img/demo/av5.jpg" /> <span>Maxi</span></a></li>
                </ul>
              </div>
            </div>
            <div class="chat-content panel-left2">
              <div class="chat-messages" id="chat-messages">
                <div id="chat-messages-inner"></div>
              </div>
              <div class="chat-message well">
                <button class="btn btn-success">Send</button>
                <span class="input-box">
                <input type="text" name="msg-box" id="msg-box" />
                </span> </div>
            </div>
          </div>
        </div>
        <div class="widget-box">
          <div class="widget-title"><span class="icon"><i class="icon-user"></i></span>
            <h5>Our Partner (Box with Fix height)</h5>
          </div>
          <div class="widget-content nopadding fix_hgt">
            <ul class="recent-posts">
              <li>
                <div class="user-thumb"> <img width="40" height="40" alt="User" src="img/demo/av1.jpg"> </div>
                <div class="article-post"> <span class="user-info">John Deo</span>
                  <p>Web Desginer &amp; creative Front end developer</p>
                </div>
              </li>
              <li>
                <div class="user-thumb"> <img width="40" height="40" alt="User" src="img/demo/av2.jpg"> </div>
                <div class="article-post"> <span class="user-info">John Deo</span>
                  <p>Web Desginer &amp; creative Front end developer</p>
                </div>
              </li>
              <li>
                <div class="user-thumb"> <img width="40" height="40" alt="User" src="img/demo/av4.jpg"> </div>
                <div class="article-post"> <span class="user-info">John Deo</span>
                  <p>Web Desginer &amp; creative Front end developer</p>
                </div>
            </ul>
          </div>
        </div>
        <div class="accordion" id="collapse-group">
          <div class="accordion-group widget-box">
            <div class="accordion-heading">
              <div class="widget-title"> <a data-parent="#collapse-group" href="#collapseGOne" data-toggle="collapse"> <span class="icon"><i class="icon-magnet"></i></span>
                <h5>Accordion Example 1</h5>
                </a> </div>
            </div>
            <div class="collapse in accordion-body" id="collapseGOne">
              <div class="widget-content"> It has multiple paragraphs and is full of waffle to pad out the comment. Usually, you just wish these sorts of comments would come to an end. </div>
            </div>
          </div>
          <div class="accordion-group widget-box">
            <div class="accordion-heading">
              <div class="widget-title"> <a data-parent="#collapse-group" href="#collapseGTwo" data-toggle="collapse"> <span class="icon"><i class="icon-magnet"></i></span>
                <h5>Accordion Example 2</h5>
                </a> </div>
            </div>
            <div class="collapse accordion-body" id="collapseGTwo">
              <div class="widget-content">And is full of waffle to It has multiple paragraphs and is full of waffle to pad out the comment. Usually, you just wish these sorts of comments would come to an end.</div>
            </div>
          </div>
          <div class="accordion-group widget-box">
            <div class="accordion-heading">
              <div class="widget-title"> <a data-parent="#collapse-group" href="#collapseGThree" data-toggle="collapse"> <span class="icon"><i class="icon-magnet"></i></span>
                <h5>Accordion Example 3</h5>
                </a> </div>
            </div>
            <div class="collapse accordion-body" id="collapseGThree">
              <div class="widget-content"> Waffle to It has multiple paragraphs and is full of waffle to pad out the comment. Usually, you just </div>
            </div>
          </div>
        </div>
        <div class="widget-box collapsible">
          <div class="widget-title"> <a data-toggle="collapse" href="#collapseOne"> <span class="icon"><i class="icon-arrow-right"></i></span>
            <h5>Toggle, Open by default, </h5>
            </a> </div>
          <div id="collapseOne" class="collapse in">
            <div class="widget-content"> This box is opened by default, paragraphs and is full of waffle to pad out the comment. Usually, you just wish these sorts of comments would come to an end. </div>
          </div>
          <div class="widget-title"> <a data-toggle="collapse" href="#collapseTwo"> <span class="icon"><i class="icon-remove"></i></span>
            <h5>Toggle, closed by default</h5>
            </a> </div>
          <div id="collapseTwo" class="collapse">
            <div class="widget-content"> This box is now open </div>
          </div>
          <div class="widget-title"> <a data-toggle="collapse" href="#collapseThree"> <span class="icon"><i class="icon-remove"></i></span>
            <h5>Toggle, closed by default</h5>
            </a> </div>
          <div id="collapseThree" class="collapse">
            <div class="widget-content"> This box is now open </div>
          </div>
        </div>
        <div class="widget-box">
          <div class="widget-title">
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#tab1">Tab1</a></li>
              <li><a data-toggle="tab" href="#tab2">Tab2</a></li>
              <li><a data-toggle="tab" href="#tab3">Tab3</a></li>
            </ul>
          </div>
          <div class="widget-content tab-content">
            <div id="tab1" class="tab-pane active">
              <p>And is full of waffle to It has multiple paragraphs and is full of waffle to pad out the comment. Usually, you just wish these sorts of comments would come to an end.multiple paragraphs and is full of waffle to pad out the comment.</p>
              <img src="img/2.jpg" alt="demo-image"/></div>
            <div id="tab2" class="tab-pane"> <img src="img/tab.jpg" alt="demo-image"/>
              <p>And is full of waffle to It has multiple paragraphs and is full of waffle to pad out the comment. Usually, you just wish these sorts of comments would come to an end.multiple paragraphs and is full of waffle to pad out the comment.</p>
            </div>
            <div id="tab3" class="tab-pane">
              <p>And is full of waffle to It has multiple paragraphs and is full of waffle to pad out the comment. Usually, you just wish these sorts of comments would come to an end.multiple paragraphs and is full of waffle to pad out the comment. </p>
              <img src="img/4.png" alt="demo-image"/></div>
          </div>
        </div>
      </div>
    </div>
@stop
