@extends('admin.index')
@section('Title','User Profile')
@section('breadcrumbs','User Profile')
@section('breadcrumbs_link','/user_profile')
@section('breadcrumbs_title','User Profile')
@section('content')


<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="dropjone/dropzone.min.js"></script>
<link rel="stylesheet" type="text/css" href="dropjone/dropzone.min.css">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<div class="container">

     <div>
        <h2>User Profile</h2><hr>
     </div>


<div class="col-sm-12">

    <div class="col-sm-6">
      <div class="image_part">
        <img style="width: 20%" src="img/blankavatar.png">
     </div>
    </div>

    <div style="float: right;margin-top: 20%;" class="col-sm-6">
        <table style=" border: 1px black solid;" class="table table-responsive">
                <thead>
                    <tr>
                        <td>Last Login</td>
                        <td>12 August 2017</td>

                    </tr>

                    <tr>
                      <td>Permission</td>
                      <td>Create , Edit , Delete . View</td>
                    </tr>


                    <tr>
                      <td>Role</td>
                      <td>Super Admin</td>
                    </tr>



                    <tr style="background: #37414B;color:#fff">
                      <td class="text-center" colspan="2">I School Managment System</td>
                    </tr>
                </thead>
            </table>
    </div>
</div>



<div class="col-sm-12">
    <div class="col-sm-6">
        <div>
            <table class="table-responsive">
                <thead>
                    <tr>
                        <td>Md Hasan Ali Haolader</td>

                    </tr>
                    <tr><td>Senior Developer,</td></tr>
                    <tr><td>{{ config('app.name') }}</td></tr>
                    <tr><td style="color:green"><i class="fa fa-circle-thin" aria-hidden="true"></i> &nbsp Active</td></tr>
                </thead>
            </table>
        </div>
    </div>



</div>


<div class="col-sm-12">
  <div class="col-sm-6">

    <table  style="width: 50%;margin-top: 12%;" class="table table-responsive">
                <thead>
                    <tr style="background: #37414B;color:#fff">
                      <td class="text-center" colspan="2">Which Module Are You Access In</td>
                    </tr>
                    <tr>
                        <td>RBAC</td>


                    </tr>

                    <tr>
                      <td>Students</td>

                    </tr>


                    <tr>
                      <td>Student Information</td>

                    </tr>

                    <tr>
                      <td>Apprisal</td>

                    </tr>




                </thead>
            </table>
  </div>
    <p style="color:red">Have More Module But We dont disclose this Have Security Purpose</p>
  </div>

</div>



<div class="container">
  <div class="row">
      <div class="col-sm-12">


              <div class="container">
          <h1 class="heading-primary"></h1>
          <div class="accordion">
            <dl>
              <dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger">Basic Information</a>
              </dt>
              <dd class="accordion-content accordionItem is-collapsed" id="accordion1" aria-hidden="true">
                  <table class="table table-responsive">
                    <thead>
                        <tr>
                          <td>Name</td>
                          <td><input type="text" class="form-control" name=""></td>
                        </tr>

                        <tr>
                          <td>Birth Date</td>
                          <td><input type="Date" class="form-control" name=""></td>
                        </tr>

                        <tr>
                          <td>Gender</td>
                          <td><input type="checkbox" name="">Male <input type="checkbox" name="">Female</td>
                        </tr>

                        <tr>
                            <td>Phone</td>
                            <td><input type="text" name=""></td>
                        </tr>




                    </thead>
                  </table>
              </dd>
              <dt>
                <a href="#accordion2" aria-expanded="false" aria-controls="accordion2" class="accordion-title accordionTitle js-accordionTrigger">
                  Secondray Accordion File</a>
              </dt>
              <dd class="accordion-content accordionItem is-collapsed" id="accordion2" aria-hidden="true">




<form action="/file-upload" class="dropzone">
  <div class="fallback">
    <input name="file" type="file" multiple />
  </div>
</form>





              </dd>
              <dt>
                <a href="#accordion3" aria-expanded="false" aria-controls="accordion3" class="accordion-title accordionTitle js-accordionTrigger">
                  Third Accordion Information
                </a>
              </dt>
              <dd class="accordion-content accordionItem is-collapsed" id="accordion3" aria-hidden="true">


                  <table class="table table-responsive">
                    <thead>
                        <tr>
                          <td>Email</td>
                          <td><input type="text" class="form-control" name=""></td>
                        </tr>

                        <tr>
                          <td>Password</td>
                          <td><input type="text" class="form-control" name=""></td>
                        </tr>

                        <tr>
                          <td>confiram Password</td>
                          <td><input type="text" name="">
                        </tr>






                    </thead>
                  </table>


              </dd>
            </dl>
          </div>
        </div>


      </div>
  </div>
</div>










</div>

<style type="text/css">

//updated ver
* {
  box-sizing:border-box;
}
@import url(https://fonts.googleapis.com/css?family=Lato:400,700);
body {

  font-family:'Lato';
}
.heading-primary {
  font-size:2em;
  padding:2em;
  text-align:center;
}
.accordion dl,
.accordion-list {
   border:1px solid #ddd;
   &:after {
       content: "";
       display:block;
       height:1em;
       width:100%;
       background-color:darken(#38cc70, 10%);
     }
}
.accordion dd,
.accordion__panel {
   background-color:#eee;
   font-size:1em;
   line-height: 5.5em;
}
.accordion p {
  padding:1em 2em 1em 2em;
}

.accordion {
    position:relative;
    background-color:#eee;
}
.container {
  max-width:960px;
  margin:0 auto;
  padding:2em 0 2em 0;
}
.accordionTitle,
.accordion__Heading {
 background-color:#37414B;
   text-align:center;
     font-weight:700;
          padding:2em;
          display:block;
          text-decoration:none;
          color:#fff;
          transition:background-color 0.5s ease-in-out;
  border-bottom:1px solid darken(#38cc70, 5%);
  &:before {
   content: "+";
   font-size:1.5em;
   line-height:0.5em;
   float:left;
   transition: transform 0.3s ease-in-out;
  }
  &:hover {
    background-color:darken(#38cc70, 10%);
  }
}
.accordionTitleActive,
.accordionTitle.is-expanded {
   background-color:darken(#38cc70, 10%);
    &:before {

      transform:rotate(-225deg);
    }
}
.accordionItem {
    height:auto;
    overflow:hidden;
    //SHAME: magic number to allow the accordion to animate

     max-height:50em;
    transition:max-height 1s;


    @media screen and (min-width:48em) {
         max-height:15em;
        transition:max-height 0.5s

    }


}

.accordionItem.is-collapsed {
    max-height:0;
}
.no-js .accordionItem.is-collapsed {
  max-height: auto;
}
.animateIn {
     animation: accordionIn 0.45s normal ease-in-out both 1;
}
.animateOut {
     animation: accordionOut 0.45s alternate ease-in-out both 1;
}
@keyframes accordionIn {
  0% {
    opacity: 0;
    transform:scale(0.9) rotateX(-60deg);
    transform-origin: 50% 0;
  }
  100% {
    opacity:1;
    transform:scale(1);
  }
}

@keyframes accordionOut {
    0% {
       opacity: 1;
       transform:scale(1);
     }
     100% {
          opacity:0;
           transform:scale(0.9) rotateX(-60deg);
       }
}
</style>


<script type="text/javascript">
  //uses classList, setAttribute, and querySelectorAll
//if you want this to work in IE8/9 youll need to polyfill these
(function(){
  var d = document,
  accordionToggles = d.querySelectorAll('.js-accordionTrigger'),
  setAria,
  setAccordionAria,
  switchAccordion,
  touchSupported = ('ontouchstart' in window),
  pointerSupported = ('pointerdown' in window);

  skipClickDelay = function(e){
    e.preventDefault();
    e.target.click();
  }

    setAriaAttr = function(el, ariaType, newProperty){
    el.setAttribute(ariaType, newProperty);
  };
  setAccordionAria = function(el1, el2, expanded){
    switch(expanded) {
      case "true":
        setAriaAttr(el1, 'aria-expanded', 'true');
        setAriaAttr(el2, 'aria-hidden', 'false');
        break;
      case "false":
        setAriaAttr(el1, 'aria-expanded', 'false');
        setAriaAttr(el2, 'aria-hidden', 'true');
        break;
      default:
        break;
    }
  };
//function
switchAccordion = function(e) {
  console.log("triggered");
  e.preventDefault();
  var thisAnswer = e.target.parentNode.nextElementSibling;
  var thisQuestion = e.target;
  if(thisAnswer.classList.contains('is-collapsed')) {
    setAccordionAria(thisQuestion, thisAnswer, 'true');
  } else {
    setAccordionAria(thisQuestion, thisAnswer, 'false');
  }
    thisQuestion.classList.toggle('is-collapsed');
    thisQuestion.classList.toggle('is-expanded');
    thisAnswer.classList.toggle('is-collapsed');
    thisAnswer.classList.toggle('is-expanded');

    thisAnswer.classList.toggle('animateIn');
  };
  for (var i=0,len=accordionToggles.length; i<len; i++) {
    if(touchSupported) {
      accordionToggles[i].addEventListener('touchstart', skipClickDelay, false);
    }
    if(pointerSupported){
      accordionToggles[i].addEventListener('pointerdown', skipClickDelay, false);
    }
    accordionToggles[i].addEventListener('click', switchAccordion, false);
  }
})();
</script>
<script src="https://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script>







</script>
@stop
