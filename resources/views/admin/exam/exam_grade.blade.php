@extends('admin.index')
@section('Title','Exam Grade')
@section('breadcrumbs','Exam Grade')
@section('breadcrumbs_link','/exam_grade')
@section('breadcrumbs_title','Exam grade')

@section('content')

@if (Session::has('success'))
    <div class="alert alert-success alert-dismissible fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong> {{ Session::get('success') }}
    </div>
   
@endif

@if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissible fade in">
        <ul  style='list-style:none'>
            @foreach ($errors->all() as $error)
                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> &nbsp;{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="container">
    <h2><i class="fa fa-university" aria-hidden="true"></i> Exam Grade</h2> <!-- Tab Heading  -->
  <p title="Transport Details">I School Exam Grade Details</p> <!-- Transport Details -->

      <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home"><i class="fa fa-bars" aria-hidden="true"></i> Exam Grade</a></li>
        <li><a data-toggle="tab" href="#menu1"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Grade </a></li>
      </ul>



  <div class="tab-content" style="width:1090px;"> <!-- Transport List Report  -->

        <div id="home" class="tab-pane fade in active">
            <div class="widget-box">
                <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                  <h5>Data table</h5>
                </div>

            <div class="widget-content nopadding">
              <table class="table table-bordered data-table">

                  <thead>
                    <tr>
                      <th>Grade Name </th>
                      <th>Grade Point</th>
                      <th>Mark From</th>
                      <th>Mark Upto</th>
                      <th>Actions</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                     @foreach($exam_grade as $exam_grade_data)
                      <tr class="gradeX">
                        <td>{{$exam_grade_data->grade_name}}</td>
                        <td>{{$exam_grade_data->grade_point}}</td>
                        <td>{{$exam_grade_data->mark_from}}</td>
                        <td>{{$exam_grade_data->mark_upto}}</td>
                        
                        <td class="center">

                          <div class="display_status">
                                    {{Form::open(['url'=>"exam_grade/$exam_grade_data->id/edit" ,'method'=>'GET'])}}
                                    {{Form::submit('Edit',['class'=>'btn btn-primary'])}} 
                                    {{Form::close()}}
                                    

                                    {{Form::button('DELETE',['class'=>'btn btn-danger applicant_student_delete','value'=>$exam_grade_data->id,])}}


                           </div>
                        </td>
                      </tr>
                     @endforeach 
                    
                     
                      
                    </tbody>
                </table>
              </div>
          </div>
      </div>







      <div id="menu1" class="tab-pane fade">




        <div >
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
          <h5>Exam Grade</h5>
        </div>
        <div class="widget-content nopadding">
          {{Form::open(['url'=>'/exam_grade','files'=>true,'class'=>'form-horizontal','method'=>'post','name'=>'basic_validate','id'=>'basic_validate','navalidate'=>'navalidate'])}}

            <div class="control-group">
              {{Form::label('grade_name','Grade Name:',['class'=>'control-label','title'=>'Grade Name'])}}
              <div class="controls">
                {{Form::text('grade_name','',['id'=>'required','title'=>'grade_name','placeholder'=>'Grade Name'])}}
              </div>
            </div>
            <div class="control-group">
              {{Form::label('grade_point','Grade Point:',['class'=>'control-label','title'=>'Grade Point'])}}
              <div class="controls">
                {{Form::text('grade_point','',['id'=>'required','title'=>'grade_point','placeholder'=>'Grade Point'])}}
              </div>
            </div>
            <div class="control-group">
              {{Form::label('mark_from','Mark From:',['class'=>'control-label','title'=>'Mark From'])}}
              <div class="controls">
                {{Form::text('mark_from','',['id'=>'required','title'=>'mark_from','placeholder'=>'Mark From'])}}
              </div>
            </div>
            <div class="control-group">
              {{Form::label('mark_upto','Mark Upto:',['class'=>'control-label','title'=>'Mark Upto'])}}
              <div class="controls">
                {{Form::text('mark_upto','',['id'=>'required','title'=>'mark_upto','placeholder'=>'Mark Upto'])}}
              </div>
            </div>
            


        <div class="form-actions">
              <input type="submit" value="Add Grade" class="btn btn-primary">
            </div>
            {{ csrf_field() }}
            {{Form::close()}}
        </div>
      </div>
    </div>





      </div>

  </div>
</div>


<script src="{{URL::asset('js/jquery-3.2.1.min.js')}}"></script>
      <script type="text/javascript">

      $(document).ready(function(){
        $('.applicant_student_delete').unbind().click(function(){
         var id = $(this).attr('value');
        if (confirm('Are you sure you want to delete this?')) { 
         $(this).closest('tr').remove();
        
         $.ajax({
            url: '/exam_grade/'+id+'',
            type: "DELETE",
            data: {'id':id,'_token': $('input[name=_token]').val()},
            success: function(data){
              
              
            }
          });
       }

        });
      });


      </script>


@stop
