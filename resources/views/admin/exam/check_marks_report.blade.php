@extends('admin.index')
@section('Title','Check Exam')
@section('breadcrumbs','Check Exam')
@section('breadcrumbs_link','/check_marks')
@section('breadcrumbs_title','Check Marks')

@section('content')
	
	<div class="container">
  		<h2><i class="fa fa-list-alt" aria-hidden="true"></i> &nbsp Edit Mark's Information</h2> <!-- Tab Heading  -->
 		<p title="Transport Details">{{config('app.name')}} Report</p> <!-- Transport Details -->


 		<div class="panel panel-default text-right" >
      <div class="panel-body">
         <ul class='dropdown_test'>
            <li><a href='/applicant_student_report'><i class="fa fa-list-alt" aria-hidden="true"></i> &nbsp;Applicant Teacher's Report</a></li>
            <li><a href='#'>Add Teacher</a></li>
            <li><a href='#'>Teacher's Report </a></li>

        </ul>
      </div>
    </div>



   

    <table style="margin-top: 4%" class="">
        <thead style="background: #1F262D">
              <tr style="height: 34px;color: #fff">
                <th>Exam</th>
                <th>Class</th>
                <th>Section</th>
                <th>Department</th>
                <th>subject</th>
                
               
              </tr>
        </thead>
        

            <tbody>
              <tr>
                  {{Form::open(['url'=>'/check_marks','class'=>'form-horizontal','method'=>'post','name'=>'basic_validate','id'=>'basic_validate','novalidate'=>'novalidate'])}}
       

               @foreach($exam_data as $exam_data_list)
                 @php 
                    $all_exam_list[$exam_data_list->exam_name]=$exam_data_list->exam_name
                 @endphp
               @endforeach

               @foreach($class_data as $class_data_list)
                @php
                 $all_class_list[$class_data_list->class_name]=$class_data_list->class_name
                @endphp
              @endforeach

            <td>{{Form::select('exam_name',$all_exam_list,null,['style'=>'width: 157px','id'=>'exam_name'])}}</td>
            <td>{{Form::select('class_name',$all_class_list,null,['style'=>'width: 157px','id'=>'class_info'])}}</td>
            
            <td>{{Form::select('section',['Section Name'=>'Section Name'],null,['id'=>'section_info','style'=>'width: 157px'])}}</td>

            <td>{{Form::select('Department',['Department Name'=>'Department Name'],null,['id'=>'Department_info','style'=>'width: 157px'])}}</td>

            <td>{{Form::select('subject',['subject Name'=>'subject Name'],null,['id'=>'subject_info','style'=>'width: 157px'])}}</td>
    
          </tr>
                {{Form::close()}}
            </tbody>
        </table>



    <div id="table_show_trigger_forsubject"  hidden="hidden" class="col-xs-12">
      <table style="width: 25%;" class="table">
          <tr>
              <td><b>Exam Name</b></td>
              <td class="exam_name_in_view"></td>
              
          </tr>

          <tr>
              <td><b>Class Name</b></td>
              <td class="class_name_in_view"></td>
              
          </tr>

          <tr>
              <td><b>Section Name</b></td>
              <td class="section_name_in_view"></td>
              
          </tr>

          <tr>
              <td><b>Department Name</b></td>
              <td class="department_name_in_view"></td>
              
          </tr>

          <tr>
              <td><b>Subject Name</b></td>
              <td class="subject_name_in_view"></td>
              
          </tr>
      </table>
    </div>


		<div class="tab-content"> <!-- Transport List Report  -->

		    	<div id="home" class="tab-pane fade in active">
		      		<div class="widget-box">
          				<div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            				<h5>Edit Mark's Information</h5>
          				</div>

          		<div class="widget-content nopadding">
		            <table class="table table-bordered data-table">

			              <thead>
			                <tr>
			                  <th>Subject</th>
			                  <th>Roll</th>
			                  <th>CGPA</th>
			                  <th>Grade Name</th>
			                  <th>Mark</th>
			                  <th>Actions</th>
			                </tr>
			              </thead>


			              <tbody id="edit_mark_data">
                      
                   

                          
			              </tbody>
            			</table>
          			</div>
        		</div>
		    </div>






		</div>
	</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script type="text/javascript">

      $(document).ready(function(){
     
         $('#class_info').unbind().change(function(){
            var class_name=$('#class_info').val();
                $.ajax({
                url: '/class_w_department_filter',
                type: "post",
                data: {'class_name':class_name,'_token': $('input[name=_token]').val()},
                success: function(data){
                  $('#Department_info').html(data);
                }
              });


                $.ajax({
                  url: '/class_w_section_filter',
                  type: "post",
                  data: {'class_name':class_name,'_token': $('input[name=_token]').val()},
                  success: function(data){
                    $('#section_info').html(data);
                  }
                });

                 $.ajax({
                  url: '/class_w_subject_filter',
                  type: "post",
                  data: {'class_name':class_name,'_token': $('input[name=_token]').val()},
                  success: function(data){
                    $('#subject_info').html(data);
                  }
                });
            
         });
         $("#section_info").unbind().change(function(){
           var section=$("#section_info").val();
           
         });
          $("#Department_info").unbind().change(function(){
           var department=$("#Department_info").val();
          
         });
           



           $("#subject_info").unbind().change(function(){
                var subject=$("#subject_info").val();
                var exam_name=$("#exam_name").val();
                var section=$("#section_info").val();
                var Department_info=$("#Department_info").val();
                var class_name=$("#class_info").val();

                $("#table_show_trigger_forsubject").show(500);

                $('.exam_name_in_view').text(exam_name);
                $('.class_name_in_view').text(class_name);
                $('.section_name_in_view').text(section);
                $('.department_name_in_view').text(Department_info);
                $('.subject_name_in_view').text(subject);


               $.ajax({
                url:'/check_marks_edit',
                type:"post",
                data:{'class_name':class_name,'subject':subject,'exam_name':exam_name,'section':section,'Department_info':Department_info,'_token': $('input[name=_token]').val()},
                success:function(r)
                {
                    $("#edit_mark_data").html("");
                    for(i=0;i<r.length;i++)
                    {
                      $("#edit_mark_data").append("<tr>\
                        <td>"+r[i].subject+"</td>\
                        <td>"+r[i].roll+"</td>\
                        <td>"+r[i].cgpa+"</td>\
                        <td>"+r[i].grade_name+"</td>\
                        <td>"+r[i].mark+"</td>\
                        <td><input type=\"button\" class=\"btn btn-primary\" class=\"editdata\"  value="+r[i].roll+" /><input type=\"button\" class=\"btn btn-danger\" id=\"delete\" value=\"Delete\"></td>\
                      </tr>");
                    }   
                }
               });
          
            });
          
       

        $('.editdata').unbind().click(function(){
          alert("ok"); 
        });
         
         //var id = $(this).attr('value');
         //alert(id);
         // $.ajax({
         //    url: '/applicant_student_admit_card',
         //    type: "post",
         //    data: {'id':id,'_token': $('input[name=_token]').val()},
         //    success: function(data){
         //      $('#markshet').html(data);
         //    }
         //  });

       

          
       // $('.delete').unbind().click(function(){
       //       if( !confirm('Are you sure you want to continue?')) {
       //              return false;
       //          }else
       //              {
       //               var id = $(this).attr('value');
       //               $(this).closest('tr').remove();
       //               $.ajax({
       //                  url: '/aplicant_student/'+id+'',
       //                  type: "DELETE",
       //                  data: {'id':id,'_token': $('input[name=_token]').val()},
       //                  success: function(data){


       //                  }
       //                });
       //           }
         

       //  });

       });

      </script>

@stop