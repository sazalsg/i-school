@extends('admin.index')
@section('Title','Tabulation')
@section('breadcrumbs','Tabulation')
@section('breadcrumbs_link','/tabulation')
@section('breadcrumbs_title','Tabulation')

@section('content')
<div class="container">
  <h2>
    <i class="fa fa-file-o" aria-hidden="true"></i>
    </i> Tabulation Sheet
  </h2>
  <!-- Tab Heading  -->
  <p title="Transport Details">I School Managment  Tabulation
  </p>
  <!-- Transport Details -->
  <div class="widget-box">
    <div class="widget-title">
      <span class="icon">
        <i class="icon-info-sign">
        </i>
      </span>
      <h5>Tabulation Sheet
      </h5>
    </div>
    <div class="widget-content padding">
      <form class="form-horizontal" method="post" action="#" name="basic_validate" id="basic_validate" novalidate="novalidate">
        <div class="control-group">
          <table class="table address">
            <thead>
              <tr>
                <th>Class
                </th>
                <th>Exam
                </th>
                <th>Date
                </th>
              </th>
            </tr>
          </thead>
        <tbody>
          <tr>
            <td>
              <select>
                <option>Select class
                </option>
                <option>Class One
                </option>
              </select>
            </td>
            <td>
              <select>
                <option>Select Exam
                </option>
                <option>First Term Exam
                </option>
              </select>
            </td>
            <td>
              <select>
                <option>Date
                </option>
                <option>18-07-2017
                </option>
                <option>19-07-2017
                </option>
              </select>
            </td>
            <td>
              <input type="submit" value="View Tabulation Sheet" class="btn btn-success">
            </td>
          </tr>
        </tbody>
        </table>
    </div>
    </form>
</div>
</div>
</div>
</div>
<div class="container">
  <div class="row" style="text-align: center;">
    <div class="col-sm-4">
    </div>
    <div class="col-sm-4" >
      <div class="tile-stats tile-gray">
        <div class="icon">
          <i class="entypo-chart-area">
          </i>
        </div>
        <h3 style="color: #34495e;">Tabulation Sheet
        </h3>
        <h4 style="color: #34495e;">
          Class One First Term Exam
        </h4>
        <h4 style="color:#34495e;">
        </h4>
      </div>
    </div>
    <div class="col-sm-4">
    </div>
  </div>
  <br>
  </br>
<br>
</br>
<form action="" method="post" accept-charset="utf-8">
  <div id="attendance_update">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>Student
          </th>
          <th>Subject
          </th>
          <th>Total
          </th>
          <th>Average Grade Point
          </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>4th semester
          </td>
          <td>285185
          </td>
          <td>Shakil Ahmmed
          </td>
          <td>A
          </td>
        </tr>
      </tbody>
    </table>
  </div>
  <center>
    <button type="submit" class="btn btn-success" id="submit_button">
    <i class="fa fa-print" aria-hidden="true"></i>
     Print Tabulation Sheet
    </button>
  </center>
</form>
</div>
@stop
