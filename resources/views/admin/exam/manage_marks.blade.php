@extends('admin.index')
@section('Title','Manage Marks')
@section('breadcrumbs','Manage Marks')
@section('breadcrumbs_link','/manage_marks')
@section('breadcrumbs_title','Manage Marks')
@section('content')


  @if (Session::has('success'))
    <div class="alert alert-success alert-dismissible fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong> {{ Session::get('success') }}
    </div>
   
@endif

 @if (Session::has('wrong'))
    <div class="alert alert-danger alert-dismissible fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>wrong!</strong> {{ Session::get('wrong') }}
    </div>
   
@endif


@if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissible fade in">
        <ul  style='list-style:none'>
            @foreach ($errors->all() as $error)
                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> &nbsp;{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   



<div class="container">
  <h2><i class="fa fa-user-plus" aria-hidden="true"></i> Student Exam Manage Marks</h2>
  <!-- Tab Heading  -->
  <p title="Transport Details">I School Managment  Student Exam Manage Marks Details</p>
  <!-- Transport Details -->
    



{{Form::open(['url'=>"/manage_marks",'class'=>'form-horizontal','method'=>'post','files'=>true,'name'=>'basic_validate','id'=>'basic_validate','novalidate'=>'novalidate'])}}

  <div class="widget-box">
      <div class="widget-title">
        <span class="icon"><i class="icon-info-sign"></i></span><h5>Add Exam Manage Marks</h5>
      </div>
  <input type="hidden" value="<?=time()?>" name="mark_evulation">

    <div class="widget-content padding">
     
        <div class="control-group">
          <table class="table address">
            <thead>
              <tr>
                <th>Exam</th>
                <th>Class</th>
                <th>Section</th>
                <th>Department</th>
                <th>subject</th>
                <th>Action</th>
              </tr>
            </thead>
        

            <tbody>
                <tr>
    @foreach($exam_list as $exam_list_data)
      @php $exam_list_array[$exam_list_data->exam_name]=$exam_list_data->exam_name @endphp
    @endforeach

    @foreach($class_list as $class_list_data)
      @php $class_list_data_array[$class_list_data->class_name]=$class_list_data->class_name @endphp
    @endforeach

    <td>{{Form::select('exam_name',$exam_list_array,null,['style'=>'width: 157px'])}}</td>
    <td>{{Form::select('class_name',$class_list_data_array,null,['style'=>'width: 157px','id'=>'class_info'])}}</td>
    <td>{{Form::select('section',['Section Name'=>'Section Name'],null,['id'=>'section_info','style'=>'width: 157px'])}}</td>
    <td>{{Form::select('Department',['Department Name'=>'Department Name'],null,['id'=>'Department_info','style'=>'width: 157px'])}}</td>
    <td>{{Form::select('subject',['subject Name'=>'subject Name'],null,['id'=>'subject_info','style'=>'width: 157px'])}}</td>
    <td>{{Form::button('Manage Mark',['class'=>'btn btn-success','id'=>'manage_mark_button','style'=>'width: 157px'])}}</td>
                </tr>
            </tbody>
        </table>
      </div>
      
    </div>
  </div>



<div class="container">
    <div class="row text-center">
        <div class="col-sm-4"></div>
      
          <div class="col-sm-4" >
            <div class="tile-stats tile-gray">
              <div class="icon"><i class="entypo-chart-area"></i></div>
              <h3 style="color: #34495e;">Exam Manage Marks</h3>
              <h4 style="color: #34495e;">Section</h4>
              <h4 style="color:#34495e;"></h4>
            </div>
          </div>
          <div class="col-sm-4"></div>
    </div>
    <br></br>
    <br></br>


      
     <div id="manage_entry_section" hidden="hidden">
       
        <table>
          <thead>
            <tr>
              <th  class="tag manage_mark_table_desgin"> Entry Marks For</th>
              <th id="subject_name"></th>
            </tr>
          </thead>

          <thead>
            <tr>
              <th class="tag manage_mark_table_desgin">Minmum - Maximum Mark</th>
              <th id="max_min_mark"></th>
            </tr>
          </thead> 

          <thead>
            <tr>
              <th class="tag manage_mark_table_desgin">Total Student</th>
              <th id="total_student"></th>
            </tr>
          </thead> 

          <thead>
            <tr>
              <th></th>
              <th><span  class="tag manage_mark_table_desgin">Class</span> <span  class="tag manage_mark_table_desgin">Section</span> <span  class="tag manage_mark_table_desgin">Department</span> <span  class="tag manage_mark_table_desgin">Exam</span></th>
            </tr>
          </thead>   
      </table>


       
       <table class="table" id="registered_participants">
         <thead style="background: #F6F6F6;">
           <td>SL NO</td>
           <td>Student Name</td>
           <td>Roll</td>
           <td>Mark</td>
           <td>CGPA</td>
           <td>GRADE</td>
           <td>Comment</td>
         </thead>
         <tbody class="student_data_show">
            
         </tbody >
       </table>
      
    

    <div style="text-align: center;">
      <button type="submit" class="btn btn-success" id="submit_button"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Save Changes</button>
    </div>
    </div>
  </div>
</div>
  
{{Form::close()}}

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script type="text/javascript">

      $(document).ready(function(){
        
        $('.marks').unbind().click(function(){
              alert("ok");
         });


         $('.Admin_card').unbind().click(function(){
         var id = $(this).attr('aplicant_id');
         
         $.ajax({
            url: '/applicant_student_admit_card',
            type: "post",
            data: {'id':id,'_token': $('input[name=_token]').val()},
            success: function(data){
              $('#markshet').html(data);
            }
          });

        });

         $('#class_info').unbind().change(function(){
            var class_name=$('#class_info').val();
                $.ajax({
                url: '/class_w_department_filter',
                type: "post",
                data: {'class_name':class_name,'_token': $('input[name=_token]').val()},
                success: function(data){
                  $('#Department_info').html(data);
                }
              });


                $.ajax({
                  url: '/class_w_section_filter',
                  type: "post",
                  data: {'class_name':class_name,'_token': $('input[name=_token]').val()},
                  success: function(data){
                    $('#section_info').html(data);
                  }
                });

                 $.ajax({
                  url: '/class_w_subject_filter',
                  type: "post",
                  data: {'class_name':class_name,'_token': $('input[name=_token]').val()},
                  success: function(data){
                    $('#subject_info').html(data);
                  }
                });
            
         });

         $("#manage_mark_button").unbind().click(function(){
              var class_name=$("#class_info").val();
              var section_name=$("#section_info").val();
              var Department=$("#Department_info").val();
              var subject_name=$("#subject_info").val();
              if(subject_name=="")
              {
                alert("Please Enter Subject Name");
              }
              else
              {
                $("#subject_name").text(subject_name);
                $("#manage_entry_section").show(1000);
                $.ajax({
                  url: '/manage_marks_for_student',
                  type: "post",
                  data: {'class_name':class_name,
                        'section_name':section_name,
                        'Department':Department,
                        '_token': $('input[name=_token]').val()},
                  success: function(data){
                    console.log(data[2]);

                    $('#max_min_mark').html(data[0]+"-"+data[1]);
                    $('#total_student').html(data[3]);

                    for(i in data[2])
                    {
                          var sl_no=parseInt(i)+1;
                         $('.student_data_show').append("<tr>" +
                         "<td>"+sl_no+"</td>" +
                         "<td>"+data[2][i].student_name+"</td>" +
                         "<td><input style='width:100px' class='roll' value="+data[2][i].roll+" name='roll[]' type='text'></td>" +
                         "<td><input style='width:100px' class='mark_j' name='mark[]' type='text'> </td>" +
                         "<td><input class='cgpa' name='cgpa[]' type='text'></td>" +
                         "<td><input class='grade_name' name='grade_name[]' type='text'></td>" +
                         "<td><input type='text' name='comment[]'></td></tr>");
                    }

                  
                  }
                });


              }
             
            });

         $('#registered_participants').on('keyup', '.mark_j', function(eq) {
            var row = $(this).closest("tr");
            var pass_mark=$(this).val();
            $.ajax({
              url:"/grade_and_cgp_find",
              type:"post",
              data:{
                'pass_mark':pass_mark,
                '_token': $('input[name=_token]').val()
              },
              success:function(data)
              {
                
                row.find(".cgpa").val(data[0]);
                row.find(".grade_name").val(data[1]);

              }
            });

          });
           

         
 });



      </script>

@stop














<!-- 




                  $.ajax({
                      url: '/manage_mark_for_student_show',
                      type: "post",
                      data: {'class_name':class_name,
                          'section_name':section_name,
                          'Department':Department,
                          '_token': $('input[name=_token]').val()},
                      success: function(data){
                            console.log(data);
                            $('.student_data_show').html(data);
                      }
                  }); -->