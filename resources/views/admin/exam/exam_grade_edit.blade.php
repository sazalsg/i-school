@extends('admin.index')
@section('Title','Exam Grade Edit')
@section('breadcrumbs','Exam Grade Edit')
@section('breadcrumbs_link','/exam_grade_edit')
@section('breadcrumbs_title','Manage Exam Grade Edit')

@section('content')
@if (Session::has('success'))
    <div class="alert alert-success alert-dismissible fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong> {{ Session::get('success') }}
    </div>
   
@endif

@if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissible fade in">
        <ul  style='list-style:none'>
            @foreach ($errors->all() as $error)
                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> &nbsp;{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
	
<h2><i class="fa fa-list-alt" aria-hidden="true"></i> &nbspChart Of Account</h2> <!-- Tab Heading  -->
 		<p title="Transport Details">I School Managment  Chart Of Accounts</p> <!-- Transport Details -->

                <div class="widget-box">
                  <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                    <h5>Data table</h5>
                  </div>

          <div class="widget-content nopadding">
             {{Form::open(['url'=>"/exam_grade/$exam_grade->id",'files'=>true,'class'=>'form-horizontal','method'=>'PUT','name'=>'basic_validate','id'=>'basic_validate','navalidate'=>'navalidate'])}} 
             
              <div class="control-group">
                {{Form::label('grade_name','Grade Name:',['class'=>'control-label','title'=>'Grade Name'])}}
                <div class="controls">
                  {{Form::text('grade_name',$exam_grade->grade_name,['id'=>'required','title'=>'grade_name','placeholder'=>'Grade Name'])}}
                </div>
              </div>
              
              <div class="control-group">
                {{Form::label('grade_point','Grade Point:',['class'=>'control-label','title'=>'Grade Point'])}}
                <div class="controls">
                  {{Form::text('grade_point',$exam_grade->grade_point,['id'=>'required','title'=>'grade_point','placeholder'=>'Grade Point'])}}
                </div>
              </div>
              
              

              <div class="control-group">
                {{Form::label('mark_from','Mark From:',['class'=>'control-label','title'=>'Mark From'])}}
                <div class="controls">
                  {{Form::text('mark_from',$exam_grade->mark_from,['id'=>'required','title'=>'mark_from','placeholder'=>'Mark From'])}}
                </div>
              </div>


              <div class="control-group">
                {{Form::label('mark_upto','Mark Upto:',['class'=>'control-label','title'=>'Mark Upto'])}}
                <div class="controls">
                  {{Form::text('mark_upto',$exam_grade->mark_upto,['id'=>'required','title'=>'mark_upto','placeholder'=>'Mark Upto'])}}
                </div>
              </div>

              

          <div class="form-actions">
                <input type="submit" value="UPDATE" class="btn btn-success">
              </div>
              {{ csrf_field() }}
            {{Form::close()}}
          </div>
            </div>
@stop