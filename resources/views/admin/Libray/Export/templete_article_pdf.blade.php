<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Library Templete Article's Report</title>
</head>



<style>
	
  .print_report{margin-top: 2%;margin-left: 5%;margin-right: 5%;}
  .p_tag_report{font-size: 12px;margin-top: 30px}
</style>



<body class='body_for_report'>
      <div class='print_report'>
      		<div class="container">
	           <img src="img/logo.png" alt="Company Logo" style='background: currentColor;height: 32px;width: 152px;' height="120" width="120"/>
		  <h5>{{Session::get('school.system_name')}}( {{Session::get('school.school_eiin')}} )
      </h5>
		  <h5>{{Session::get('school.address')}}<br>
          {{Session::get('school.Phone')}}<br>
          {{Session::get('school.country')}}<br>
          {{Session::get('school.postal_code')}}</h5>

              <h5 style="text-align: center;">Templete Article Report<hr style="width: 100px; margin-left:266px; "></h5>
                     <div class="col-sm-12">
                         <div class="panel-body">
                          <table style="width: 100%;">
                                <thead>
                                   <tr style="background: aliceblue; font-size:11px;">
                                     <th>Id</th>
                                     <th>Article Name</th>
                                     <th>Author</th>
                                     <th>Publisher</th>
                                     <th>Language</th>
                                     <th>ISBN</th>
                                    </tr>
                              	</thead>
	                              <tbody style="font-size: 10px">
	                              @foreach($templet_article_data as $templet_article_information)
	                                     <tr style="font-size: 12px;">
	                                         <td>{{$templet_article_information->templet_id}}</td>
                                            <td>{{$templet_article_information->article_name}}</td>
                                            <td>{{$templet_article_information->author}}</td>
                                            <td>{{$templet_article_information->publisher}}</td>
                                            <td>{{$templet_article_information->language}}</td>
                                            <td>{{$templet_article_information->isbn}}</td>
	                                         
	                                     </tr>
								            	@endforeach  
	                             </tbody>
                         		</table>
                       		</div>
                  	</div>
             </div>
             <p class='p_tag_report'>Print Date &nbsp;:&nbsp;{{date('d-m-Y')}}</p>
      			 <p class='p_tag_report' style='text-align:center'>Develop by : Codebool Software Company Ltd.<br>website : http://codebool.com<br>Contact No :01621370238</p>
      </div>

       


</body>
</html>



