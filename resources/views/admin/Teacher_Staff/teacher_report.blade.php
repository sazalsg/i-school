@extends('admin.index')
@section('Title','Teacher Report')
@section('breadcrumbs','Teacher Report')
@section('breadcrumbs_link','/teacher_info_report')
@section('breadcrumbs_title','Report')

@section('content')
 
@if (Session::has('success'))
    <div class="alert alert-success alert-dismissible fade in">
                <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong> {{ Session::get('success') }}
    </div>
   
@endif

@if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissible fade in">
        <ul  style='list-style:none'>
            @foreach ($errors->all() as $error)
                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> &nbsp;{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
	<div class="container">
  		<h2><i class="fa fa-list-alt" aria-hidden="true"></i> &nbsp Teacher's Report</h2> <!-- Tab Heading  -->
 		<p title="Transport Details">{{Session::get('school.system_name')}} Teacher Report</p> <!-- Transport Details -->


 		
     <div class='row'>
         
         <div class="panel panel-default" >
          <div class="panel-body text-left">
             <ul class='dropdown_test'>
             
            <li><a href='/home'><i class="fa fa-tachometer" aria-hidden="true"></i> &nbsp;Homes</a></li>
             <li><a href='/teacher_info'><i class="fa fa-user" aria-hidden="true"></i> Add Teacher</a></li>
            <li><a href='/staff_info'><i class="fa fa-street-view" aria-hidden="true"></i> Add Staff</a></li>
            <li><a href='/staff_report'><i class="fa fa-address-book-o" aria-hidden="true"></i> Staff's Report </a></li>
             </ul>
          </div>
        </div>



      <div class="controls text-right">
                <div data-toggle="buttons-checkbox" class="btn-group">
                  <button  class="btn btn-default" title='Export PDF' type="button"><a target="_blank" href="/teacher_report_pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></button>

                  <button class="btn btn-default" title='Export Excel' type="button"><a  href="/teacher_report_excle"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a></button>
                  
                  <button class="btn btn-default" title='Preview' ttype="button"><a target="_blank" href="/teacher_report_pdf"><i class="fa fa-street-view" aria-hidden="true"></i></a></button>
                  
                  <button id='print' class="btn btn-default" title='Print' type="button"><i class="fa fa-print" aria-hidden="true"></i></button>

                </div>
        </div>
    </div>


		<div class="tab-content"> <!-- Transport List Report  -->

		    	<div id="home" class="tab-pane fade in active">
		      		<div class="widget-box">
          				<div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            				<h5>Applicant Teacher's Data table</h5>
          				</div>

          		<div class="widget-content nopadding">
		            <table class="table table-bordered data-table">

			              <thead>
			                <tr>
                        <th>Teacher ID</th>
			                  <th>Teacher Name</th>
			                  <th>Mobile</th>
			                  <th>Email</th>
			                  <th>Job Type</th>
                        <th>Department</th>
			                  <th>Picture</th>
			                  <th>Actions</th>
			                </tr>
			              </thead>


			              <tbody>
                        @foreach($teacher_report as $teacher_reports)      
			                <tr class="gradeX">
                        <td>{{$teacher_reports->teacher_id}}</td>
			                  <td>{{$teacher_reports->teacher_name}}</td>
			                  <td>{{$teacher_reports->mobile_no}}</td>
			                  <td>{{$teacher_reports->email}}</td>
			                  <td>{{$teacher_reports->job_type}}</td>
                         <td style="width: 8%;"><img onerror="this.src='img/blankavatar.png'" style="height: 16%;" src='{{URL::asset("img/backend/teacher_staff/$teacher_reports->teacher_id")}}.jpg'></td>
			                   <td>{{$teacher_reports->work_department}}</td>
			                  <td class="center">

			                  	<div class="display_status">
                                {{Form::open(['url'=>"teacher_info/$teacher_reports->teacher_id/edit" ,'method'=>'GET'])}}
                                    {{Form::submit('Edit',['class'=>'btn btn-primary'])}} 
                                    {{Form::close()}}
                                    {{Form::open(['url'=>"teacher_info/$teacher_reports->teacher_id" ,'method'=>'DELETE'])}}
                                    
                                    {{Form::submit('Delete',['class'=>'btn btn-danger','onclick'=>"return confirm('Are you sure you want to Remove ?')"])}}
                                    {{Form::close()}}
								</div>
			                  </td>
			                </tr>
                        @endforeach   
			              </tbody>
            			</table>
          			</div>
        		</div>
		    </div>






		</div>
	</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

 <script type="text/javascript">
 $(document).ready(function()
 {
    $("#print").click(function()
     {
      
          var w = window.open('/teacher_report_pdf');

          w.onload = function()
          {
              w.print();
          };
      
    });
});

 </script>

 @stop
