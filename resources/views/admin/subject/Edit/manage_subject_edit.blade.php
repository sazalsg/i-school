@extends('admin.index')
@section('Title',"Subject ")
@section('breadcrumbs','subject_one_class')
@section('breadcrumbs_link','/subject_one_class')
@section('breadcrumbs_title','Subject ')
@section('content')
<div class="container">


@if (Session::has('success'))
    <div class="alert alert-success alert-dismissible fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong> {{ Session::get('success') }}
    </div>
   
@endif


@if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissible fade in">
        <ul  style='list-style:none'>
            @foreach ($errors->all() as $error)
                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> &nbsp;{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


  
  <h2><i class="fa fa-check-square-o" aria-hidden="true"></i> Edit  Class Subject Details  </h2>
  <p title="Transport Details">{{Session::get('school.system_name')}}( {{Session::get('school.school_eiin')}} )  subject Details</p>
  
 
  <br/>
<div class='row'>
     <div class="panel panel-default" >
      <div class="panel-body text-left">
         <ul class='dropdown_test'> 
            <li><a href='/home'><i class="fa fa-tachometer" aria-hidden="true"></i> &nbsp;Home</a></li>
            <li><a href='/academic_syllabus'><i class="fa fa-list-alt" aria-hidden="true"></i> &nbsp; Academic Syllabus</a></li>
            <li><a href='/marks_publish'><i class="fa fa-calendar-check-o" aria-hidden="true"></i>&nbsp; Publish Marks</a></li>
            <li><a href='/notice_board'><i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;NoticeBoard</a></li>
         </ul>
      </div>
    </div>



  
</div><br/>


  <div class="tab-content">
    <!-- Transport List Report  -->
    


    
      <div>
        <div class="widget-box">
          <div class="widget-title">
            <span class="icon"><i class="icon-info-sign"></i></span>
            <h5>Edit Class  Subject Details </h5>
          </div>

          <div class="widget-content nopadding">
          {{Form::open(['url'=>"/manage_subject/$subject_info->id",'class'=>'form-horizontal','method'=>'put','name'=>'basic_validate','id'=>'basic_validate','novalidate'=>'novalidate'])}}
            
              <div class="control-group" hidden="hidden">
              {{Form::label('User','',['class'=>'control-label'])}}
                
                <div class="controls">
                {{Form::text('user',Auth::user()->name,['id'=>'required','placeholder'=>'Subject Name'])}}
                </div>
              </div>


              
              <div class="control-group">
              {{Form::label('Subject Name','',['class'=>'control-label'])}}
                
                <div class="controls">
                {{Form::text('subject_name',$subject_info->subject_name,['id'=>'required','placeholder'=>'Subject Name','title'=>'subject_name'])}}
                </div>
              </div>
               
                <div class="control-group">
              {{Form::label('Subject Code','',['class'=>'control-label'])}}
                
                <div class="controls">
                {{Form::text('subject_code',$subject_info->subject_code,['id'=>'required','placeholder'=>'Subject Code','title'=>'subject_code'])}}
                </div>
              </div>

               <div class="control-group">
              {{Form::label('Subject Mark','',['class'=>'control-label'])}}
                
                <div class="controls">
                {{Form::text('subject_mark',$subject_info->subject_mark,['id'=>'required','placeholder'=>'Subject Mark','title'=>'subject_mark'])}}
                </div>
              </div>

               <div class="control-group">
              {{Form::label('Subject Credit','',['class'=>'control-label'])}}
                
                <div class="controls">
                {{Form::text('subject_credit',$subject_info->subject_credit,['id'=>'required','placeholder'=>'Subject Credit','title'=>'subject_credit'])}}
                </div>
              </div>


              <div class="control-group">
                
                {{Form::label('Class','',['class'=>'control-label','title'=>'class'])}}
                <div class="controls">

                @php $subject_class_array[$subject_info->class]=$subject_info->class @endphp
                @foreach($class_list as $class_list_data)
                  @php $subject_class_array[$class_list_data->class_name]=$class_list_data->class_name @endphp
                @endforeach

                
                  {{Form::select('class',$subject_class_array)}}
                </div>
              </div>

              <div class="control-group">
                
                {{Form::label('Teacher','',['class'=>'control-label','title'=>'teacher'])}}
                <div class="controls">

               @php $teacher[$subject_info->teacher]=$subject_info->teacher @endphp
               
                @foreach($teacher_list as $teacher_list_data)
                @php $teacher[$teacher_list_data->teacher_name]=$teacher_list_data->teacher_name @endphp
                @endforeach

                  {{Form::select('teacher',$teacher)}}
                </div>
              </div>

              <div class="form-actions">
              {{Form::submit('submit',['class'=>'btn btn-success'])}}
              </div>

            {{Form::close()}}
          </div>
        </div>
      </div>
    

  </div>
</div>
@stop