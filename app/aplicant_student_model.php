<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class aplicant_student_model extends Model
{
    protected $fillable = ['applicant_id', 'student_name', 'parent_name','relation','session','class','admission_test','department','birth_date','gender','postal_code','phone','email','batch','shift'];
    protected $table='applicant_student';
    // protected $primaryKey='applicant_id';
    protected $primaryKey='applicant_id';

    public  function roles_rule(){

  		return [
	    'student_name' => 'required',
	    // 'parent_name' => 'required',
	    // 'relation' => 'required',
	    'session' => 'required',
	    'class' => 'required',
	    'admission_test' => 'required',
	    'department' => 'required',
	    'birth_date' => 'required',
	    'gender' => 'required',
		'postal_code' => 'required',
	    'phone' => 'required',
	    'email' => 'required',
	    'image'=>'image|mimes:jpeg,png',
	    ];
	}
}
