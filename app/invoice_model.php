<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class invoice_model extends Model
{
    protected $table='invoice';
    protected $primaryKey='invoice_id';
    protected $fillable=['class_name','student_roll','title','waber','waber_purpose','Payment','account_name','invoice_creator','templete_id','on_net_total','cash_status'];
}
