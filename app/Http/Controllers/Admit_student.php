<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\students_child;
use App\students;
use App\parents_info_model;
use App\manage_class_model;
use Redirect;
use Session;
use App\manage_section_model;
use Validator;
use Hash;
use App\ov_setup_model;
use App\general_settings_model;
use App\make_autoname;
class admit_student extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // echo "<pre>";
        // print_r();

        $session=ov_setup_model::where('type','Session')->get();
        $shift=ov_setup_model::where('type','Shift')->get();
        $batch=general_settings_model::first();
        $obj=new make_autoname;
        $name=$obj->autoname();


       return view('admin.students.admit_student',['parents_data'=>parents_info_model::select('name','parent_id')->get(),'class_data'=>manage_class_model::select('class_name','id')->get(),'manage_section'=>Manage_section_model::select('section_name')->get(),'batch'=>$batch,'session'=>$session,'shift'=>$shift,'autoname'=>$name]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $make_autoname=new make_autoname;

        $students_obj=new students;
        $validation=Validator::make($request->all(),$students_obj->roles_rule());
        if($validation->fails())
        {
             return back()->withInput()->withErrors($validation);
        }
        else
        {

    $check_validaity=students::where('reg_number',$request->reg_number)->orWhere('email',$request->email)->first();
    if(!$check_validaity):
            $Data=$request->all();
            $Data=array_add($Data,'status','Active');

            if(!$request->email):
                $Data=array_set($Data,'email',$request->roll);
            endif;

            if($request->password):
                $Data=array_set($Data,'password',Hash::make($request->password));
            else:
                $Data=array_set($Data,'password',Hash::make($request->roll));
            endif;
            $Data=array_set($Data,'roll',$make_autoname->autoname());

            $students_obj->fill($Data)->save();

            if(!empty($request->post_office)):
                $students_child_obj=new students_child;
                $students_child_obj->fill($request->all())->save();
            endif;

            if($request->hasfile('student_image')):
                $file_path="img/backend/student";
                $file_name=$request->roll.".jpg";
                $request->file('student_image')->move($file_path,$file_name);

            endif;
            Session::flash('success',"New Student Information Are Successfully Added");
            return Redirect::back();
    else:
            Session::flash('wrong',"Sorry Dupliacte Regestration Number Or Email Are Not Allowed");
            return Redirect::back();
    endif;
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.students.Edit.admit_student_edit',['parents_data'=>parents_info_model::select('name','parent_id')->get(),'class_data'=>manage_class_model::select('class_name','id')->get(),'manage_section'=>Manage_section_model::select('section_name')->get(),'students_data'=>students::where('roll',$id)->first()]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
