<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\article_issue_model;
use App\article_model;
use App\article_recive_model;
use Session;
use Validator;
use Redirect;


class article_recive extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.Libray.article_recive',['article_recive_info'=>article_issue_model::where('status','Recived')->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {     

        $article_id=$request->article_id;
        $article_issue_info=new article_issue_model;
        $validation=Validator::make($request->all(),$article_issue_info->roles_rule());
        if($validation->fails())
        {
           return back()->withInput()->withErrors($validation); 
        }
        else{
            article_issue_model::where('article_id',$article_id)->update(['status'=>'Recived']);
            article_model::where('article_id',$article_id)->update(['available_status'=>'Avaialble']);
             session()->flash('success', "$request->article_name Article Successfully Recived");
            return back()->with('success',"$request->article_name Article Successfully Recived");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.Libray.Edit.article_recive_edit',['article_recive_info'=>article_issue_model::where('article_issue_id',$id)->first()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        article_issue_model::where('article_issue_id',$id)->first()->fill($request->all())->save();  
        Session::flash('success',"$request->article_name Article Information Are Successfully Updated");
        return back()->with('success',"$request->article_name Article Information Are Successfully Updated");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        article_issue_model::where('article_issue_id',$id)->delete();
        session()->flash('success', "Delete Operation Successfully Completed");
       return back()->with('success',"Delete Operation Successfully Completed");
    }
}
