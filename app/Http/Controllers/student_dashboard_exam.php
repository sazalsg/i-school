<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\exam_list_model;
class student_dashboard_exam extends Controller
{ /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $user_auth_check=Session::get('student_or_parents_login');
        if($user_auth_check=='Loggedin')
        {   
            $class=Session::get('class');
             $roll=Session::get('roll');
             return view('student.student_dashboard_exam',['exam_list'=>exam_list_model::where('publish','Yes')->get()]);
        }
        else
        {
            abort(404);
        }
        
    }
}
