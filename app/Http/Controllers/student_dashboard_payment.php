<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\invoice_model;
use Session;
class student_dashboard_payment extends Controller
{
       /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $user_auth_check=Session::get('student_or_parents_login');
        if($user_auth_check=='Loggedin')
        {

          $roll=Session::get('roll');
          $class=Session::get('class');
          return view('student.student_dashboard_payment',['invoice_data'=>invoice_model::where('student_roll',$roll)->first()]);
        }
        else
        {
            abort(404);
        }
        
    }
}
