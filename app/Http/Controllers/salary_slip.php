<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use File;
use Crypt;
use Validator;
use App\salary_slip_model;
use App\salary_slips_EarningComponent;
use App\salary_slips_DeducationComponent;
use App\teacher_model;


use App\salary_structure_deduction_component_model;
use App\salary_structure_earning_component_model;
use App\salary_structure_model;
use App\salary_structure_teacher_staff_details_model;
class salary_slip extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        return view('admin.payroll.slary_slip');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $salary_slip_model=new salary_slip_model;
        $salary_slip_model->fill($request->all())->save();
            
        Session::flash('success','Add Succesfully');
        
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function teacher_name_get(Request $request)
    {
        $Data=teacher_model::where('status',$request->teacher_or_staff)->get();
        echo "<option></option>";
        foreach($Data as $Data_found)
        {
            echo "<option value=\"$Data_found->teacher_id\">$Data_found->teacher_name</option>";
        }
        
    }

    public function salary_slip_details(Request $request)
    {
        $employee_find=salary_structure_teacher_staff_details_model::where('teacher_or_staff_name',$request->teacher_or_staff_name)->first();
        
        if($employee_find){
            $Active_chek=salary_structure_model::where('frequency',$request->payroll_frequecy)->where('payroll_structure_id',$employee_find->parent)
            ->where('status','Yes')
            ->first();
            if($Active_chek==false)
            {
                return "This Empoyee Are Not Found In Active Salary Structure";
            }
            else
            {
                $all_details=salary_structure_teacher_staff_details_model::where('teacher_or_staff_name',$request->teacher_or_staff_name)->first();


                $teacher_salary_details=salary_structure_teacher_staff_details_model::where('teacher_or_staff_name',$request->teacher_or_staff_name)->get()->toArray();
                $teacher_name=teacher_model::select('teacher_name')->where('teacher_id',$request->teacher_or_staff_name)->get()->toArray();
                
                $slary_structure=salary_structure_model::where('payroll_structure_id',$all_details->parent)->get()->toArray();
                
                // Earn Component Add
                $earn=salary_structure_earning_component_model::where('parent',$all_details->parent)->get()->toArray();
                
                $earn_component['earn_component']=$earn;
                $object=array_merge($teacher_salary_details[0],$teacher_name[0],$slary_structure[0]);
                array_push($object,$earn_component);


                // Deduction Component Add
                $deduction=salary_structure_deduction_component_model::where('parent',$all_details->parent)->get()->toArray();
                $deduction_component['deduction']=$deduction;
                array_push($object,$deduction_component);

                
                
                return $object;
            }
       
        }else
        {
            return "This Empoyee  Are Not Found In any Salary Structure";
        }

            
    }
}
