<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\manage_class_model;
use App\daily_attendance_model;

use App\students;
use Session;
use Validator;
use Redirect;
class daily_attendance extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.Attendance.daily_attendance',['class_data'=>manage_class_model::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


                $time=time();
               foreach ($request->status as $value)
                  {

                      $daily_attendance=new daily_attendance_model;
                      $daily_attendance->student_id=$value;
                      $daily_attendance->device_id='system_does_not_track';
                      $daily_attendance->date=$request->date;
                      $daily_attendance->time=$time;
                      $daily_attendance->status="Present";
                      $daily_attendance->save();
                 }

             session()->flash('success','successfully Submited Your Record');
             return Redirect::back();


  }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
     public function show_student_data(Request $request)
    {

        return students::where('class',$request->class_name)
                ->Where('section',$request->section)
                ->Where('department',$request->Department_info)->get();

    }
     public function show_attendance_data(Request $request)
    {

            $student_info=students::where('class',$request->class_name)
                ->Where('section',$request->section)
                ->Where('department',$request->Department_info)->get();
            // echo $request->from_date_info;
            $date1=date_create((string)$request->from_date_info);
            $date2=date_create((string)$request->to_date_info);
            $found_date_diff=date_diff($date1,$date2);
            $date_diff=$found_date_diff->format('%d');
            $from_date=$request->from_date_info;

             $from_date=date('d-m-Y',strtotime($from_date));

             $table="<table class=\"table table-bordered data-table\">";
                $table.="<thead>";
                    $table.="<tr>";
                         $table.="<th>Student Name</th>";
                        $table.="<th>Roll</th>";

                        for ($i=0;$i<$date_diff;$i++){
                            $calculate_date = strtotime($from_date . ' +1 day');
                            $from_date=date('d-m-Y',$calculate_date);

                         $table.="<th class=\"date\">$from_date</th>";
                         }

                    $table.="</tr>";
                $table.="</thead>";
                $table.="<tbody>";


                  foreach ($student_info as $value) {
                    $table.="<tr>";
                        $table.="<td>$value->student_name</td>";
                        $table.="<td>$value->roll</td>";


                        $request_date=$request->from_date_info;

                        for ($i=0;$i<$date_diff;$i++)
                         {
                            $a = strtotime($request_date . ' +1 day');
                            $request_date=date('d-m-Y',$a);


                              $match_values=daily_attendance_model::where('student_id',$value->roll)->where('date',$request_date)->where('status','Present')->first();

                             if($match_values)
                             {
                                 $table.="<td><input type=\"checkbox\" checked/></td>";
                             }
                             else
                             {
                                 $table.="<td><input type=\"checkbox\"/></td>";
                             }
                         }

                }

                $table.="</tbody>";
            $table.="</table>";
            echo $table;

    }


}
