<?php

namespace App\Http\Controllers;
use  App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\membership_model;
use Session;
use Validator;
use Redirect;
class membership extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.Libray.membership',["membership_data"=>membership_model::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $membership_info=new membership_model;
        $validation=Validator::make($request->all(),$membership_info->roles_rule());
         if($validation->fails())
        {
             return back()->withInput()->withErrors($validation);
        }
        else
        {

            $member_data=$request->all();
            $member_data=array_add($member_data,'member_id',time());
            $membership_info->fill($member_data)->save();

          Session::flash('success',"$request->member_name Information are Successfully Inserted");
          return Redirect::back();
         }
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.Libray.Edit.membership_edit',['membership_data'=>membership_model::where('member_id',$id)->first()]); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $membership_info=new membership_model;
        $membership_info->where('member_id',$id)->first()->fill($request->all())->save();
        Session::flash('success',"$request->member_name Information are Successfully Update");
        return back()->with('success',"$request->member_name Information are Successfully Update");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       membership_model::where('member_id',$id)->delete();
       Session::flash('success','Delete Operation Successfully Inserted');
       return Redirect::back();
    }
    public function membership_data_get(Request $request)
    {
       return membership_model::where('roll_number',$request->roll_number)->first();
       
    }
}
