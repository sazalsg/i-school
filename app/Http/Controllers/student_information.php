<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\students;
use Session;
use Redirect;
use App\parents_info_model;
use App\manage_class_model;
use App\students_child;
use Validator;
use Hash;
use File;
class student_information extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        Session::flash('error','You Are Going True Wrong Track');
        return Redirect::back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {

    }

  


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         return view('admin.students.student_information',['student_data_table'=>students::where('class',$id)->get()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        return view('admin.students.Edit.admit_student_edit',['students_data'=>students::where('roll',$id)->first(),'parents_data'=>parents_info_model::all(),'class_data'=>manage_class_model::all(),'student_child'=>students_child::where('roll',$id)->first()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $students_obj=new students;
        $validation=Validator::make($request->all(),$students_obj->student_info_edit());
        if($validation->fails())
        {
             return back()->withInput()->withErrors($validation);
        }
        else
        {


            $Data=$request->all();
            if($request->password){
                $Data=array_set($Data,'password',Hash::make($request->password));
            }
            $students_obj->where('roll',$id)->first()->fill($Data)->save();

            if(!empty($request->post_office) or !empty($request->home_district) or !empty($request->division) or !empty($request->village_name)):
                if(students_child::where('roll',$id)->first()):
                students_child::where('roll',$id)->first()->fill($request->all())->save();
                else:
                    $students_child_obj=new students_child;
                    $students_child_obj->fill($request->all())->save();
                endif;
            endif;

            if($request->hasfile('student_image')):
                $file_path="img/backend/student/";
                $file_name=$request->roll.".jpg";
                $request->file('student_image')->move($file_path,$file_name);

            endif;
            Session::flash('success',"$request->student_name ($request->roll) Profile are Succesfully Update");
            return Redirect::back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        students::where('roll',$id)->delete();
        students_child::where('roll',$id)->delete();

         $my_file="img/backend/student/$id".".jpg";
                if (File::exists($my_file))
                {
                    File::delete($my_file);

                }
         Session::flash('success',"Successfully Deletd This Student Information");
         return Redirect::back();


        // if($request->hasfile('student_image')):
        //         $file_path="img/backend/student/";
        //         $file_name=$request->roll.".jpg";
        //         $request->file('student_image')->move($file_path,$file_name);

        //     endif;
    }
}
