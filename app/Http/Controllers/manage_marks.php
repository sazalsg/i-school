<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\exam_list_model;
use App\manage_class_model;
use App\students;
use App\exam_grade_model;

use Auth;
use App\manage_mark;
use App\manage_mark_child;
use Redirect;
class manage_marks extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $exam_list=exam_list_model::where('publish','Yes')->get();
        $class_list=manage_class_model::all();
        return view('admin.exam.manage_marks',['exam_list'=>$exam_list,'class_list'=>$class_list]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if(manage_mark::where('exam_name',$request->exam_name)
            ->where('class_name',$request->class_name)
            ->where('section',$request->section)
            ->where('Department',$request->Department)
            ->where('subject',$request->subject)->first())
           {
                session()->flash('wrong','Already Submitted Your Record For This Info Purpose');
                return Redirect::back();

           }
           else
           {

            $Data=$request->all();
            $Data=array_add($Data,'entry_here',Auth::user()->name);
            
            $manage_mark=new manage_mark;
            $manage_mark->fill($Data)->save();
            
            echo count($request->roll);
            for ($i=0;$i<count($request->roll);$i++)
            {
               
                        if($request->mark[$i])
                        {
                            $manage_mark_child=new manage_mark_child;
                            $manage_mark_child->parents=$request->exam_name;
                            $manage_mark_child->roll=$request->roll[$i];
                            $manage_mark_child->mark=$request->mark[$i];
                            $manage_mark_child->cgpa=$request->cgpa[$i];
                            $manage_mark_child->grade_name=$request->grade_name[$i];
                            $manage_mark_child->comment=$request->comment[$i];
                            $manage_mark_child->save();

                            
                        }

                        

               
                            
            }

            session()->flash('success','successfully Submited Your Record');
            return Redirect::back();

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function mange_mark_student(Request $request)
    {
        $student_information=students::where('class',$request->class_name)
                            ->Where('section',$request->section_name)
                            ->Where('department',$request->Department)->get();


       $table="<table class=\"table table-bordered\">";
         $table.="<thead>";
           $table.="<tr>";
             $table.="<th>#</th>";
             $table.="<th>Roll</th>";
             $table.="<th>Name</th>";

             $table.="<th>Marks Obtained</th>";
             // $table.="<th>Garde</th>";
             // $table.="<th>CGP</th>";
             $table.="<th>Comment</th>";
           $table.="</tr>";
         $table.="</thead>";

         $table.="<tbody>";
         $i=1;
           foreach ($student_information as $value) {

                    $table.="<tr>";
                     $table.="<td>$i</td>";
                     $table.="<td>$value->roll</td>";
                     $table.="<td>$value->student_name</td>";

                     $table.="<td id='result_mark_ovi'><input  type=\"text\" class=\"marks\"></td>";
                     // $table.="<th>Garde</th>";
                     //   $table.="<th>CGP</th>";
                     $table.="<td><input class='comment' type=\"text\"></td>";

                   $table.="</tr>";
                   $i++;
            }
         $table.="</tbody>";
     $table.="</table>";

    $min_value=exam_grade_model::min('mark_from');
    $max_value=exam_grade_model::min('mark_upto');
    $total_student=count($student_information);
    return $max_min=[$min_value,$max_value,$student_information,$total_student];
      
        


     
    }

//     public function manage_mark_for_student_show(Request $request)
//     {
//         $student_information=students::where('class',$request->class_name)
//             ->Where('section',$request->section_name)
//             ->Where('department',$request->Department)->get();


//         $table="<table class=\"table table-bordered\">";
//           $table.="<thead>";
//             $table.="<tr>";
//               $table.="<th>#</th>";
//               $table.="<th>Roll</th>";
//               $table.="<th>Name</th>";

//               $table.="<th>Marks Obtained</th>";
//               // $table.="<th>Garde</th>";
//               // $table.="<th>CGP</th>";
//               $table.="<th>Comment</th>";
//             $table.="</tr>";
//           $table.="</thead>";

//           $table.="<tbody>";
//           $i=1;
//             foreach ($student_information as $value) {

//                      $table.="<tr>";
//                       $table.="<td>$i</td>";
//                       $table.="<td>$value->roll</td>";
//                       $table.="<td>$value->student_name</td>";

//                       $table.="<td id='result_mark_ovi'><input  type=\"text\" class=\"marks\"></td>";
//                       // $table.="<th>Garde</th>";
//                       //   $table.="<th>CGP</th>";
//                       $table.="<td><input class='comment' type=\"text\"></td>";

//                     $table.="</tr>";
//                     $i++;
//              }
//           $table.="</tbody>";
//       $table.="</table>";

//       echo $table;

// //        $min_value=exam_grade_model::min('mark_from');
// //        $max_value=exam_grade_model::min('mark_upto');
// //        $total_student=count($student_information);
// //        return $max_min=[$min_value,$max_value,$student_information,$total_student];
//     }

    public function grade_and_cgp_find(Request $request)
    {
        $exam_grade_value=exam_grade_model::all();
        $find_grade="0.0";
        $find_grade_name="Fail";
        foreach ($exam_grade_value as  $value) {
            
            if($request->pass_mark >= $value->mark_from and $request->pass_mark <= $value->mark_upto)
            {
                $find_grade=$value->grade_point;
                $find_grade_name=$value->grade_name;
                break;
            }
        }
        $grade_name_grade_pont=[$find_grade,$find_grade_name];
        return $grade_name_grade_pont;
    }
}



