<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\manage_subject_model;
use Session;
class student_dashboard_subject extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $user_auth_check=Session::get('student_or_parents_login');
        if($user_auth_check=='Loggedin')
        {   
            $class=Session::get('class');

             return view('student.student_dashboard_subject',['subject_data'=>manage_subject_model::where('class',$class)->get()]);
        }
        else
        {
            abort(404);
        }
        
    }

}
