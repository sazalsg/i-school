<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\manage_class_model;
use App\teacher_model;
use App\manage_subject_model;
use App\component_model;
use App\subject_component;
use Session;
use Validator;
class manage_subject extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $manage_subject=new manage_subject_model;
        $validation=Validator::make($request->all(),$manage_subject->validation_rule());
        if($validation->fails())
        {
            return back()->withInput()->WithErrors($validation);
        }
        else
        { 
            $manage_subject_data=$request->all();
          
             $manage_subject->fill($manage_subject_data)->save();

              $subject_component_data=new subject_component;
              $subject_component_data->subject_component_id=time();
              $subject_component_data->subject_id=$request->id;
              $subject_component_data->component_id=$request->component_id;
        
              $subject_component_data->save();
           

        Session::flash('success',"$request->subject_name Infomation Are successfully Inserted");
        return back()->with('success',"$request->subject_name Infomation Are successfully Inserted");
        }   
       
       

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        return view('admin.subject.manage_subject',['teacher'=>teacher_model::all(),'request_class'=>$id,'subject_list'=>manage_subject_model::where('class',$id)->get(),'component_data'=>component_model::all()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.subject.Edit.manage_subject_edit',['subject_info'=>manage_subject_model::where('id',$id)->first(),'request_class'=>$id,'class_list'=>manage_class_model::all(),'teacher_list'=>teacher_model::all(),'component_data'=>component_model::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        manage_subject_model::where('id',$id)->first()->fill($request->all())->save();
        Session::flash('success',"$request->subject_name Infomation Are successfully Updated");
        return back()->with('success',"$request->subject_name Infomation Are successfully Updated");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        manage_subject_model::where('id',$id)->delete();
        Session::flash('success','Delete Operation Successfully Completed');
        return back()->with('success','Delete Operation Successfully Completed');
    }
}