<?php

namespace App\Http\Controllers;
use Session;
use Crypt;
use Validator;
use App\exam_grade_model;
use Illuminate\Http\Request;

class exam_grade extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.exam.exam_grade',['exam_grade'=>exam_grade_model::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $exam_grade=new exam_grade_model;
        
        $validation=Validator::make($request->all(),$exam_grade->validation());
        if($validation->fails())
        {
           return back()->withInput()->withErrors($validation); 
        }
        else{
            $exam_grade->fill($request->all())->save();
             session()->flash('success', "New Grade Added ");
            return back()->with('success','New Grade Added ');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.exam.exam_grade_edit',['exam_grade'=>exam_grade_model::where('id',$id)->first()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $exam_grade=new exam_grade_model;
        
        $validation=Validator::make($request->all(),$exam_grade->validation());
        if($validation->fails())
        {
           return back()->withInput()->withErrors($validation); 
        }
        else{
            exam_grade_model::where('id',$id)->first()->fill($request->all())->save();
             session()->flash('success', " Exam Grade updated successfully ");
            return back()->with('success','Exam Grade updated successfully ');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
