<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\aplicant_student_model;
use session;
use App\applicant_student_child_model;
use Validator;
use File;
use App\exam_list_model;
use App\parents_info_model;
use App\manage_class_model;
use App\manage_department_model;
use Redirect;
use App\general_settings_model;
use App\ov_setup_model;

class Aplicant_student extends Controller
{
    // public __construct()
    // {
    //     $this->middleware('permission:students')->only('create');
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $session=ov_setup_model::where('type','Session')->get();
        $shift=ov_setup_model::where('type','Shift')->get();
        $batch=general_settings_model::first();
        return view('admin.students.aplicant_student',['exam_lsit'=>exam_list_model::all(),'parents_data'=>parents_info_model::all(),'manage_class'=>manage_class_model::all(),"department"=>manage_department_model::all(),'batch'=>$batch,'session'=>$session,'shift'=>$shift]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $check_email=aplicant_student_model::where('email',$request->email)->first();
        
        if($check_email)
        {
             session()->flash('wrong', "Sorry ! Duplicate Email Are Not Allowed");
            return back()->withInput();
        }
        else
        {
            $aplicant_student=new aplicant_student_model;
            $validation=Validator::make($request->all(),$aplicant_student->roles_rule());
            if($validation->fails())
            {
                 return back()->withInput()->withErrors($validation);
            }
            else
            {
               
                $aplicant_student->fill($request->all())->save();
                
                if($request->hasfile('image')):
                    $this->image_upload($request,$request->applicant_id);
                endif;
                
                if(!empty($request->post_office) or !empty($request->home_district) or !empty($request->division) or !empty($request->village_name))
                {
                    $applicant_student_child_model=new applicant_student_child_model;
                    $applicant_student_child_model->fill($request->all())->save();
                }


                session()->flash('success', "Applicant Information Added Operation Are Succesfully Completed : Thank You ! ");
                return back()->with('success','Applicant Information Added Operation Are Succesfully Completed : Thank You ! ');
            }
        }




    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $session=ov_setup_model::where('type','Session')->get();
        $shift=ov_setup_model::where('type','Shift')->get();
        $batch=general_settings_model::first();
       

        return view('admin.students.applicant_student_edit',['applicant_student'=>aplicant_student_model::where('applicant_id',$id)->first(),'aplicant_student_child'=>applicant_student_child_model::where('parent',$id)->first(),'exam_lsit'=>exam_list_model::all(),'parents_data'=>parents_info_model::all(),'manage_class'=>manage_class_model::all(),"department_data"=>manage_department_model::all(),'batch'=>$batch,'session'=>$session,'shift'=>$shift]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $aplicant_student=new aplicant_student_model;
        $validation=Validator::make($request->all(),$aplicant_student->roles_rule());
        if($validation->fails())
        {
             return back()->withInput()->withErrors($validation);
        }
        else
        {
            
            if($request->hasfile('image')):
                $this->image_upload($request,$id);
            endif;

            $data=array_except($request->all(),['_method','_token','post_office','home_district','division','village_name']);

           aplicant_student_model::where('applicant_id',$id)->first()->fill($data)->save();
           applicant_student_child_model::where('parent',$id)->first()->fill($request->all())->save();
            session()->flash('success', "$request->student_name Information Succesfully Updated");
           return Redirect::back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       aplicant_student_model::where('applicant_id',$id)->delete();
       applicant_student_child_model::where('parent',$id)->delete();
       
                $my_file="img/backend/aplicant_student/$id".".jpg";
        
                if (File::exists($my_file))
                {
                   File::Delete($my_file);
                }
        
    }

    public function image_upload(Request $request,$applicant_id)
    {
        $file_path="img/backend/aplicant_student";
        $fil_name=$applicant_id.'.jpg';
        $request->file('image')->move($file_path,$fil_name);



    }
}
