<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\invoice_component_model;
use App\chart_of_accounts_model;
use App\manage_class_model;
use App\students;
use App\invoice_templete_model;
use App\invoice_model;
use Auth;
use Session;
use Redirect;
use Validator;


class student_payment_entry extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   

        $class_name=manage_class_model::all();
        $Account=chart_of_accounts_model::where('parent_account','Asset')->get();
        $component=invoice_component_model::all();
        $invoice=invoice_model::all();
        return view('admin.Account.student_payment_entry',['invoice_component'=>$component,'Account'=>$Account,'class_name'=>$class_name,'invoice_data'=>$invoice]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $invoice_role=new invoice_templete_model;
        $validation=Validator::make($request->all(),$invoice_role->roles_rule());
        if($validation->fails())
        {
           return back()->withInput()->withErrors($validation); 
        }
        else
        {



               if($request->invoice_type=="Single Invoice"):
                    $templete_id=invoice_templete_model::where('class',$request->class_name)->where('default_status','Default')->first();
                    $invoice_creator=Auth::user()->name;
                    $request_value=$request->all();
                    $request_value=array_add($request_value,'templete_id', $templete_id->id);
                    $request_value=array_add($request_value,'invoice_creator',$invoice_creator);
                    
                    $invoice=new invoice_model;
                    $invoice->fill($request_value)->save();
               else:



            $all_student=students::where('class',$request->class_name)->get();
                   foreach ($all_student as $value) {
                       
            $Name=students::select('student_name')->where('roll',$value->roll)->first();
            $templete_id=invoice_templete_model::where('class',$request->class_name)->where('default_status','Default')->first();
            $invoice_creator=Auth::user()->name;

            $request_value=$request->all();
            $request_value=array_add($request_value,'templete_id', $templete_id->id);
            $request_value=array_add($request_value,'invoice_creator',$invoice_creator);
            $request_value=array_set($request_value,'student_roll',$value->roll);   
            $request_value=array_set($request_value,'student_name',$invoice_creator);   
            $invoice=new invoice_model;
            $invoice->fill($request_value)->save();
                       
                   }

                endif;

                Session::flash('success','Invoice Create Succesfully');
                return Redirect::back();


        }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function student_info(Request $request)
    {

        $students=students::where('class',$request->class_name)->get();
        $option="<option value=''>Data Found</option>";
        foreach ($students as $value) {
            $option.="<option value=\"$value->roll\">$value->student_name</option>";  
        }
        echo $option;

    }

    public function templete_desgin(Request $request)
    {
       
        return invoice_templete_model::where('default_status','Default')->where('class',$request->class_name)->first();
    

    }
}
