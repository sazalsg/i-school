<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\Http\Controllers\Controller;
use Session;
use Validator;
use App\notice_board_model;
use App\stuent_notice_child_model;
use App\teacher_notice_child_model;
use App\class_notice_child_model;
use App\parent_notice_child_model;
use App\teacher_model;
use App\manage_class_model;
use App\parents_info_model;
use App\students;
use SoapClient;
use Redirect;

class notice_board extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.notice_board.notice_board',["notice_board_data"=>notice_board_model::all(),'student_notice'=>notice_board_model::where('to','student')->get(),'teacher_notice'=>notice_board_model::where('to','Teacher')->get(),'class_notice'=>notice_board_model::where('to','Class')->get(),'parents_notice'=>notice_board_model::where('to','Parents')->get(),'teacher_data'=>teacher_model::all(),'class_data'=>manage_class_model::all(),'parents_data'=>parents_info_model::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $Data=$request->all();


        $notice_board_model_obj=new notice_board_model;
        $notice_board_model_obj->fill($Data)->save();


        if($request->to=="Student"):
            $stuent_notice_child_model_obj=new stuent_notice_child_model();

            $student_roll=$request->sw_student_roll;
            $student_info=students::where('roll',$student_roll)->first();
            $parent_id=$student_info->parent_name;
            $parent_data=parents_info_model::where('parent_id',$parent_id)->first();
            $parent_mobile=$parent_data->phone;
            $count=count($parent_mobile);
            $soapClient = new SoapClient("https://api2.onnorokomsms.com/sendsms.asmx?WSDL");
            $paramArray = array(
                                'userName'=>"01849942053",
                                'userPassword'=>"25102",
                                'mobileNumber'=> $parent_mobile,
                                'smsText'=>"Hi I Am I SCHOOL SOFTWARE",
                                'type'=>"TEXT",
                                'maskName'=> "",
                                'campaignName'=>'',
                                );
            if($count>1)
            {
              $value = $soapClient->__call("OneToMany", $paramArray);   
            }
            else
            {
                $value = $soapClient->__call("OneToOne", array($paramArray));
          
            }
            if($value->OneToOneResult=='1900')
            {
                echo "Successfully";
            }

            $stuent_notice_child_model_obj->fill($Data)->save();
             
        elseif($request->to=="Teacher"):
            $stuent_notice_child_model_obj=new teacher_notice_child_model();
            $stuent_notice_child_model_obj->fill($Data)->save();


        elseif($request->to=="Class"):
            $stuent_notice_child_model_obj=new class_notice_child_model();
            $stuent_notice_child_model_obj->fill($Data)->save();

        elseif($request->to=="Parents"):
            $stuent_notice_child_model_obj=new parent_notice_child_model();
            $stuent_notice_child_model_obj->fill($Data)->save();

        endif;

        Session::flash('success',"Successfully Added A New Notice");
        //return Redirect::back();



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

          //return view('admin.notice_board.notice_board_edit',['teacher_data'=>teacher_model::all(),'class_data'=>manage_class_model::all(),'parents_data'=>parents_info_model::all(),'notice_data'=>notice_board_model::where('notice_id',$id)->first()]);//,['templet_article_data'=>templet_article_model::where('templet_id',$id)->first()]
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        notice_board_model::where('notice_id',$id)->delete();
        parent_notice_child_model::where('notice_id',$id)->delete();
        stuent_notice_child_model::where('notice_id',$id)->delete();
        teacher_notice_child_model::where('notice_id',$id)->delete();
        class_notice_child_model::where('notice_id',$id)->delete();

       Session::flash('success','Notice Deleted Successfully');
       return Redirect::back();
    }
}
