<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class academic_syllebus_model extends Model
{
    protected $table='academic_syllebus';
    protected $primaryKey='id';
    protected $fillable=['class_name',
                        'title',
                         'description',
                         'subject',
                         'id'];
    
    
    public function validation()
    {
        return [
           'subject'=>'required',
            'title'=>'required',
            'class_name'=>'required',
            'files'=>'required|mimes:pdf'
            
            
        ];
    }
}
