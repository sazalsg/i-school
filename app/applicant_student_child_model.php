<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class applicant_student_child_model extends Model
{
    protected $fillable = ['parent', 'post_office', 'home_district','division','village_name'];
    protected $table='applicant_student_child';
    protected $primaryKey='parent';
}
