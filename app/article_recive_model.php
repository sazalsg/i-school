<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class article_recive_model extends Model
{
     protected $table='article_recive';
    protected $primaryKey='article_recive_id';
   
    protected $fillable=['article_id','article_name','member_name','issue_date','return_date','e_mail','phone','total_day','article_recive_id'];

      public  function roles_rule()
      {
  		return [

  		'article_id'=>'required',
	    'article_name' => 'required',
		'member_name' => 'required',
		'issue_date' =>'required',
	    'return_date' => 'required',
	    'e_mail' => 'required',
	    'phone' => 'required',
	    'total_day' => 'required',
		
	    ];
      }
}
