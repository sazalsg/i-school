<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class membership_model extends Model
{
  
  
    protected $table='membership';
    protected $primaryKey='member_id';
     protected $fillable = ['member_id', 'member_name', 'roll_number','reg_number','status','email','phone','from_date','to_date'];
      public  function roles_rule(){
  		return [
	    'member_name' => 'required',
	    'roll_number' => 'required',
	    'reg_number' => 'required',
	    'status' => 'required',
	    'email' => 'required',
		'phone' => 'required',
	    'from_date' => 'required',
	    'to_date' => 'required',
	    ];
	}
}
