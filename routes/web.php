<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
// Route::get('test',function(){
// 	return "Working";
// })->middleware('can:INSERT');

Route::get('/user_profile','user_profile@index');
Route::get('/','Auth\LoginController@showLoginForm');
Route::post('/auth_check','Auth\LoginController@user_check');

Route::resource('/book_list','book_list');
Route::get('/Student_dashboard','Student_dashboard@index');

Route::resource('/student_dashboard_exam','student_dashboard_exam');
Route::resource('/student_dashboard_hostel','student_dashboard_hostel');
Route::resource('/student_dashboard_payment','student_dashboard_payment');
Route::resource('/student_dashboard_class_routine','student_dashboard_class_routine');
Route::resource('/student_dashboard_transport','student_dashboard_transport');
Route::resource('/student_dashboard_library','student_dashboard_library');
Route::resource('/student_dashboard_syllabus','student_dashboard_syllabus');
Route::resource('/student_dashboard_teacher','student_dashboard_teacher');
Route::resource('/student_dashboard_subject','student_dashboard_subject');
Route::get('/syllabus_download/{id}','student_dashboard_syllabus@syllabus_download');

Route::group(['middleware' => 'auth'], function ()
{

   



	//complete start
	Route::resource('/create_admin','Create_admin');
	Route::resource('/report','report');
	Route::resource('/create_permission','Create_permission');
	Route::resource('/create_role','Create_role');
	Route::resource('/permission_role','Permission_role');

	Route::put('/extra_operation/{delete_id}','Extra_operation@delete_role_current_role');
	Route::resource('/user_access','User_access');

	Route::resource('/aplicant_student','Aplicant_student');
	Route::get('/applicant_student_report','ischool_report@applicant_student_report');


	Route::post('/applicant_student_admit_card','ischool_report@applicant_student_admit_card');


	Route::resource('/exam_list','Exam_list');
	Route::resource('/manage_class','Manage_class');
	Route::resource('/manage_section','Manage_section');


	 Route::get('/applicant_student_admit_card','ischool_report@applicant_student_admit_card');

   Route::post('/student_information_data','ischool_report@student_information_data');

      Route::post('/article_filter_data','ischool_report@article_filter_data');

	  Route::resource('/parents_info','Parents_info');
    Route::resource('/parents_info_child','arents_info_child');


    Route::resource('/teacher_info','teacher_info');

    Route::get('/teacher_info_report','ischool_report@teacher_info_report');
    Route::resource('/academic_syllabus','Academic_syllabus');
    Route::resource('/component','component');
    Route::get('/academic_syllabus_download/{id}','ischool_report@academic_syllabus_download');
	Route::resource('/manage_subject','Manage_subject');

    Route::resource('/route','route');
    Route::resource('/manage_transport','manage_transport');
    Route::resource('/addmission_test','addmission_test');
    Route::post('/admission_test_student','ischool_report@admission_test_student');
    Route::post('/class_w_section_filter','ischool_report@Class_w_section_filter');
    Route::post('/class_w_department_filter','ischool_report@class_w_department_filter');
	Route::post('/class_w_subject_filter','ischool_report@class_w_subject_filter');
    Route::post('/class_data_check','ischool_report@class_data_check');
Route::post('/route_wise_data','ischool_report@route_wise_data');
Route::post('/transport_wise_data','ischool_report@transport_wise_data');
//    Route::post('/student_information_filter','ischool_report@student_information_filter');

    Route::resource('/admit_student','Admit_student');
    Route::post('/admit_bulk_student_autoname_genrate','admit_bulk_student@admit_student_roll_name_genarate');
    Route::resource('/student_promoation','student_promoation');

	Route::resource('/staff_info','Staff_info');
    Route::resource('/salary_slip','salary_slip');
    Route::resource('/salary_slip_report','salary_slip_report');
    Route::post('/salary_slip_teacher_name','salary_slip@teacher_name_get');
    Route::post('/salary_slip_details','salary_slip@salary_slip_details');

    Route::resource('/check_marks','check_marks');
    Route::post('/check_marks_edit','check_marks@show_tabulize_data');
   Route::post('/show_attendance_data','daily_attendance@show_attendance_data');
    Route::resource('/marks_publish','marks_publish');
    Route::resource('/invoice_componnet','invoice_componnet');
     Route::post('/attendance_student','daily_attendance@show_student_data');

	//complete end



Route::resource('/manage_book','manage_book');


Route::resource('/admit_bulk_student','admit_bulk_student');

Route::resource('/student_information','student_information');


Route::resource('/accountant','accountant');
Route::resource('/setup','setup');
Route::resource('/chart_of_account','chart_of_account');
Route::resource('/create_template','create_template');
Route::resource('/student_payment_entry','student_payment_entry');
Route::post('/student_payment_entry_for_student_data','student_payment_entry@student_info');
Route::post('/student_templete_desgin','student_payment_entry@templete_desgin');
Route::resource('/staff_info','staff_info');






Route::resource('/manage_class','manage_class');
Route::resource('/manage_section','manage_section');
Route::resource('/academic_syllabus','academic_syllabus');
Route::resource('/manage_department','Manage_department');
Route::resource('/manage_subject','manage_subject');

Route::resource('/manage_class_routine','manage_class_routine');
Route::resource('/daily_attendance','daily_attendance');
Route::resource('/invoice','invoice');
Route::get('/invoice_print/{id}','invoice@invoice_print');
Route::resource('/expense','expense');

Route::resource('/exam_grade','exam_grade');
Route::resource('/manage_marks','manage_marks');
Route::resource('/question_paper','question_paper');
Route::resource('/salary_component','salary_component');
Route::resource('/salary_structure','salary_structure');
Route::resource('/salary_slip','salary_slip');
Route::resource('/notice_board','notice_board');
Route::resource('/student_apprisal_component','student_apprisal_component');
Route::resource('/student_apprisal','student_apprisal');
Route::get('/student_apprisal_pdf','student_apprisal@pdf');
Route::get('/student_apprisal_excel','student_apprisal@excel');
Route::get('/student_apprisal_edit/{id}','student_apprisal@student_apprisal_edit');


Route::resource('/article','article');
Route::resource('/stock_article','stock_article');
Route::resource('/templet_article','templet_article');
Route::resource('/membership','membership');
Route::resource('/article_issue','article_issue');
Route::resource('/article_recive','article_recive');
Route::resource('/create_admin','Create_admin');
Route::resource('/invoice_component','invoice_component');
//settings
Route::resource('/general_settings','general_settings');
Route::resource('/sms_settings','sms_settings');
Route::resource('/language_settings','language_settings');
//end of settings


Route::resource('/assign_transport','assign_transport');
Route::resource('/dormitory','dormitory');
Route::resource('/assign_dormitory','assign_dormitory');


//extra
Route::get('/question_paper_download/{file_name}/{file_ext}','question_paper@download');


// Excel and Pdf View and Download Report
	//complete start
	Route::resource('/create_admin_Excel_report','ischool_report@create_admin_Excel_report');
	Route::resource('/create_admin_pdf_report','ischool_report@create_admin_pdf_report');
	Route::resource('/create_role_pdf_report','ischool_report@create_role_pdf_report');
	Route::resource('/create_role_Excel_report','ischool_report@create_role_Excel_report');
	Route::resource('/role_based_permission_pdf_report','ischool_report@role_based_permission_pdf_report');
   Route::resource('/staff_report_pdf','ischool_report@staff_report_pdf');
     Route::resource('/staff_report_excle','ischool_report@staff_report_excle');
      Route::resource('/teacher_report_pdf','ischool_report@teacher_report_pdf');
    Route::resource('/teacher_report_excle','ischool_report@teacher_report_excle');
	Route::resource('/role_permission_excel','ischool_report@role_permission_excel');
	Route::resource('/user_access_export','ischool_report@user_access_export');
	Route::resource('/user_access_export_excel','ischool_report@user_access_export_excel');
  Route::resource('/assign_dormitory_pdf','ischool_report@assign_dormitory_pdf');
    Route::resource('/assign_dormitory_excle','ischool_report@assign_dormitory_excle');

  
      Route::resource('/component_pdf','ischool_report@component_pdf');
    Route::resource('/component_excel','ischool_report@component_excel');



  Route::resource('/salary_slip_report_pdf','ischool_report@salary_slip_report_pdf');
    Route::resource('/salary_slip_report_excle','ischool_report@salary_slip_report_excle');
    Route::resource('/daily_attendance_pdf','ischool_report@daily_attendance_pdf');


       Route::resource('/accountant_pdf','ischool_report@accountant_pdf');
    Route::resource('/accountant_excle','ischool_report@accountant_excle');

      Route::resource('/route_pdf','ischool_report@route_pdf');
    Route::resource('/route_excle','ischool_report@route_excle');
     Route::resource('/manage_transport_excle','ischool_report@manage_transport_excle');
    Route::resource('/manage_transport_pdf','ischool_report@manage_transport_pdf');
     Route::resource('/assign_transport_pdf','ischool_report@assign_transport_pdf');
    Route::resource('/assign_transport_excle','ischool_report@assign_transport_excle');

    Route::resource('/manage_dormitory_pdf','ischool_report@manage_dormitory_pdf');
    Route::resource('/manage_dormitory_excle','ischool_report@manage_dormitory_excle');


    Route::resource('/manage_department_pdf','ischool_report@manage_department_pdf');
    Route::resource('/manage_department_excle','ischool_report@manage_department_excle');


        Route::resource('/academic_syllabus_pdf','ischool_report@academic_syllabus_pdf');
    Route::resource('/academic_syllabus_excle','ischool_report@academic_syllabus_excle');

     Route::resource('/manage_section_pdf','ischool_report@manage_section_pdf');
    Route::resource('/manage_section_excle','ischool_report@manage_section_excle');

     Route::resource('/article_recive_pdf','ischool_report@article_recive_pdf');
    Route::resource('/article_recive_excle','ischool_report@article_recive_excle');

	Route::get('/parents_report_pdf','ischool_report@parents_report_pdf');
	Route::get('/parents_report_excel','ischool_report@parents_report_excel');

	Route::get('/manage_class_pdf','ischool_report@manage_class_pdf');
	Route::get('/manage_class_excel','ischool_report@manage_class_excel');
	Route::get('/applicant_student_pdf','ischool_report@applicant_student_pdf');
	Route::get('/applicant_student_excel','ischool_report@applicant_student_excel');
	Route::post('/manage_marks_for_student','manage_marks@mange_mark_student');
  Route::post('/manage_mark_for_student_show','manage_marks@manage_mark_for_student_show');
	Route::get('/student_print/{student_id}','ischool_report@student_print');
  Route::post('/grade_and_cgp_find','manage_marks@grade_and_cgp_find');

	//complete end
// Route::post('/admin_current_password','ajax_call@Admin_current_password'); wrong for ajax_call
    Route::resource('/templete_article_pdf','ischool_report@templete_article_pdf');
    Route::resource('/templete_article_excle','ischool_report@templete_article_excle');
    Route::resource('/article_pdf','ischool_report@article_pdf');
    Route::resource('/article_excle','ischool_report@article_excle');
    Route::resource('/stock_article_excle','ischool_report@stock_article_excle');
    Route::resource('/stock_article_pdf','ischool_report@stock_article_pdf');
    Route::resource('/membership_pdf','ischool_report@membership_pdf');
    Route::resource('/membership_excle','ischool_report@membership_excle');
    Route::resource('/article_issue_pdf','ischool_report@article_issue_pdf');
    Route::resource('/article_issue_excle','ischool_report@article_issue_excle');
    Route::resource('/article_id_wise_data','ischool_report@article_id_wise_data');
// Report Controller
        Route::post('/member_wise_data','ischool_report@member_wise_data');

Route::post('/article_id_issue_data','ischool_report@article_id_issue_data');
Route::get('/parentreport','ischool_report@parentreport');
Route::get('/staff_report','ischool_report@staff_report');
Route::get('/daily_attendance_report','ischool_report@daily_attendance_report');
Route::get('/payment_history','ischool_report@payment_history');

Route::get('/tabulation_sheet','ischool_report@tabulation_sheet');
Route::get('/student_apprisal_report','ischool_report@student_apprisal_report');
Route::get('/salary_structure_teacher_name','ischool_report@salary_structure_teacher_name');

Route::post('/notice_board_student_data_get','ischool_report@notice_board_student_data_get'); Route::post('/notice_board_teacher_data_get','ischool_report@notice_board_teacher_data_get');
Route::post('/notice_board_parents_data_get','ischool_report@notice_board_parents_data_get');
Route::post('/notice_board_class_data_get','ischool_report@notice_board_class_data_get');
Route::post('/notice_board_students_data_get','ischool_report@notice_board_students_data_get');
Route::resource('/create_admin_report','ischool_report@create_admin_report');
Route::resource('/apprisal_template_report','ischool_report@apprisal_template_report');
Route::post('/student_data_get','ischool_report@student_data_get');
Route::post('/class_wise_subject','ischool_report@class_wise_subject');
// Route::post('/password_forget','password_reset@password_forget'); Fack Email Sent Test For Email
Route::get('/home', 'HomeController@index');
});
Auth::routes();
